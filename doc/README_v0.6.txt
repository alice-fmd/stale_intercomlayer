---------------------------------------------------------------------------------
                InterComLayer version 0.6 release notes 
---------------------------------------------------------------------------------

Linux version:
--------------
Compilation:
------------
To compile the InterComLayer please follow these steps:
  - run ". setup.sh" or "source setup.sh" to prepare your shell (bash) 
  - if you compile InterComLayer for the first time on your system:
  - run "make -f makefile_dim" to compile the required DIM Library
  - run "make -f makefile_interCom.linux" to finally compile InterComLayer
  
  if you want to clean up your system:
    - run "make realclean -f makefile_dim.linux" to clean up DIM stuff
    - run "make clean -f makefile_interCom.linux" to clean up

Execution:
----------
To execute InterComLayer properly please follow these steps:
  - set DIM_DNS_NODE as environment variable to the dns for DIM
  - make sure that the dns for DIM is running on the machine you specified
  - make sure that all configuration files are available in the linux folder:
    - property file to load the required files
    - Server.txt for servers / dcscards
    - Services.txt for services / monitoring values
    - Database.txt for database information like contact string and user name/pw
  - finally run interComLayer from the linux folder

Composition of the Property file(Property.txt):
-----------------------------------------------
Body to load values from file:
source=File
logging=yes/no -> enable/dissable service logging
grouping=yes/no -> assess how the intercomlayer should publish the feeserver services. 
		   If grouping is choosen the InterComLayer publish all services of a FeeServer
		   in ONE channel. DIMTree and DID always show only the last published value of 
		   the InterComLayer.
		   If the value is set to "no" every service of a FeeServer is published in a 
		   single channel.
filenameserver=<filename comprised servernames>
filenameservices=<filename comprised servicenames>

Body to load values from Database:
source=Database
logging=yes/no
filenamedb=<filename comprised database values>

Notice: Don't let a free line between the values and don�t make blanks between the characters.

Keywords:
to load from file:source,logging,grouping,filenameserver,filenameservices
to load from database:source,logging,grouping,filenamedb


Important Notes:
----------------
  - make sure that the setup.sh is executed only ONCE per shell! 
  - make sure that the servername is comprised in the servicename in the
    Service.txt file
  

Windows version:
----------------
Required Environment Variables:
-------------------------------
Set the following enviroment variables:
DIM_DNS_NODE told on which host the dim name server is running
Path add the path of the instant client libs (.dll's) to the PATH variable

Execution:
----------
-start the dim framework
-The required file is contained in the dim directory/bin/dns.exe
-you can controll that the dim name server starts via the dimtree tool in the same directory as
 dns.exe
-change to the intercomlayer directory and enter the bin folder
-start interComLayer.exe
-the file to load Servernames calls Server.txt and for the Services calls Services.txt

---------------------------------------------------------------------------------

State for version 0.4.0:
------------------------
Besides the basic functionality there are several features implemented:
  - mockup user interface to control the whole program via command line:
  
  - integration of an Instruction Block Coder of Roland Bramm for:
    - creating ALTRO instruction memory blocks
    - creating BoardController instruction blocks
    - creating pedestal memory blocks
    please refer to Roland for more info about these blocks <roland.bramm@cern.ch>


Changes for version 0.4.1:
--------------------------
2004-04-30 and before:
  - adapted date format in all log messages
  - adapted creation of date entries
  - improved MockupUI
  - prepared donwload of TPC Pedestal Memory / instruction block(s)
  
Changes for version 0.4.2:
--------------------------
2004-05-11:
 - adapted connection to new Instruction Block Coder

Changes for version 0.4.3:
--------------------------
2004-07-20:
 - integrated version 14.4 of DIM framework
 - introduced exit handler in FedServer to avoid termination by DIM (e.g. ambitious user)
 - forced closing of log file in case of internal failures / exceptions
 

Changes for version 0.5.0:
--------------------------
2005-09-07
  - using a property file that specified where the required values came from (loading from a file or a database)
  - write the state of the connection to the different services into files like Value_servicename.txt
  - naming convention implemented
  - FeeServerCollection for 3 dimensional order of FeeServer
    (TPC: side, sector, segment; TRD: supermodule, stack, layer)
     -> FeeServernames have to be provided in correct order, 
        example: starting with side 0, sector 0, segment 0,side 0, sector 0, segment 1, ...
     the maxima for each dimension are important.
  - first database connection implemented

Changes for version 0.6.0:
--------------------------
2006-02-20
  - implementation of FedApi.
  - implementation of Rolands CommandCoder
  - improved handling of InterComLayer
  - publish of feeserver services changed
  - makefile changed to incremental building 
 
Changes for version 0.6.1:
--------------------------
2006-04-01
  - changes on the FedApi channels
 
Changes for version 0.6.2:
--------------------------
2006-08-11
  - needless channels removed
  - threading concept improved
  - latest CommandCoder version of Roland included
  - needless writing of logfiles removed
  - changes in the internal structure of the InterComLayer
  - setup script modified
