---------------------------------------------------------------------------------
                InterComLayer version 0.7 release notes 
---------------------------------------------------------------------------------

Linux version:
--------------
Compilation:
------------
To compile the InterComLayer please follow these steps:
  - run "./configure [--prefix <Installation Directory>]" to prepare your shell (bash) 
  - if you compile InterComLayer for the first time on your system:
  - run "make" to build the binary
  - run "make install" to install the InterComLayer in the desired directory  
  if you want to clean up your system:
    - run "make clean" to clean up the compilation stuff
    - delete the installation folder manually
  if you want to compile the InterComLayer with Oracle DB support:
  - run ./configure --with-oracle-libdir=[location of the Oracle libs]
Execution:
----------
To execute InterComLayer properly please follow these steps:
  - set DIM_DNS_NODE as environment variable to the dns for DIM
  - make sure that the dns for DIM is running on the machine you specified
  - make sure that all configuration files are available in the bin folder:
    - property file to load the required files
    - Server.txt for servers / dcscards
    - Services.txt for services / monitoring values
    - Database.txt for database information like contact string and user name/pw
  - finally run interComLayer from the bin folder

Installed project structure:
------------------
ICL root
  |
  |-bin
  |  |-interComLayer
  |
  |-data
  |  |-Property.txt
  |  |-Server.txt
  |  |-Services.txt
  |  |-Database.txt
  |
  |-libs
  |   |-libACE
  |   |-libCommandCoder
  |   |-libdim
  |
  |-doc
     |-InterComLayer-Versionxxx
                |-Readme

Composition of the Property file(Property.txt):
-----------------------------------------------
Body to load values from file:
source=File
logging=yes/no -> enable/dissable service logging
grouping=yes/no -> assess how the intercomlayer should publish the feeserver services. 
		   If grouping is choosen the InterComLayer publish all services of a FeeServer
		   in ONE channel. DIMTree and DID always show only the last published value of 
		   the InterComLayer.
		   If the value is set to "no" every service of a FeeServer is published in a 
		   single channel.
filenameserver=<filename comprised servernames>
filenameservices=<filename comprised servicenames>

Body to load values from Database:
source=Database
logging=yes/no
filenamedb=<filename comprised database values>

Notice: Don't let a free line between the values and don�t make blanks between the characters.

Keywords:
to load from file:source,logging,grouping,filenameserver,filenameservices
to load from database:source,logging,grouping,filenamedb

Configuration of the ICL via database:
--------------------------------------
This version of the InterComLayer can contact different hardware types. This is done by the Targe name given
by the PVSS (For example to contact FrontEndElectronics: TPC-FEE_0_0_0; to contact HighVoltage components:
TRD-HV_0_0_0;...). To told the InterComLayer which servers are correlated to the different hardwaretypes,
the servernames are stored in a table/view with the name of the hardwaretype.(for FrontEndElectronics use FEE,
for HighVoltage use HV). Please check the example databasefile in the linux folder.


Important Notes:
----------------
  - make sure that the servername is comprised in the servicename in the
    Service.txt file
  - Example files for the server and service files are included in the linux directory.
  - A example file for the database connection is also included in this directory.

Execution:
----------
-start the dim framework
-The required file is contained in the dim directory/bin/dns.exe
-you can controll that the dim name server starts via the dimtree tool in the same directory as
 dns.exe
-change to the intercomlayer directory and enter the bin folder
-start interComLayer.exe
-the file to load Servernames calls Server.txt and for the Services calls Services.txt

---------------------------------------------------------------------------------

Changes for version 0.5.0:
--------------------------
2005-09-07
  - using a property file that specified where the required values came from (loading from a file or a database)
  - write the state of the connection to the different services into files like Value_servicename.txt
  - naming convention implemented
  - FeeServerCollection for 3 dimensional order of FeeServer
    (TPC: side, sector, segment; TRD: supermodule, stack, layer)
     -> FeeServernames have to be provided in correct order, 
        example: starting with side 0, sector 0, segment 0,side 0, sector 0, segment 1, ...
     the maxima for each dimension are important.
  - first database connection implemented

Changes for version 0.6.0:
--------------------------
2006-02-20
  - implementation of FedApi.
  - implementation of Rolands CommandCoder
  - improved handling of InterComLayer
  - publish of feeserver services changed
  - makefile changed to incremental building 
 
Changes for version 0.6.1:
--------------------------
2006-04-01
  - changes on the FedApi channels
 
Changes for version 0.6.2:
--------------------------
2006-08-11
  - needless channels removed
  - threading concept improved
  - latest CommandCoder version of Roland included
  - needless writing of logfiles removed
  - changes in the internal structure of the InterComLayer
  - setup script modified
  
Changes for version 0.7:
------------------------
2006-09-06
  - bug fixed in grouped mode (InterComLayer creates "no link to service" messages although the link to these services are established) 
  - the internal server list have been changed
  - the InterComLayer can contact now different Hardware types(FEE,HV,LV,... only in database mode)
 - new CommandCoder interface contains a error handling mechanism

Changes for version 0.7.1
-------------------------
2006-10-26
   - race condition in the command channels fixed.
   - new build mechanism to InterComLayer introduced

Changes for version 0.7.2
-------------------------
2007-02-09
   - extend the Ack channel to propagate more information
   - new watchdog concept
   - add server names to the timeout message of ICL_ACK
   - new lookup table for send IDs and server names

Changes for version 0.7.3
-------------------------
2007-02-26
   - Ack channel can now handle binary data
   - new project structure and autotool stuff
   - Oracle libs not longer included in ICL project
