---------------------------------------------------------------------------------
                InterComLayer version 0.4.3 release notes 
---------------------------------------------------------------------------------

Linux version:
--------------
Compilation:
------------
To compile the InterComLayer please follow these steps:
  - run ". setup.sh" or "source setup.sh" to prepare your shell (bash) 
  - if you compile InterComLayer for the first time on your system:
    - run "make -f makefile_dim.linux" to compile the required DIM Library
    -(optional not really needed)run "make -f makefile_libibc.linux" to compile Instruction Block Coder
  - run "make -f makefile_interCom.linux" to finally compile InterComLayer
  
  if you want to clean up your system:
    - run "make realclean -f makefile_dim.linux" to clean up DIM stuff
    - run "make clean -f makefile_libibc.linux" to clean up
    - run "make clean -f makefile_interCom.linux" to clean up

Preparation:
------------
If you want to use the InstructionBlockCoder you need to create a
RCU configuration file, e.g. test0000. Therefore you should follow these steps:
  - go to the ibc folder
  - run "make ConfigIO"
  - execute "./ConfigIOMain.app"
Now there should be a file like test0000, which you can use in the application

Execution:
----------
To execute InterComLayer properly please follow these steps:
  - set DIM_DNS_NODE as environment variable to the dns for DIM
  - make sure that the dns for DIM is running on the machine you specified
  - make sure that all configuration files are available:
    - for servers / dcscards
    - for services / monitoring values
    - for Altro instructions and Pedestal stuff
  - finally run interComLayer from the linux folder
  

Important Notes:
----------------
  - make sure that the setup.sh is executed only ONCE per shell!
  

Windows version:
----------------
Required Environment Variables:
-------------------------------
Set the following enviroment variables:
DIM_DNS_NODE told on which host the dim name server is running
TNS_ADMIN told the program where the database is located
Path add the path of the instant client libs (.dll's) to the PATH variable

Execution:
----------
-start the dim framework
-The required file is contained in the dim directory/bin/dns.exe
-you can controll that the dim name server starts via the dimtree tool in the same directory as
 dns.exe
-change to the intercomlayer directory and enter the bin folder
-start interComLayer.exe


---------------------------------------------------------------------------------

State for version 0.4.0:
------------------------
Besides the basic functionality there are several features implemented:
  - mockup user interface to control the whole program via command line:
  
  - integration of an Instruction Block Coder of Roland Bramm for:
    - creating ALTRO instruction memory blocks
    - creating BoardController instruction blocks
    - creating pedestal memory blocks
    please refer to Roland for more info about these blocks <roland.bramm@cern.ch>


Changes for version 0.4.1:
--------------------------
2004-04-30 and before:
  - adapted date format in all log messages
  - adapted creation of date entries
  - improved MockupUI
  - prepared donwload of TPC Pedestal Memory / instruction block(s)
  
Changes for version 0.4.2:
--------------------------
2004-05-11:
 - adapted connection to new Instruction Block Coder

Changes for version 0.4.3:
--------------------------
2004-07-20:
 - integrated version 14.4 of DIM framework
 - introduced exit handler in FedServer to avoid termination by DIM (e.g. ambitious user)
 - forced closing of log file in case of internal failures / exceptions
 
 For next versions done ...
  - FeePacket ID is now static in FeePacket itself
  - Check for FedRPC size <= 0
  - Broadcast of commands via PVSS now possible (distribution on InterComLayer)
  - filtering of messages of different origin (InterComLayer, FeeServer)
  
  

