#ifndef CONFIGUREFERO
#define CONFIGUREFERO

#include "FedCommand.hpp"
#include <string>
#include "SendViaCoCo.hpp"
#include "FeeIclAck.hpp"
#include "AnswerStruct.hpp"
#include <boost/scoped_ptr.hpp>
#include "WatchDog.hpp"

//#include "ace/OS.h"
#include "ace/Synch.h"


/**
* CONFIGUREFERO_STRUCTURE represent the channel structure
*/
#define CONFIGUREFERO_STRUCTURE "C:20;I"
/**
* CFF_WATCH_DOG_TIMEOUT represent the watchdog timeout in ms
*/
#define CFF_WATCH_DOG_TIMEOUT 2000L

namespace ztt{namespace dcs{
/**
* the ConfFero struct store the incomming commands
*/
typedef struct ConfFero{
	/** Target */
	char target[20];
	/** mock up */
	int tags[256];
	
  
      /** 
       * Assignment operator 
       * @param rhs Object to assign from 
       * @return Reference to this object 
       */
      ConfFero& operator=(const ConfFero& rhs){
		if(this==&rhs){
			return *this;
		}
		memcpy(target,rhs.target,20);
		for(int i=0;i<10;i++){
			tags[0]=rhs.tags[i];
		}
		return *this;
			
	}

}ConfFero;
/**
* The ConfigureFero class represent the ConfigureFero channel of the FedApi.
* This class is used to configure the DCS-Boards and the hardware below.
* The class use a special send method to get via command coder the required data.
* 
* @date 2006-01-30
* @author Benjamin Schockert
*/
	class ConfigureFero:public FedCommand{
	
	public:
		/**
		* the ConfigureFero constructor initialize all required values.
		* @param name Name 
		* @param handler The command handler 
		*/
		ConfigureFero(char* name,DimCommandHandler* handler):FedCommand(name,CONFIGUREFERO_STRUCTURE,handler),
			mutex(),cond(mutex),busy(false),localID(0),iclAck(FeeIclAck::createInstance()),mWatchDog("ConfigureFero",&mAnswerStruct,&busy){
			//iclAck(FeeIclAck::createInstance());
			mWatchDog.activate(THR_DETACHED|THR_NEW_LWP,5,1);
			mb = new ACE_Message_Block((char*)&localID);

		};
		/**
		 * the ConfigureFero destructor destroy the mutex and condition
		* variable
		*/
	    virtual ~ConfigureFero();
		/**
		* the commandHandler method handle the incomming command.
		* @return @c true on success, @c false otherwise 
		*/
		virtual bool handleCommand();
		/**
		* the setAnswerData method collect the corresponding feeserver
		* acknowledges and wake the watch dog when all acks arrived.
		*
		* @param ID contain the answer ID
		* @return @c true on success, @c false otherwise 
		*/
		virtual bool  setAnswerData(const int& ID);
		/**
		* the watchDog is needed to detect timeouts of the feeserver
		*
		* @param objectHandler transport the required values into
		* the thread
		*/
		static void* watchDog(void* objectHandler);
		/**
		* the isBusy method return the actual state of the channel.
		* @return Whether we're busy 
		*/
	    bool isBusy();
	private:
		/**
		* the busy flag
		*/
		bool busy;
		/**
		* the mutex variable to lock the critical section
		*/
		ACE_Thread_Mutex mutex;
		/**
		* the condition value to wake up the watchdog when the
		* operation was successfull
		*/
		ACE_Condition<ACE_Thread_Mutex> cond;
		/**
		* send provide thread safe sending of data
		*/
		SendViaCoCo send;

		/**
		* the object to the InterComLayer acknowledge channel.
		*/
		boost::scoped_ptr<FeeIclAck> iclAck;
		/**
		* stores the first ID of each idpacket to use it as key
		*/
		int localID;
		/**
		* answerStruct store the id sets in a map and performs the
		* required actions such as searching, deleting, inserting.
		*/
		AnswerStruct mAnswerStruct;
		/**
		* mWatchDog is the watchDog object.
		*/
		WatchDog mWatchDog;
		/**
		* Message 
		*/
		ACE_Message_Block* mb;
	  
	};
  inline bool ConfigureFero::isBusy(){
    return busy;
  }

}}
//end namespace
#endif
