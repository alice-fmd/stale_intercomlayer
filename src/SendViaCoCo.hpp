#ifndef SEND_COCO_COMMANDS
#define SEND_COCO_COMMANDS


#include <iostream>
#include <vector>
#include <list>
#include "boost/shared_ptr.hpp"

#include "ace/Task.h"

#define SOURCE_COMMAND_CODER "ztt_intercomlayer_commandcoder"

namespace ztt{
  namespace dcs{

    //foreward declaration of FeePacket instead #include
    class FeePacket;
    /**
     * This class is used to create a new thread which main task is
     * the sending of configuration data down to the FeeServer.  This
     * class contain also the CommandCoder written by Roland and Tom.
     *
     * @date 2006-08-01
     * @author Benjamin Schockert
     */
    class SendViaCoCo : public ACE_Task_Base
    {
    public:
      /** 
       * Constructor 
       */
      SendViaCoCo(void){};
      /** 
       * Destructor 
       */
      ~SendViaCoCo(void);
      /**
       * function svc is the entry point of the thread.
       *
       */
      virtual int svc(void);
      /**
       * function to set the required values which are required in the thread.
       * @param name contain the servernames.
       * @param number contain the tag.
       */
      void setValues(const std::vector<char*>& name,
		     const std::list<int>& number){
	id = number;
	servernames = name;
      }
    private:
      /** id store all tags required for the db in a list. */
      std::list<int> id;
      /** vector of servernames to build customized data for each
       *  board. */
      std::vector<char*> servernames;
	
    };
    //end namespaces
  }
}
#endif


