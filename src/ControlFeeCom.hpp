#ifndef CONTROLFEECOM
#define CONTROLFEECOM

#include "FedCommand.hpp"
#include "SendWithoutCoCo.hpp"
#include "ace/Map_Manager.h"

/**
 * define the dim structure
 */
#define CONTROLFEECOM_STRUCTURE "I:1;I:1;C:256"
/**
 * command to send new feeserver binary to feeserver
 */
#define FEESERVER_UPDATE_FLAG 0x0004
/**
 * command to restart the dedicated feeserver
 */
#define FEESERVER_RESTART_FLAG 0x0008
/**
 * reboot the specified DCS board
 */
#define FEESERVER_REBOOT_FLAG 0x0010
/**
 * shutdown the specified DCS board
 */
#define FEESERVER_SHUTDOWN_FLAG 0x0020
/**
 *exit the specified feeserver
 */
#define FEESERVER_EXIT_FLAG 0x0040
/**
 * Define for watch dog timer in ms
 */
#define WATCH_DOG_TIMEOUT 1000
/**
 * Define to indicate a timeout via the last FeePacket ID.
 */
#define CHANNEL_TIMED_OUT -2
/**
 * Define to indicate that the RPC channel is currently in use / busy.
 */
#define CHANNEL_BUSY -1

namespace ztt{namespace dcs{

/**
* structure to get the content of the ControlFeeCom channel.  the
* structure is coresponding to the layout of the command channel.
*/
	typedef struct ContFeeComContent{
      		/** the id of the command */
		int commandID;
		/** is only used for the update feeserver command */
		int optionalTag;
		/** which server should receive the command */
		char target[256];
		/**
		* Assignment operator for ControlFeeCom struct that copy all
		* values.
		* @param rhs Object to assign from 
		* @return Reference to this 
		*/
	  	ContFeeComContent& operator=(const ContFeeComContent& rhs){
		  	if(this==&rhs){
				return *this;
			}
			commandID=rhs.commandID;
			optionalTag=rhs.optionalTag;
			memcpy(target,rhs.target,256);
    
			return *this;
			
		}
	}ContFeeComContent;

/**
* The class ControlFeeCom represent the ControlFeeCom command
* channel. This channel is used to control the Fee-/FedServer.  This
* class handle the incomming command and start the proper action.
*
* @author Benjamin Schockert
*/
	class ControlFeeCom:public FedCommand{
	
	public:
		/** 
		* Constructor 
		* @param name Name 
		* @param handler Command handler  
		*/
		ControlFeeCom(char* name,DimCommandHandler* handler)
	: FedCommand(name,CONTROLFEECOM_STRUCTURE,handler)
		{
	//send = SendFeeCommands::createInstance();
		};
		/** 
		* Destructor 
		*/
		virtual ~ControlFeeCom();
		/**
		* command Handler check the incomming command and 
		* create proper feeserver command
		* @return true on sucesss 
		*/
		virtual bool handleCommand();
		/**
		* set the corresponding answer of the feeserver (not
		* implemented!cause feeserver can�t answer!see commandHandler
		* commands)
		* @param ID Id 
		* @return true on sucesss 
		*/
		virtual bool  setAnswerData(const int& ID);
		/**
		* check that the answer thread is still alive(not implemented)
		* @param objectHandler Object to handle 
		* @return 
		*/
		static void* watchDog(void* objectHandler);
	private:
		/**
		* Send the assembled command to the feeserver
		*/
		SendWithoutCoCo send;
	};
    //end namespace
}}

#endif

