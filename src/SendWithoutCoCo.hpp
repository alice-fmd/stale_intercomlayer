#ifndef SEND_WITHOUT_COCO
#define SEND_WITHOUT_COCO

#include <vector>
#include "boost/shared_ptr.hpp"
#include "ace/Task.h"

namespace ztt{
  namespace dcs{

    // Forward decl. 
    class FeePacket;

    /**
     * Class to send messages to a number of Fee servers.  The same
     * message is send to all servers
     */
    class SendWithoutCoCo : public ACE_Task_Base
    {
    public:
      /** Constructor */
      SendWithoutCoCo(void){};
      /**
       * Copy constructor 
       * @param send Object to copy from 
       */
      SendWithoutCoCo(const SendWithoutCoCo& send){
		packet = send.packet;
      };
      /** 
       * Assignement operator 
       * @param send Object to assign from 
       * @return  Reference to this object 
       */
      SendWithoutCoCo& operator=(const SendWithoutCoCo& send){
		if (this != &send){
			packet = send.packet;
		}
		return *this;
      }
      /**
       * Destructor 
       */
      ~SendWithoutCoCo(void);
      /**
       * @return 
       */
      virtual int svc(void);
      /** Set the fee packet to send. 
       *  @param payload Fee packet to send. 
       */
      void setFeePacket(const boost::shared_ptr<FeePacket>payload){
		packet = payload;
      }

    private:
      /* Server names */
      // std::vector<char*>servernames;
      /** Packet to send */
      boost::shared_ptr<FeePacket> packet;
    };

  }}
//end namespace
#endif
