#include "Logger.hpp"
#include "FedServer.hpp"
#include "Timestamp.hpp"

#include <string>

#ifdef __DEBUG
#include <iostream>
#endif

using namespace std;
using namespace ztt::dcs;

Logger* Logger::pInstance = 0;

Logger* Logger::createLogger(FedMessenger* pMessenger) {
    if (pInstance == 0) {
        pInstance = new Logger(pMessenger);
    }
    return pInstance;
}

Logger::Logger(FedMessenger* pMessenger):mutex() {
    mpMessenger = pMessenger;
    mLogLevel = DEFAULT_LOCAL_LOGLEVEL;
    mRemoteLogLevel = DEFAULT_REMOTE_LOGLEVEL;
	mFileCount = 0;

    time_t timeVal;
    time(&timeVal);
    struct tm* timeStruct = localtime(&timeVal);

	Timestamp now;
    char description[100];
    char source[15];
    unsigned int type;

    type = Msg_Info;
    source[sprintf(source, "InterCom-Layer")] = 0;
    description[sprintf(description, "InterCom - Layer started (LogLevel: %d).",
            mLogLevel)] = 0;

	MessageStruct msgStruct(type, DETECTOR, source, description, now.date);

    mpMessenger->setMessage(&msgStruct);

    // write into log file
    if (timeStruct != NULL) { // Logfile with date of runstart in name
/*#ifndef WIN32
		 mFileName[sprintf(mFileName, "%sInterCom-%04d-%02d-%02d-(%02d-%02d).log",STANDARD_LOG_DIRECTORY,
                (timeStruct->tm_year + 1900), (timeStruct->tm_mon + 1),
                timeStruct->tm_mday, timeStruct->tm_hour,
                timeStruct->tm_min)] = 0;
#endif*/
//#ifdef WIN32
        mFileName[sprintf(mFileName, "InterCom-%04d-%02d-%02d-(%02d-%02d).log",
                (timeStruct->tm_year + 1900), (timeStruct->tm_mon + 1),
                timeStruct->tm_mday, timeStruct->tm_hour,
                timeStruct->tm_min)] = 0;
//#endif
		miLogDay = timeStruct->tm_mday;
    } else { // name of logfile is InterCom.log, new entries are appended to old ones.
        mFileName[sprintf(mFileName, "InterCom.log")] = 0;
    }

    mLogFile = new ofstream(mFileName, ios_base::out | ios_base::app);

    if (!(*mLogFile)) {
#		ifdef __DEBUG
        cout << "Can't create logfile! ";
        cout << "Created file name : " << mFileName << endl;
#		endif

        type = Msg_Error;
        source[sprintf(source, "InterCom-Layer")] = 0;
        description[sprintf(description,
                "Unable to create LogFile on InterCom - Layer")] = 0;

        MessageStruct errorMsg(type, DETECTOR, source, description, now.date);
        mpMessenger->setMessage(&errorMsg);

    } else {
        char msg[600];

        mLogFile->put('\n');
        msg[sprintf(msg,
                "Log Event Types: INFO [%3d], WARNING [%3d], ERROR [%3d], ",
                Msg_Info, Msg_Warning, Msg_Error)] = 0;
        mLogFile->write(msg, strlen(msg));
        msg[sprintf(msg,
                "FAILURE AUDIT [%3d], SUCCESS AUDIT [%3d], DEBUG [%3d], ALARM[%3d].\n",
                Msg_Failure_Audit, Msg_Success_Audit, Msg_Debug, Msg_Alarm)] = 0;
        mLogFile->write(msg, strlen(msg));
        mLogFile->put('\n');

        msg[sprintf(msg, "%s [%3d] - %s/%s:\t %s\n", now.date, type, DETECTOR,
                source, description)] = 0;

        mLogFile->write(msg, strlen(msg));
    }

};

Logger::~Logger() {

    if (pInstance == 0) {
        return;
    }
	
    time_t timeVal;
    time(&timeVal);

    // write into log file
    if (*mLogFile) {
        char description[35];
        char source[15];
        int type;
        char msg[500];
		Timestamp now;

        type = Msg_Info;
        source[sprintf(source, "InterCom-Layer")] = 0;
        description[sprintf(description, "Shuting down InterCom - Layer.")] = 0;


        msg[sprintf(msg, "%s [%3d] - %s/%s:\t %s\n", now.date, type, DETECTOR,
                source, description)] = 0;

        mLogFile->write(msg, strlen(msg));
        mLogFile->put('\n');
        mLogFile->close();
    }
    delete mLogFile;
	mutex.remove();
    // set pInstance to 0
    pInstance = 0;
	mutex.remove();
}

// for local events
void Logger::createLogMessage(unsigned int type, char* origin, char* description) const {
   
    if (!checkLogLevel(type)) {
        return;
    }

	Timestamp now;

	MessageStruct msg(type, DETECTOR, origin, description, now.date);

    sendLogEntry(&msg);
}

// for remote events
void Logger::relayLogEntry(MessageStruct* msgStruct) const {
    // check msg-type, if delivery to PVSS is wanted
    if (!checkRemoteLogLevel(msgStruct->eventType)) {
        // not wanted in PVSS -> write only to log file
  //      char message[600];
		//!!!moved to writeLogEntry!!!
  /*      message[sprintf(message, "%s [%3d] - %s/%s:\t %s\n", 
                msgStruct->date, msgStruct->eventType, msgStruct->detector,
                msgStruct->source, msgStruct->description)] = 0;*/
		if(!pInstance->writeLogEntry(msgStruct)) {
			// something went wrong, so what? upper layers are already informed...
		}
        return;
    }
    // send to PVSS AND write to log file
    sendLogEntry(msgStruct);
}

// delivers local and remote events
void Logger::sendLogEntry(MessageStruct* msgStruct) const {
	// set and update FedMessenger service (-> PVSS)
    mpMessenger->setMessage(msgStruct);

    // write into log file
	if(!pInstance->writeLogEntry(msgStruct)) {
		// something went wrong, so what? upper layers are already informed...
	}
}

bool Logger::writeLogEntry(MessageStruct* msgStruct) {
	//synchronize the critical area
	ACE_Guard<ACE_Mutex>guard(mutex);

	time_t timeVal;
    time(&timeVal);
    struct tm* timeStruct = localtime(&timeVal);

	char message[600];
    message[sprintf(message, "%s [%3d] - %s/%s:\t %s\n", msgStruct->date,
            msgStruct->eventType, msgStruct->detector, msgStruct->source,
            msgStruct->description)] = 0;

	if (*mLogFile) {
		mLogFile->write(message, strlen(message));
		++mFileCount;
		if(mFileCount >= FILE_CLOSE_COUNT) {
			mLogFile->close();
			delete mLogFile;
			// create every day a new log file
			if ((timeStruct != NULL) && (miLogDay != timeStruct->tm_mday)){
/*#ifndef WIN32
				mFileName[sprintf(mFileName, "%sInterCom-%04d-%02d-%02d-(%02d-%02d).log",STANDARD_LOG_DIRECTORY,
					(timeStruct->tm_year + 1900), (timeStruct->tm_mon + 1),
					timeStruct->tm_mday, timeStruct->tm_hour,
					timeStruct->tm_min)] = 0;
#endif
#ifdef WIN32*/
				mFileName[sprintf(mFileName, "InterCom-%04d-%02d-%02d-(%02d-%02d).log",
					(timeStruct->tm_year + 1900), (timeStruct->tm_mon + 1),
					timeStruct->tm_mday, timeStruct->tm_hour,
					timeStruct->tm_min)] = 0;
//#endif
				miLogDay = timeStruct->tm_mday;
			}
			mLogFile = new ofstream(mFileName,ios_base::app | ios_base::out );
			if (!(*mLogFile)) {
#			ifdef __DEBUG
				cout << "!!! could not create logfile: ";
				cout << mFileName << endl;
#			endif

				Timestamp now;
				char description[100];
				char source[15];
				unsigned int type = Msg_Error;
				source[sprintf(source, "InterCom-Layer")] = 0;
				description[sprintf(description,
						"Unable to create LogFile on InterCom - Layer")] = 0;
					MessageStruct errorMsg(type, DETECTOR, source, description, now.date);
				mpMessenger->setMessage(&errorMsg);
				return false;
			}
			mFileCount = 0;
		}
		return true;
	}
	return false;	
}

// local log level
void Logger::setLogLevel(unsigned int logLevel) {
    char description[100];

    if ((logLevel > Msg_Max_Val) || (logLevel < 0)) {

        description[sprintf(description,
                "Received wrong LogLevel - value (%d) for local log level.",
                logLevel)] = 0;

        createLogMessage(Msg_Error, "InterCom-Layer", description);
    } else {
        mLogLevel = logLevel | Msg_Alarm;
    
        description[sprintf(description,
                "Set new LogLevel (%d) for local events.", mLogLevel)]
                = 0;

        createLogMessage(Msg_Info, "InterCom-Layer", description);
    }
}

// log level for remote events
void Logger::setRemoteLogLevel(unsigned int remoteLogLevel) {
    char description[100];

    if ((remoteLogLevel > Msg_Max_Val) || (remoteLogLevel < 0)) { 
        
        description[sprintf(description,
                "Received wrong LogLevel - value (%d) for remote events.",
                remoteLogLevel)] = 0;

        createLogMessage(Msg_Error, "InterCom-Layer", description);
    } else {
        mRemoteLogLevel = remoteLogLevel | Msg_Alarm;
    
        description[sprintf(description,
                "Set new LogLevel (%d) for remote events.", mRemoteLogLevel)]
                = 0;

        createLogMessage(Msg_Info, "InterCom-Layer", description);
    }
}

unsigned int Logger::getLogLevel() {
    return mLogLevel;
}

unsigned int Logger::getRemoteLogLevel() {
    return mRemoteLogLevel;
}
