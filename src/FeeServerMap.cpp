#include "FeeServerMap.hpp"
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <cctype>



using namespace std;
using namespace ztt::dcs;

void FeeServerMap::setFeeServer(std::vector<int> x,std::vector<int> y,
								std::vector<int> z,std::vector<char*> server,char* name)
{	
	for (unsigned int i=0;i<server.size();i++){
		char* temp = new char[100];
		
		if ((x[i] >= xCoor)||(y[i] >= yCoor)||(z[i] >= zCoor)){
			throw std::logic_error(
					"ERROR one of the dimension sizes is out of range!");
			break;
		}
		
		temp[sprintf(temp,"%d%d%d%s",x[i],y[i],z[i],name)]=0;
		string key = temp;
		//transform all chars into lowercase
		//the hack with the (int(*)(int)) is required to told the gcc which 
		//tolower is desired.
		transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
		string pair = server[i];
		collection[key] = pair;
		delete[] temp;
	}
	
}
void FeeServerMap::setFeeServer(int x, int y, int z, std::vector<char*> server,char* name){


	if ((x <= 0)||(y <= 0)||(z <= 0)){
			throw std::logic_error(
					"ERROR one of the dimension sizes is zero or below!!");
	}
	if (x*y*z>(signed int)server.size()){
		 throw std::logic_error(
            "Container is to small to store all FeeServer names");
	}
	xCoor = x;
	yCoor = y;
	zCoor = z;
	string st;
	


	for (int i = 0; i < xCoor; i++){
		for (int j = 0; j < yCoor; j++){
			for (int k = 0; k < zCoor; k++){
				char* temp = new char[100];
				temp[sprintf(temp,"%d%d%d%s",i,j,k,name)]=0;
				string key = temp;
				//transform chars in lowercase to ensure that always the correct key is created.
				//(int(*)(int)) is a hack to told the gcc which tolower function is required.
				transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
				string pair = server[k];
				collection[key] = pair;
				delete[] temp;
			}
		}
	}
}
vector<char*> FeeServerMap::getFeeServer(int x,int y,int z,char* type){
	//locking critical area
	ACE_Guard<ACE_Mutex> guard(mutex);

	resultSet.clear();
	map<string,string>::iterator itermap;
	char temp[100];
	//check that no coordinates exceed the possible boarders
	if((x>=xCoor)||(y>=yCoor)||(z>=zCoor)){
		
#ifdef __DEBUG
		cout<<"ERROR the chosen FeeServer is out of of range!"<<endl;
#endif
			return resultSet;
	}

	if(x < 0){
		for(int i=0;i<xCoor;i++){

			if (y < 0){
				for(int j=0;j<yCoor;j++){

					if(z<0){
						//x,y,z broadcast
						for(int k=0;k<zCoor;k++){
							temp[sprintf(temp,"%d%d%d%s",i,j,k,type)]=0;
							string key = temp;
							transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
							itermap = collection.find(key);
							if(itermap != collection.end()){
								//cout<<itermap->second<<endl;
								//char* val = new char[itermap->second.length()];
								const char* val = itermap->second.data();
								//itermap->second.copy(val,string::npos);
								resultSet.push_back(const_cast<char*>(val));
							}else{
								//cout<<"Falsche Eingabe"<<endl;
							}
						}
					//x and y broadcast
					}else{
						temp[sprintf(temp,"%d%d%d%s",i,j,z,type)]=0;
						string key = temp;
						transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
						itermap = collection.find(key);
						if(itermap != collection.end()){
						//	cout<<itermap->second<<endl;
							
							const char* val = itermap->second.data();
							
							resultSet.push_back(const_cast<char*>(val));
						}else{
					//		cout<<"Falsche Eingabe"<<endl;
						}
					}
				}
			//x and z broadcast
			}else if(z<0){
				for (int k=0;k<zCoor;k++){
					temp[sprintf(temp,"%d%d%d%s",i,y,k,type)]=0;
					string key = temp;
					transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
					itermap = collection.find(key);
					if(itermap != collection.end()){
					//	cout<<itermap->second<<endl;
						//char* val = new char[itermap->second.length()];
						const char* val = itermap->second.data();
						//itermap->second.copy(val,string::npos);
						resultSet.push_back(const_cast<char*>(val));
					}else{
					//	cout<<"Falsche Eingabe"<<endl;
					}
				}
			//x broadcast
			}else{
				temp[sprintf(temp,"%d%d%d%s",i,y,z,type)]=0;
				string key = temp;
				transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
				itermap = collection.find(key);
				if(itermap != collection.end()){
					//cout<<itermap->second<<endl;
					//char* val = new char[itermap->second.length()];
					const char* val = itermap->second.data();
					//itermap->second.copy(val,string::npos);
					resultSet.push_back(const_cast<char*>(val));
				}else{
					//cout<<"Falsche Eingabe"<<endl;
				}
			}
		}
	}else if (y < 0){
		for (int j=0;j<yCoor;j++){
			if(z < 0){
				//y and z broadcast
				for (int k=0;k<zCoor;k++){
					temp[sprintf(temp,"%d%d%d%s",x,j,k,type)]=0;
					string key = temp;
					transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
					itermap = collection.find(key);
					if(itermap != collection.end()){
					//	cout<<itermap->second<<endl;
						//char* val = new char[itermap->second.length()];
						const char* val = itermap->second.data();
						//itermap->second.copy(val,string::npos);
						resultSet.push_back(const_cast<char*>(val));
					}else{
					//	cout<<"Falsche Eingabe"<<endl;
					}
				}
			//y broadcast
			}else{
				temp[sprintf(temp,"%d%d%d%s",x,j,z,type)]=0;
				string key = temp;
				transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
				itermap = collection.find(key);
				if(itermap != collection.end()){
				//	cout<<itermap->second<<endl;
					//char* val = new char[itermap->second.length()];
					const char* val = itermap->second.data();
					//itermap->second.copy(val,string::npos);
					resultSet.push_back(const_cast<char*>(val));
				}else{
					//cout<<"Falsche Eingabe"<<endl;
				}
			}
		}
	//z broadcast
	}else if (z < 0){
		for (int k=0; k < zCoor; k++){
				temp[sprintf(temp,"%d%d%d%s",x,y,k,type)]=0;
				string key = temp;
				transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
				itermap = collection.find(key);
				if(itermap != collection.end()){
				//	cout<<itermap->second<<endl;
					//char* val = new char[itermap->second.length()];
					const char* val = itermap->second.data();
					//itermap->second.copy(val,string::npos);
					resultSet.push_back(const_cast<char*>(val));
				}else{
					//cout<<"Falsche Eingabe"<<endl;
				}
		}
	//single call only
	}else{
		temp[sprintf(temp,"%d%d%d%s",x,y,z,type)]=0;
		string key = temp;
		transform(key.begin(),key.end(),key.begin(),(int(*)(int))std::tolower);
		itermap = collection.find(key);
		if(itermap != collection.end()){
		//	cout<<itermap->second<<endl;
			//char* val = new char[itermap->second.length()];
			const char* val = itermap->second.data();
			//itermap->second.copy(val,string::npos);
			resultSet.push_back(const_cast<char*>(val));
		}else{
			//cout<<"Falsche Eingabe"<<endl;
		}
	}
	return resultSet;

}
vector<char*> FeeServerMap::getAllFeeServer(){
	
	resultSet.clear();

	map<string, string>::iterator iterMap;
	iterMap = collection.begin();
	while(iterMap != collection.end()){
		const char* val = iterMap->second.data();
		//itermap->second.copy(val,string::npos);
		resultSet.push_back(const_cast<char*>(val));
		iterMap++;
	}
	return resultSet;
}

void FeeServerMap::print() const
{
  map<string, string>::const_iterator iterMap;
  iterMap = collection.begin();
  size_t i = 1;
  while(iterMap != collection.end()) {
    std::cout << std::setw(4) << i << ":\t" << iterMap->first
             << "\t" << iterMap->second << std::endl;
    iterMap++;
    i++;
  }
}
