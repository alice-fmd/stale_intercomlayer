#include "AnswerStruct.hpp"
#include <iostream>
#include <algorithm>
//#include <windows.h>

using namespace ztt::dcs;
using namespace std;

void AnswerStruct::setAnswerStruct(const int extID,const std::map<int, char*>& value){
	
	//store the key/value pair
	ACE_Guard<ACE_Thread_Mutex> guard(mut);
	idMap[extID] = value;
}
int AnswerStruct::findAnswerID(const int& extID){
	
	//Sleep(5000);
	int bRet = 0;
	//check that the map is not empty
	if (idMap.size() == 0){
		return 0;
	}
	ACE_Guard<ACE_Thread_Mutex> guard(mut);
	//begin of the critical area
	//walk through the map and return the single values of the map
	for(map<int,map<int,char*> >::iterator mapIter = idMap.begin(); mapIter != idMap.end(); mapIter++){
		//assign the value of the map to a temporary set
		//set<int> expectedAnswer = mapIter->second;
		map<int,char*> expectedAnswer = mapIter->second;
		//search in the temporary set for the given answer id
		//set<int>::iterator iter = find(expectedAnswer.begin(),expectedAnswer.end(),extID);
		map<int, char*>::iterator iter = expectedAnswer.find(extID);
		//if the answer id is found the coresponding entry will be deleted
		//after deleting the entry the map content is updated
		if ( iter != expectedAnswer.end() ){
			cout<<"Servername:"<<iter->second<<endl;
			expectedAnswer.erase(iter);
			idMap[mapIter->first] = expectedAnswer;
			bRet = 2;
			//when the set is empty all answers reached
			if (expectedAnswer.empty()){
				bRet = 1;
				idMap.erase(mapIter->first);
#ifdef __DEBUG
				cout<<"Container size: "<<idMap.size()<<endl;
#endif
				break;
			}
			break;
		}
	}
	return bRet;
}

std::map<int,char*> AnswerStruct::freeAnswerStruct(const int& extID){

	ACE_Guard<ACE_Thread_Mutex> guard(mut);
	//delete the set from the map because a timeout has occured and the 
	//set is not valid any longer
	map <int,char*> val;
	map<int,map<int,char*> >::iterator iter = idMap.find(extID);
	if(iter != idMap.end()){
		val = iter->second;
		idMap.erase(extID);
	}
//#ifdef __DEBUG
	cout<<"Clearing Map: "<<idMap.size();
//#endif
	return val;
}
