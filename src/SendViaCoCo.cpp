#include "SendViaCoCo.hpp"
#include "InterCom.hpp"
#include "Logger.hpp"

#include "CommandCoderBase.hpp"
 

using namespace std;
using namespace ztt::dcs;

int SendViaCoCo::svc(void){
	
	list<int> dbTags = id;
	id.clear();
	vector<char*> serverContainer = servernames;
	servernames.clear();

	const Logger* log = Logger::getLogger();
	InterCom* icl = InterCom::createInterCom();
	int mResult;
	
	char* server= new char[256];
	//iterator to iterate through the servernames vector	
	vector<char*>::iterator iter = serverContainer.begin();
	while(iter != serverContainer.end()){
		//list is used to get more api conform in ConfigureFero channel (256 tags supported)
		list<int>::iterator listiter = dbTags.begin();
		while(listiter != dbTags.end()){
			server[sprintf(server,*iter)]=0;
			//commandcoder interface
			int length = -1;
			length = CommandCoderBase::createInstance()->createDataBlock(server,*listiter);
			//if length is smaller 0 an error in the CoCo occured
			if(length < 0){
				int count = 1;
				vector<string> error = CommandCoderBase::createInstance()->getError();
				vector<string>::iterator iterror;
				string errormessage;
				try{
					iterror = error.begin();
					while(iterror!= error.end()){
						string text = *iterror;
						errormessage += text+";";
						iterror++;
						count++;
					}
					char* text = new char[(errormessage.length() + 1)];
					errormessage.copy(text,string::npos);
					text[errormessage.length()]=0;
					log->createLogMessage(Msg_Alarm,SOURCE_COMMAND_CODER,text);
					delete[] text;
					error.clear();
					errormessage.clear();
				}catch(std::runtime_error &excep){
					cout << excep.what() << endl;
				}
				catch(...){}
				return 0;
			}
			long int* pBlob = CommandCoderBase::createInstance()->getDataBlock();
			//convert and copy the long int data to the char array
			char* payload = new char[length];
			memcpy(payload,reinterpret_cast<char*>(pBlob),length);
			//create the corresponding FeePacket
			boost::shared_ptr<FeePacket> feepack = icl->createFeePacket(payload,length);
			//check that the FeeServer is online
			if(icl->validateServerName(server)){
				strcat(server,"_Command");
				mResult = icl->sendCommand(server,feepack);
			}else{
#ifdef __DEBUG
				cout<<"no link to server: "<< server <<endl;
#endif
				try{
					char text[100];
					text[sprintf(text,"no link to server: %s",server)]=0;
					log->createLogMessage(Msg_Warning,SOURCE_LOCAL,text);
				}catch(std::runtime_error &excep){
					cout << excep.what() << endl;
				}
			}
			listiter++;
			delete[] payload;
			feepack.reset();
		}
		iter++;
		
	}
	delete[] server;
	serverContainer.clear();
	dbTags.clear();
	
	return 0;

}
SendViaCoCo::~SendViaCoCo(void)
{
	servernames.clear();	
	id.clear();
}

