#ifndef ZTT_DCS_FEE_ACK_INFO_HPP
#define ZTT_DCS_FEE_ACK_INFO_HPP

#include "FeeInfo.hpp"
#include "ConfigureFeeCom.hpp"
#include "ControlFero.hpp"
#include "ConfigureFero.hpp"
#include <boost/shared_ptr.hpp>

namespace ztt { namespace dcs {

	

/**
 * Value, when link to an ACK-Service is not available.
 */
#define FEE_ACK_NO_LINK "No Link"
/**
 * Size of "No Link" - value for ACK-Service, NOT inclusive '\\0' .
 */
#define FEE_ACK_NO_LINK_SIZE 7


/*
 * The area -70 to -79 is especially reserved for FeeAckInfos.
 * So use these numbers here. For OK use always '0'.
 */

/**
 * Define for the FEE - acknowledge info, when link is ok.
 */
#define FEE_ACK_LINK_OK 0
/**
 * Define for the FEE - acknowledge info, when link is lost.
 */
#define FEE_ACK_LOST_LINK -71
/**
 * Define for the FEE - acknowledge info, when strange data has been received.
 */
#define FEE_ACK_STRANGE_DATA -72
/**
* ACK_STRUCTURE describe the structure of the FeeServer ack channel
*/
#define ACK_STRUCTURE "I:1;C"
/**
* MAX_SIZE define the length of the resultData array
*/
#define MAX_SIZE 11000

class FeeClient;
/**
* The AckStructure represent the structure of
* the ack channel of each feeserver
*
* @author Benjamin Schockert
*/
typedef struct AckStructure{
        /**
        * errorCode contain the error code returned by the feeserver.
        */
	int errorCode;
        /**
        * resultData is a array that contain messages or results of commands.
        */
        char resultData[MAX_SIZE];
        /**
        * Std C-Tor of the AckStructure.
        */
	AckStructure(){
		errorCode = 0;
		resultData[sprintf(resultData,"initialized")]=0;
	}
        /**
        * overloaded C-Tor to set the values of the struct directly.
        */
	AckStructure(short code,int& size,char* data){
		errorCode=code;
		memcpy(resultData,data,(size < MAX_SIZE ? size : MAX_SIZE));
	}
        /**
        * Assignment operator of the AckStructure.
        */
	AckStructure& operator=(const AckStructure& rhs){
		
		if(this ==&rhs) {
            return *this;
        }
		errorCode = rhs.errorCode;
		memcpy(resultData,rhs.resultData,MAX_SIZE);
        
		return *this;
	}
}AckStructure;


/**
 * Class, to subscribe and get access to the FEE - ACK - service.
 * It inheritates from FeeInfo to get the DimInfo features, and to enable a
 * polymorph access to the service data. Create objects of FeeAckInfo to get
 * a connection to a given FEE - ACK - service.
 *
 * @see FeeInfo for more details
 *
 * @author Christian Kofler, Sebastian Bablok
 *
 * @date 2003-08-27
 */
class FeeAckInfo : public FeeInfo {
	public:
		/**
		 * Constructor for the FeeAckInfo.
		 * This constructor calls implictly the FeeInfo - constructor and with 
		 * that the DimInfo constructor, so you subsrcibe to the inserted 
		 * FEE - ACK- Service (name).
		 *
		 * @param name name of the service, to subscribe to
		 * @param noLink pointer to value to be shown, when no connection to  
		 *			this service is available
		 * @param noLinkSize size of the noLink - value
		 * @param pHandler pointer to the DimInfoHandler
		 * @param serverName the name of the corresponding FeeServer
		 */
		FeeAckInfo(char* name, void* noLink, int noLinkSize,
				DimInfoHandler* pHandler, char* serverName)
				: FeeInfo(name, noLink, noLinkSize, pHandler),
				mpServerName(serverName), ackState(FEE_ACK_LOST_LINK),mpConfigureFeeCom(),mpConfigureFero(),mpControlFero(),mpAckService(){};
		/**
		 * Destructor for the FeeAckInfo objects
		 */
		virtual ~FeeAckInfo();

		/**
		 * Function, to be executed when new data from the FEE - ACK - Service
		 * is available. It retrieves the data of the FeeServer from the DIM 
		 * framework. It stores the value in the object itself. 
		 *
		 * @return true, if service is available, false in case of "no link"
		 */
		virtual bool retrieveServiceValue();

		/**
		 * Function to set the initial state of the link for the ACK-Service.
		 * This should be called to determine, if the link is currently 
		 * available and should be called once after constructing this object.
		 */ 
		virtual void initialize();

        /**
         * Gets a pointer to the server name of the corresponding FeeServer for
         * this DimInfo object.
         *
         * @return the corresponding server name
         */
         const char* getCorrespondingServerName() const;

         /**
          * Indicates if the FeeClient has "link" to the server corresponding 
          * to this FeeAckInfo object.
          *
          * @return false if no-link, true otherwise
          */
         bool hasLink();

		int recErrorCode;
		//dim service points to this variable
		AckStructure sAckStruct;
		//set the corresponding dim service
		void setAckChannel(boost::shared_ptr<DimService> service);
		//fill the ackstructure
		void setAckStructure(AckStructure* ackstruct);
		//void setAckStructure(char* ackstruct);
		/**
		* Points to the several command channel which could wait for an 
		* acknowledge
		* 
		*/
		void setFedApiChannels(boost::shared_ptr<ConfigureFeeCom> conf,boost::shared_ptr<ControlFero> contfero,
			boost::shared_ptr<ConfigureFero> conffero);
		
	private:
		/**
		 * Here is the ACK value stored.
		 */
		int ackState;

        /**
         * Pointer to the corresponding FedApi - services in the FedServer
         * to notify the answer channel of a returned ack - result.
         */
		boost::shared_ptr<ConfigureFeeCom> mpConfigureFeeCom;
		boost::shared_ptr<ConfigureFero> mpConfigureFero;
		boost::shared_ptr<ControlFero> mpControlFero;
		/**
		* points to the acknowledge channel of each FeeServer
		*/
		boost::shared_ptr<DimService> mpAckService;
		/**
		 * Here is the name of the corresponding FeeServer stored.
		 */
		const char* mpServerName;


}; // end class

inline const char* FeeAckInfo::getCorrespondingServerName() const {
    return mpServerName;
}

inline bool FeeAckInfo::hasLink() {
    return (ackState != FEE_ACK_LOST_LINK) ? true : false;
}
inline void FeeAckInfo::setAckChannel(boost::shared_ptr<DimService> service){
	mpAckService = service;
}

inline void FeeAckInfo::setAckStructure(AckStructure* ackstruct){
	sAckStruct = *ackstruct;
}
inline void FeeAckInfo::setFedApiChannels(boost::shared_ptr<ConfigureFeeCom> conf,boost::shared_ptr<ControlFero> contfero,
										  boost::shared_ptr<ConfigureFero> conffero){
	mpConfigureFeeCom = conf;
	mpControlFero = contfero;
	mpConfigureFero = conffero;
}

} }// end namespace

#endif

