#ifdef __UTEST

#ifndef ZTT_DCS_FED_MESSENGERTEST_HPP
#define ZTT_DCS_FED_MESSENGERTEST_HPP

#include "FedMessenger.hpp"
#include "DataAccess.hpp"

namespace ztt { namespace dcs {

/**
 * This class makes unit tests of the FedMessenger,
 * and the appertaining to MessageStruct.
 *
 * @author Sebastian Bablok
 */
class FedMessengerTest {
public:
    FedMessengerTest();

    ~FedMessengerTest();

    bool testFedMessenger();

    bool testSetMessage();

    bool testMessageStruct();
    
    bool testAssignMessageStruct();

    bool testAll();

private:
    boost::shared_ptr<DataAccess> dA;


};// end class

} } // end namespace


#endif // end of header

#endif // end of UTEST
