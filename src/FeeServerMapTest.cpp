#ifdef __UTEST

#include "FeeServerMapTest.hpp"
#include "FeeServerMap.hpp"
#include <vector>


using namespace std;
using namespace ztt::dcs;

FeeServerMapTest::FeeServerMapTest(void)
{
}

FeeServerMapTest::~FeeServerMapTest(void)
{
}
bool FeeServerMapTest::setServer(){

	bool nRet = false;
	bool exception1 = false;
	bool exception2 = false;
	bool exception3 = false;

	vector<char*> test;
	test.push_back("test");
	test.push_back("test1");
	test.push_back("test2");

	try{
		FeeServerMap feeMap;
		feeMap.setFeeServer(-1,1,1,test,"TEST");
	}catch(std::logic_error& e){
		exception1 = true;	
	}
	try{
		FeeServerMap feeMap;
		feeMap.setFeeServer(1,-1,1,test,"TEST");
	}catch(std::logic_error& e){
		exception1 = true;	
	}
	try{
		FeeServerMap feeMap;
		feeMap.setFeeServer(1,1,-1,test,"TEST");
	}catch(std::logic_error& e){
		exception1 = true;	
	}
	try{
		FeeServerMap feeMap;
		feeMap.setFeeServer(-1,-1,-1,test,"TEST");
	}catch(std::logic_error& e){
		exception1 = true;	
	}
	try{
		FeeServerMap feeMap(1,0,1);
	}catch(std::logic_error& e){
		exception2 = true;
	}
	try{
		FeeServerMap feeMap(0,1,1);
	}catch(std::logic_error& e){
		exception2 = true;
	}
	try{
		FeeServerMap feeMap(1,1,0);
	}catch(std::logic_error& e){
		exception2 = true;
	}
	//test that no value can exceed the specified border
	try{
		FeeServerMap feeMap(1,2,4);
		vector<int> x,y,z;
		x.push_back(0);x.push_back(0);x.push_back(0);
		y.push_back(0);y.push_back(1);y.push_back(2);
		z.push_back(2);z.push_back(3);z.push_back(1);
		feeMap.setFeeServer(x,y,z,test,"TEST");
	}catch(std::logic_error& e){
		exception3 = true;	
	}
	try{
		FeeServerMap feeMap(1,2,4);
		vector<int> x,y,z;
		x.push_back(1);x.push_back(0);x.push_back(0);
		y.push_back(0);y.push_back(1);y.push_back(0);
		z.push_back(2);z.push_back(3);z.push_back(1);
		feeMap.setFeeServer(x,y,z,test,"TEST");
	}catch(std::logic_error& e){
		exception3 = true;	
	}
	try{
		FeeServerMap feeMap(1,2,4);
		vector<int> x,y,z;
		x.push_back(0);x.push_back(0);x.push_back(0);
		y.push_back(0);y.push_back(1);y.push_back(0);
		z.push_back(2);z.push_back(3);z.push_back(1);
		feeMap.setFeeServer(x,y,z,test,"TEST");
	}catch(std::logic_error& e){
		exception3 = false;	
	}
	if(exception1&&exception2&&exception3 == true){
		nRet = true;
	}

	return nRet;

}
bool FeeServerMapTest::getServer(){

	bool nRet = false;
	bool exception1 = false;
	bool exception2 = false;
	bool exception3 = false;
	bool exception4 = false;
	bool exception5 = false;
	bool exception6 = false;

	
	vector<char*> resultSet;
	vector<char*> test;
	test.push_back("test");
	test.push_back("test1");
	test.push_back("test2");
	vector<int> x,y,z;
	x.push_back(0);x.push_back(0);x.push_back(0);
	y.push_back(0);y.push_back(1);y.push_back(0);
	z.push_back(2);z.push_back(3);z.push_back(1);

	
	FeeServerMap feeMap(1,2,4);
	feeMap.setFeeServer(x,y,z,test,"TEST");

	try{
		resultSet = feeMap.getFeeServer(1,0,0,"TEST");
	}catch(std::logic_error& e){
		exception1 = true;
	}
	resultSet.clear();
	resultSet = feeMap.getFeeServer(0,0,0,"TST");
	if (resultSet.empty()){
		exception2 = true;
	}
	resultSet.clear();
	resultSet = feeMap.getFeeServer(0,0,2,"TEST");
	if ( strcmp(resultSet.at(0),"test")== 0){
		exception3 = true;
	}
	resultSet.clear();
	resultSet = feeMap.getFeeServer(0,1,3,"TEST");
	if ( strcmp(resultSet.at(0),"test1")== 0){
		exception4 = true;
	}
	resultSet.clear();
	resultSet = feeMap.getFeeServer(0,0,1,"TEST");
	if ( strcmp(resultSet.at(0),"test2")== 0){
		exception5 = true;
	}

	if (exception1&&exception2&&exception3&&exception4&&exception5 == true){
		nRet = true;
	}
	return nRet;
}
bool FeeServerMapTest::broadCasting(){

	bool nRet = false;
	bool exception1 = false;
	bool exception2 = false;
	bool exception3 = false;
	bool exception4 = false;
	bool exception5 = false;
	bool exception6 = false;

	
	vector<char*> resultSet;
	vector<char*> test;
	test.push_back("test");
	test.push_back("test1");
	test.push_back("test2");
	vector<int> x,y,z;
	x.push_back(0);x.push_back(0);x.push_back(0);
		y.push_back(0);y.push_back(1);y.push_back(0);
		z.push_back(2);z.push_back(3);z.push_back(1);
	//vector<int> y = {0,1,0};
	//vector<int> z = {2,3,1};

	
	FeeServerMap feeMap(1,2,4);
	feeMap.setFeeServer(x,y,z,test,"TEST");

	resultSet = feeMap.getFeeServer(-1,0,1,"TEST");
	if (strcmp(resultSet.at(0),"test2")==0){
		exception1 = true;
	}
	resultSet.clear();
	resultSet = feeMap.getFeeServer(0,0,-1,"TEST");
	if ((strcmp(resultSet.at(0),"test2")== 0)&&(strcmp(resultSet.at(1),"test")== 0)){
		exception2 = true;
	}
	resultSet.clear();
	resultSet = feeMap.getFeeServer(0,-1,-1,"TEST");
	if ((strcmp(resultSet.at(0),"test2")==0)&&(strcmp(resultSet.at(1),"test")==0)&&(strcmp(resultSet.at(2),"test1")==0)){
		exception3 = true;
	}
	resultSet.clear();
	resultSet = feeMap.getFeeServer(-1,-1,-1,"TEST");
	if ((strcmp(resultSet.at(0),"test2")==0)&&(strcmp(resultSet.at(1),"test")==0)&&(strcmp(resultSet.at(2),"test1")==0)){
		exception4 = true;
	}
	resultSet.clear();
	resultSet = feeMap.getAllFeeServer();
	if ((strcmp(resultSet.at(0),"test2")==0)&&(strcmp(resultSet.at(1),"test")==0)&&(strcmp(resultSet.at(2),"test1")==0)){
		exception5 = true;
	}

	if(exception1&&exception2&&exception3&&exception4&&exception5 == true){
		nRet = true;
	}

	return nRet;
}
bool FeeServerMapTest::testAll(){
	
	bool nRet = true;
	printf("\nTesting FeeServerMap: \n");

	if(!setServer()){
		nRet = false;
		printf("Test: setServer() failed!\n");
	}else{
		printf("Test: setServer() successful!\n");
	}
	if(!getServer()){
		nRet = false;
		printf("Test: getServer() failed!\n");
	}else{
		printf("Test: getServer() successful!\n");
	}
	if(!broadCasting()){
		nRet = false;
		printf("Test: broadCasting() failed!\n");
	}else{
		printf("Test: broadCasting() successful!\n");
	}

	return nRet;
}
#endif