#include "Main.hpp"
#include "InterCom.hpp"
#ifdef __UTEST
#include "FeePacketTest.hpp"
#include "FedMessengerTest.hpp"
#include "LoggerTest.hpp"
#include "FeeServerMapTest.hpp"
#include "PropertyTest.hpp"
#include "DataAccessTest.hpp"
#include "RegExprTest.hpp"
#include "SetGroupedChannelTest.hpp"
#endif
#include <iostream>


//____________________________________________________________________
ztt::dcs::Main::Main(int argc, char** argv) 
{
  std::cout << "\n\t InterComLayer version: " 
	    << INTERCOM_VERSION << "\n" << std::endl;
  std::string configDir = CONFIGDIR;
  for (int i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'u': mUTests   = argv[++i]; break; 
      case 'c': configDir = argv[++i]; break;
      case 'h': 
	std::cout << "Usage: " << argv[0] << " [OPTIONS]\n\n"
		  << "Options:\n" 
		  << "\t-u TESTS\tUnit tests to run\n" 
		  << "\t-c DIR\tConfiguration directory\n"
		  << "\t-h\t\tThis help\n" 
		  << std::endl;
      }
    }
  }
  InterCom::setConfigDir(configDir);
  mIcLayer = InterCom::createInterCom();
}

//____________________________________________________________________
void
ztt::dcs::Main::unitTest(const std::string& what) 
{
  
  // This part is only, for running the unit-tests.
#ifdef __UTEST
  std::cout << "Starting the U-Tests ... !\n" << std::endl;
  int input=1;

  if (what.find("feepacket") != std::string::npos) {
    FeePacketTest ct;
    ct.testAll();
  }
      
  if (what.find("fedmessenger") != std::string::npos) {
    FedMessengerTest fmt;
    fmt.testAll();
  }
      
  if (what.find("logger") != std::string::npos) {
    LoggerTest lg;
    lg.testAll();
  }

  if (what.find("feeservermap") != std::string::npos) {      
    FeeServerMapTest fsc;
    fsc.testAll();
  }
      
  if (what.find("property") != std::string::npos) {
    PropertyTest pt;
    pt.testPropertyAll();
  }

  if (what.find("dataaccess") != std::string::npos) {
    DataAccessTest dt;
    dt.testAll();
  }
      
  if (what.find("regexpr") != std::string::npos) {
    RegExprTest ret;
    ret.TestInputValidationAll();
  }
      
  if (what.find("groupedchannel") != std::string::npos) {
    SetGroupedChannelTest sgct;
    sgct.TestAll();
  }
      
  if (!what.empty()) {
    std::cout << "Tests finished to exit press 0"<< std::endl;
    while(input!=0) std::cin >> input; 
  }
#endif
}
    

//____________________________________________________________________
int
ztt::dcs::Main::run()
{
#ifndef UTEST
  std::cout << "Starting InterCom - Layer ..." << std::endl;
  // main functionality is enclosed with try-catch to handle
  // unexpected termination of the application and to be able to close
  // the log file
  try {
    // Setup
    mIcLayer->setUp();
    // serving condition
    mIcLayer->run();
    // clear and delete InterCom
  } 
  catch(std::logic_error& e){
    mIcLayer->setLogEntry(Msg_Alarm, SOURCE_LOCAL, (char*) e.what()); 
    if (mIcLayer != 0) mIcLayer = 0;
  } 
  catch (std::exception& ex) {
    mIcLayer->setLogEntry(Msg_Alarm, SOURCE_LOCAL, (char*) ex.what()); 
    if (mIcLayer != 0) mIcLayer = 0;
  } 
  catch (...) {
    mIcLayer->setLogEntry(Msg_Alarm, SOURCE_LOCAL, 
			 "Unknown exception occured! Terminating");
    if (mIcLayer != 0) mIcLayer = 0;
  }
  if (mIcLayer != 0) mIcLayer = 0;
  
# ifdef __DEBUG
  std::cout << "\nClearing complete, now finishing ..." << std::endl;
# endif
#endif
  return 0;
}

//____________________________________________________________________
int
ztt::dcs::Main::main(int argc, char** argv) 
{
  Main m(argc, argv);
  return m.run();
}

//
// EOF
//
