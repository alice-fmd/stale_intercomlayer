#ifndef ZTT_DCS_FED_MESSENGER_HPP
#define ZTT_DCS_FED_MESSENGER_HPP


#include "dim/dis.hxx"
#include "fee_loglevels.h"
#include "Timestamp.hpp"

#include <ctime>
#include <string>


namespace ztt { namespace dcs {

// when doing changes here: remember to change size of reserved memory for this
// this names in the different places, where FED_SERVER_NAME is used.
#ifdef TPC
#define DETECTOR "tpc\0"
#define FED_SERVER_NAME "tpc_dimfed_server"    // name of the FedServer
#define SOURCE_LOCAL "tpc_interComLayer"
#define SOURCE_FEE_CLIENT "tpc_dimfee_client"
#else
#ifdef TRD
#define DETECTOR "trd\0"
#define FED_SERVER_NAME "trd_dimfed_server"    // name of the FedServer
#define SOURCE_LOCAL "trd_interComLayer"
#define SOURCE_FEE_CLIENT "trd_dimfee_client"
#else
#ifdef FMD
#define DETECTOR "fmd\0"
#define FED_SERVER_NAME "fmd_dimfed_server"
#define SOURCE_LOCAL "fmd_interComLayer"
#define SOURCE_FEE_CLIENT "fmd_dimfee_client"
#else
#ifdef PHOS
#define DETECTOR "phos\0"
#define FED_SERVER_NAME "phos_dimfed_server"
#define SOURCE_LOCAL "phos_interComLayer"
#define SOURCE_FEE_CLIENT "phos_dimfee_client"
#else
#define DETECTOR "ZTT\0"
#define FED_SERVER_NAME "ztt_dimfed_server"    // name of the FedServer
#define SOURCE_LOCAL "ztt_interComLayer"
#define SOURCE_FEE_CLIENT "ztt_dimfee_client"
#endif
#endif
#endif
#endif

/**
 * @def DETECTOR
 * Defines the type of detector (TRD or TPC).
 */

/**
 * @def FED_SERVER_NAME
 * Defnies the servername of the FedServer, depends on the detector.
 */

/**
 * @def SOURCE_LOCAL
 * Define for message has local source.
 */

/**
 * @def SOURCE_FEE_CLIENT
 * Define for message has source in FeeClient.
 */


class FedServer;

/**
 * This struct represents the composition of a message.
 * The size of this struct depends on how many gaps the compiler inserts to 
 * fill to architecture borders. (On our first test, there seems to be gaps,
 * who have in sum 3 bytes; total amount 620 (payload 617) - border size seems 
 * to be 4 byte).  
 * 
 * 05-04-2004: -->>  Size (and so borders too) has changed ! Total size is now 
 * 620 bytes. (-> there should be no gap now.)
 */
typedef struct MessageStruct{
  
    /**
     * The type of event (info, warning, error, ...).
     */
    unsigned int eventType;

    /**
     * The detector type (TRD, TPC).
     */
    char detector[Msg_Detector_Size];

    /**
     * The origin of this message.
     */
    char source[Msg_Source_Size];

    /**
     * The message itself.
     */
    char description[Msg_Description_Size];

    /**
     * The date of the message ("YYYY-MM-DD hh:mm:ss\0").
     */
    char date[Msg_Date_Size];

    /**
     * Initial constructor for MessageStruct initializes all struct members.
     *
     * @param detectorName three character long string representing name of
     *          detector.
     */
    MessageStruct(char* detectorName) {
        Timestamp now;

        eventType = Msg_Info;
        memcpy(detector, detectorName, Msg_Detector_Size);
        strcpy(source, "FedServer");
        strcpy(description, "FedServer started");

        // set the proper date with Timestamp struct
        strcpy(date, now.date);

    }
	MessageStruct(const MessageStruct &orig){
		eventType = orig.eventType;
		memcpy(detector,orig.detector,Msg_Detector_Size);
		memcpy(source,orig.source,Msg_Source_Size);
		strcpy(description,orig.description);
		memcpy(date,orig.date,Msg_Date_Size);
	}

    /**
     * Constructor for MessageStruct which allows to set all members.
     * This Contructor will be mainly used by the logger, to prepare a message
     * for the FedMessenger.
     * 
     * @param evType the type of the event (for details: @see FedMessenger).
     * @param detectorName the name of the detector (size 4 byte).
     * @param origin the source, where message originally comes from 
     *      (size 256 byte).
     * @param descript the description of the event (size 256 byte).
     * @param eventDate the date, when event occured (size ??? byte).
     */
    MessageStruct(unsigned int evType, char* detectorName, char* origin, char* descript, 
            char* eventDate) {
        eventType = evType;
        memcpy(detector, detectorName, Msg_Detector_Size);
        strcpy(source, origin);
		strcpy(description, descript);
        strcpy(date, eventDate);
    }

    /**
     * Assignment operator for MessageStructs.
     * All values are copied.
     *
     * @param rhs the struct, which values should be copied to this MessageStruct
     *
     * @return the updated MessageStruct
     */
    MessageStruct& operator=(const MessageStruct& rhs) {
        if (this == &rhs) {
            return *this;
        }
        eventType = rhs.eventType;
        memcpy(detector, rhs.detector, Msg_Detector_Size);
        memcpy(source, rhs.source, Msg_Source_Size);
        memcpy(description, rhs.description, Msg_Description_Size);
        memcpy(date, rhs.date, Msg_Date_Size);
        return *this;
    }

} MessageStruct;


/**
 * This class is responsible for delivery of messages of all loglevels
 * to the upper layers.
 * They are published all via one DimService. The structure of the log-
 * information is:<br> <ul>
 * <li> Eventtype (1 Integer): <ul> <li> info [1] </li> <li> warning [2] </li> 
 * <li> error [4] </li> <li> failure audit [8] </li> <li> success audit [16] </li>
 * <li> debug [32] </li> <li> alarm [64] </li></ul></li>
 * <li> Detector (4 Characters): <ul><li>TRD</li> <li>TPC</li></ul> </li>
 * <li> Origin (256 Characters): path to source of message. </li>
 * <li> Description (256 Characters): concern of message. </li>
 * <li> Date (Msg_Date_Size Characters): structure of date is not yet finally 
 *  defined.</li></ul>
 * The logmessages are received from the Logger, the FedMessenger keeps a copy of
 * the current log-entry. The FedMessenger inherits the publishing facility from
 * its base class (DimService).
 *
 * !! Eventually working on a struct may cause errors in member access due to 
 * unpredictable distance between members in memory.
 * !! See "Large-Scale C++ Software Design" by John Lakos (Addison-Wesley)
 * 
 * @date 4-12-2003
 *
 * @author Christian Kofler, Sebastian Bablok
 */
 class FedMessenger : public DimService{
public:
    /**
     * Constructor for the FedMessenger.
     * 
     * @param detectorName the name of the detector
     * @param serviceName the name of this service
     * @param server pointer to the hosting FedServer
     */
    FedMessenger(char* detectorName, char* serviceName, FedServer* server)
            : DimService(serviceName, MSG_DIM_STRUCTURE, &mMessage,
            sizeof(MessageStruct)), mMessage(detectorName), mpServer(server) {};

    /**
     * Destructor for FedMessenger.
     */
    virtual ~FedMessenger();

    /**
     * Sets a new message for this message - service and updates the service.
     *
     * @param pMsg pointer to a structure, containing the new message.
     */
    void setMessage(MessageStruct* pMsg);

    /**
     * Sets the desired logging level. Only messages of the this level or above
     * will be delivered further. (NOTE: The log level Msg_Alarm is always set) 
     *
     * @param level the desired log-level.
     */
    void setLogLevel(unsigned int level);

#ifdef __UTEST
    /** 
     * Getter for the message struct. 
     * For now only used and only available in unit test 
     * (compiled with -D__UTEST).
     *
     * @return pointer to the message struct.
     */
    MessageStruct* getMessage();
#endif


private:
    /**
     * The current message.
     */
    MessageStruct mMessage;

    /**
     * Pointer to the hosting FedServer.
     */
    FedServer* mpServer;

    /**
     * The current log - level.
     */
    unsigned int mLogLevel;

};


} } //end namespaces

#endif
