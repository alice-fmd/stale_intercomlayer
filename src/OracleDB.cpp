#include "OracleDB.hpp"
#include "DataAccess.hpp"
#include "Logger.hpp"
#include <string.h>
#include <iostream>

#ifndef HAVE_ORACLE
#include <sstream>
#include <stdexcept>
#define NO_ORACLE_MSG(X) do { \
  std::stringstream s;        \
  s << X << ": Oracle database not supported!"; \
  throw std::runtime_error(s.str().c_str()); } while (true)

using namespace ztt::dcs;
OracleDB::OracleDB() 
{ 
  NO_ORACLE_MSG("CTOR"); 
}
int OracleDB::createConnection() 
{ 
  NO_ORACLE_MSG("createConnection"); 
  return 0;
}
int OracleDB::getDataFromDB(char*,std::vector<char*>*)
{
  NO_ORACLE_MSG("getDataFromDB"); 
  return 0;
}
int OracleDB::getDataFromDB(char*,
			    std::vector<char*>*,
			    std::vector<int>*,
			    std::vector<int>*,
			    std::vector<int>*)
{
  NO_ORACLE_MSG("getDataFromDB"); 
  return 0;
}
int OracleDB::killConnection() { return 0; }
int OracleDB::setData(char *,char*)
{
  
  NO_ORACLE_MSG("setData"); 
  return 0;
}
#else
# include "occi.h"
using namespace oracle::occi;
using namespace std;
using namespace ztt::dcs;

OracleDB::OracleDB()
{
	//singleton call
	dataAccess = DataAccess::createDataAccess();
}
int OracleDB::createConnection()
{
	maxCon = 5;
	minCon = 1;
	incCon = 2;
	
	//create oracle environment and the connection pool with content from propertyfile
	try{
		env = Environment::createEnvironment();
		conPl = env->createConnectionPool(dataAccess->userName,dataAccess->password,dataAccess->database,minCon,maxCon,incCon);

	}catch(SQLException& e){
		char desc[256];
#ifdef __DEBUG
        cout << "Exception:" << e.what() << endl;
#endif
        strncpy(desc, e.what(), 255);
        desc[255] = 0;
        Logger::getLogger()->createLogMessage(Msg_Error, INTERCOM_DATA_ACCESS,  
                desc);
		return 0;
	}
	
	return 1;
}
int OracleDB::getDataFromDB(char* tablename,std::vector<char*>* collection,std::vector<int>* posX,std::vector<int>* posY,std::vector<int>* posZ)
{	
 	
	//int pos[3]={0,0,0};
	char statement[150];
	//create statement to load values from database
	statement[sprintf(statement, "Select * From %s",tablename)] = 0;
	try{
		//get one connection from the connection pool
		con = conPl->createConnection(dataAccess->userName,dataAccess->password);
		stm = con->createStatement(statement);
		rs = stm->executeQuery();
		//while content in specific table get content
		while (rs->next()){
			string tmp="";
			//load string from first column
			//cout<<rs->getString(1);
			tmp=rs->getString(1);
			char *name = new char[(tmp.length() + 1)];
			//copy the actual string content to the char* and finish with 0
			tmp.copy(name,string::npos);
			name[tmp.length()] = 0;
			//write result to result vector
			collection->push_back(name);
			//load the position of the server from the database
			//and store it in vectors
			posX->push_back(rs->getInt(2));
			posY->push_back(rs->getInt(3));
			posZ->push_back(rs->getInt(4));
			
		}
		//close all created database values
		stm->closeResultSet(rs);
		con->terminateStatement(stm);
		conPl->terminateConnection(con);

	}catch(exception& e)
	{
	   char desc[256];
#ifdef __DEBUG
        cout << "Exception:" << e.what() << endl;
#endif
        strncpy(desc, e.what(), 255);
        desc[255] = 0;
        Logger::getLogger()->createLogMessage(Msg_Error, INTERCOM_DATA_ACCESS,  
                desc);
	}
	catch(...)
	{
#ifdef __DEBUG
		cout << "Unknown exception occured";
#endif
	}

	return collection->size();
}
int OracleDB::getDataFromDB(char* tablename,std::vector<char*>* collection)
{	
	char statement[150];
	//create statement to load values from database
	statement[sprintf(statement, "Select * From %s",tablename)] = 0;
	try{
		//get one connection from the connection pool
		con = conPl->createConnection(dataAccess->userName,dataAccess->password);
		stm = con->createStatement(statement);
		rs = stm->executeQuery();
		//while content in specific table get content
		while (rs->next()){
			string tmp="FRITZ";
			//load string from first column
			//cout<<rs->getString(1);
			tmp=rs->getString(1);
			char *name = new char[(tmp.length() + 1)];
			//copy the actual string content to the char* and finish with 0
			tmp.copy(name,string::npos);
			name[tmp.length()] = 0;
			//write result to result vector
			collection->push_back(name);
		}

		stm->closeResultSet(rs);
		con->terminateStatement(stm);
		conPl->terminateConnection(con);

	}catch(exception& e)
	{
	   char desc[256];
#ifdef __DEBUG
        cout << "Exception:" << e.what() << endl;
#endif
        strncpy(desc, e.what(), 255);
        desc[255] = 0;
        Logger::getLogger()->createLogMessage(Msg_Error, INTERCOM_DATA_ACCESS,  
                desc);
	}
	catch(...)
	{
#ifdef __DEBUG
		cout << "Unknown exception occured";
#endif
	}

	return collection->size();
}

int OracleDB::killConnection(){
	//terminate connection pool and environment
	try{
		env->terminateConnectionPool(conPl);
		Environment::terminateEnvironment(env);
	}catch(SQLException& e){
	   char desc[256];
#ifdef __DEBUG
        cout << "Exception:" << e.what() << endl;
#endif
        strncpy(desc, e.what(), 255);
        desc[255] = 0;
        Logger::getLogger()->createLogMessage(Msg_Error, INTERCOM_DATA_ACCESS,  
                desc);
	}
	catch(...){
#ifdef __DEBUG
		cout << "Unknown exception occured";
#endif
	}

	return 1;
}
int OracleDB::setData(char *tablename,char*data)
{
	//not implemented yet cause no one knowes the final structure of the database 
	return 0;
}
#endif
