#include "InterCom.hpp"
#include "FeeClient.hpp"
#include "FedServer.hpp"
#include "DataAccess.hpp"
#include "FeePacket.hpp"

#ifdef __DEBUG
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#endif

#include <iostream>
#include <boost/shared_ptr.hpp>
#include <boost/scoped_ptr.hpp>

#include <fstream>


//#ifdef WIN32
#include <signal.h>
//#endif

using namespace std;
using namespace ztt::dcs;

InterCom* InterCom::pInstance = 0;
std::string configDir;

void      InterCom::setConfigDir(const std::string& config)
{
  configDir = config;
}

InterCom* InterCom::createInterCom() {
    if (pInstance == 0) {
        pInstance = new InterCom();
    }
	//!!!something went wrong while tidy up!!!
	//static InterCom pInstance;
    return pInstance;
}


InterCom::InterCom():mutex(),mutexFeePack() {
	
	mClient.reset(new FeeClient(this));
	mDataAx.reset(DataAccess::createDataAccess());
	mFedServer.reset(new FedServer(this, mDataAx));	
    mLogger.reset(Logger::createLogger(mFedServer->getMessenger()));
	
#	if defined(TPC) || defined(FMD) || defined(PHOS) // TPC specific initialization
	mpIBC = 0;
#	endif
}

InterCom::~InterCom() {
    // leave destructor, if already destroyed
    if (pInstance == 0) {
        return;
    }
	mutex.remove();
	mutexFeePack.remove();

	mDataAx.reset();
	// delete FeeItemInfo objects
	if (!mFeeItemInfoCollection.empty()) {
		vector<FeeItemInfo*>::iterator itInfo = mFeeItemInfoCollection.begin();
		while (itInfo != mFeeItemInfoCollection.end()) {
			if (*itInfo != 0) {
				delete *itInfo;
			}
			++itInfo;
		}
		
		mFeeItemInfoCollection.clear();
	}

    // delete FeeAckInfo objects 
		
    if (!mFeeAckInfoCollection.empty()) {
		vector<FeeAckInfo*>::iterator itInfo = mFeeAckInfoCollection.begin();
		while (itInfo != mFeeAckInfoCollection.end()) {
			if (*itInfo != 0) {
				delete *itInfo;
			}
			++itInfo;
		}

		mFeeAckInfoCollection.clear();
	}

    // delete FeeMsgInfo objects 

    if (!mFeeMsgInfoCollection.empty()) {
		vector<FeeMsgInfo*>::iterator itInfo = mFeeMsgInfoCollection.begin();
		while (itInfo != mFeeMsgInfoCollection.end()) {
			if (*itInfo != 0) {
				delete *itInfo;
			}
			++itInfo;
		}

		mFeeMsgInfoCollection.clear();
	}

	// delete service Names
	if (!mServiceCol.empty()) {
		vector<char*>::iterator itServ = mServiceCol.begin();
		while (itServ != mServiceCol.end()) {
			if (*itServ != 0) {
				delete[] *itServ;
			}
			++itServ;
		}

		mServiceCol.clear();
	}



    // set static object to 0
  pInstance = 0;


}

#ifdef __DEBUG
void InterCom::sendCommandMU() {

	char commandTarget[100];
    char input;
    int choice;
    FlagBits flagBits = NO_FLAGS;
	char* commandData = 0;
    char filename[30];
    bool checksumFlag = false;
    unsigned long count = 0;
    FILE* hFile = 0;


	cout << "Insert name of the Server (max. 80 chars): ";
	fflush(stdin);
	cin >> commandTarget;
    fflush(stdin);
    // check, if server is in subscribed list
    if (!validateServerName(commandTarget)) {
        cout << "Unknown server or no link to server.\n Please stand by." << endl;
        return;
    }
	strcat(commandTarget, "_Command\0");

    cout << "Do you want to use a checksum (Y/N)? ";
    cin >> input;
    fflush(stdin);
    input = toupper(input);
    if (input == 'Y') {
        checksumFlag = true;
    }
	
	//system(CLEAR_SCREEN);

	do {
		commandData = 0;
		count = 0;
		flagBits = NO_FLAGS;
		choice = 0;
		//-- menu with selection of command --
		cout << endl << "----- target is: " << commandTarget << " ----------"  << endl;
		cout << "Choose a command to send:" << endl;
		cout << "-  1 - Send instructions from a file to the server.\n";
		cout << "-  2 - Restart FeeServer.\n";
		cout << "-  3 - Reboot DCS board.\n";
		cout << "-  4 - Shutdown DCS board.\n";
		cout << "-  5 - Exit FeeServer.\n";
		cout << "-  6 - Set Deadband for a specified service.\n";
		cout << "-  7 - Get Deadband of a specified service.\n";
		cout << "-  8 - Set timeout for watchdog (while exec command in CE) [ms].\n";
		cout << "-  9 - Get timeout of watchdog (while exec command in CE) [ms].\n";
		cout << "- 10 - Set update rate for 'deadband checker' [ms].\n";
		cout << "- 11 - Get update rate of 'deadband checker' [ms].\n";
		cout << "- 12 - Set loglevel in FeeServer.\n";
		cout << "- 13 - Get configuration of FeeServer. [NOT implmented yet]\n";
#		if defined(TPC) || defined(FMD) || defined(PHOS)
		cout << "- 14 - Send ALTRO instruction as broadcast (same for all)\n";
		cout << "- 15 - Send instruction to only ONE ALTRO\n";
		cout << "- 16 - Send Board Controller instruction as broadcast (same for all)\n";
		cout << "- 17 - Send instruction to only ONE Board Controller" << endl;
		cout << "- 18 - Send pedestal memory block for one ALTRO as broadcast" << endl; 
        cout << "- 19 - Send individual pedestals to one ALTRO" << endl;
        cout << endl;
#		endif
		cout << "-  0 - Leave menu." << endl;
		cout << "Command: " << endl;
		fflush(stdin);
		cin >> choice;
		if(!cin.fail()){
		system(CLEAR_SCREEN);

		// check which command to execute
		switch (choice) {
			case (0): {
				// leave this menu
				return;
			}
			case (1): {
				system(CLEAR_SCREEN);
				cout << "Insert filename with instruction data: ";
				fflush(stdin);
				cin >> filename;
				fflush(stdin);

				// maybe use here a more C++ like file operation ?!
				// this C like works so far
				hFile = fopen(filename, "rb");
				if (!hFile) {
					cout << "ERROR opening file! \nPlease stand by.\n";
					continue;
				}
				while (getc(hFile) != EOF) {
					++count;
				}
				fclose(hFile);

				if (count >= 5300000) {
					cout << "File to big, not sending any data (max. 5 MB) !" << endl << endl;
					continue;
				}
				commandData = new char[count];

				hFile = fopen(filename, "rb");
				fread(commandData, sizeof(char), count, hFile);
				fclose(hFile);
				break;
			}
			case (2): {
				flagBits = FEESERVER_RESTART_FLAG;
				break;
			}
			case (3): {
				flagBits = FEESERVER_REBOOT_FLAG;
				break;
			}
			case (4): {
				flagBits = FEESERVER_SHUTDOWN_FLAG;
				break;
			}
			case (5): {
				flagBits = FEESERVER_EXIT_FLAG;
				break;
			}
			case (6): {
				char serviceName[50];
				float deadband = 0.0;
				flagBits = FEESERVER_SET_DEADBAND_FLAG;
				system(CLEAR_SCREEN);
				cout << "insert service name (WITHOUT leading server name!): ";
				fflush(stdin);
				cin >> serviceName;
				fflush(stdin);
				cout << "insert new deadband for service " << serviceName << ": ";
				fflush(stdin);
				cin >> deadband;
				fflush(stdin);

				count = sizeof(float) + strlen(serviceName);

				commandData = new char[count];
				memcpy(commandData, &deadband, sizeof(float));
				memcpy(commandData + sizeof(float), serviceName, strlen(serviceName));
				break;
			}
			case (7): {
				char serviceName[50];
				flagBits = FEESERVER_GET_DEADBAND_FLAG;
				system(CLEAR_SCREEN);
				cout << "insert service name (WITHOUT leading server name!): ";
				fflush(stdin);
				cin >> serviceName;
				fflush(stdin);

				count = strlen(serviceName);

				commandData = new char[count];
				memcpy(commandData, serviceName, strlen(serviceName));
				break;
			}
			case (8): {
				unsigned long timeout = 0;
				flagBits = FEESERVER_SET_ISSUE_TIMEOUT_FLAG;
				system(CLEAR_SCREEN);
				cout << "insert new timeout for watchdog in " << commandTarget <<
						"[ms]: ";
				fflush(stdin);
				cin >> timeout;
				fflush(stdin);

				count = sizeof(unsigned long);

				commandData = new char[count];
				memcpy(commandData, &timeout, sizeof(unsigned long));
				break;
			}
			case (9): {
				flagBits = FEESERVER_GET_ISSUE_TIMEOUT_FLAG;
				break;
			}
			case (10): {
				unsigned short updateRate = 0;
				flagBits = FEESERVER_SET_UPDATERATE_FLAG;
				system(CLEAR_SCREEN);
				cout << "insert new update rate for deadband checker in " <<
						commandTarget << "[ms]: ";
				fflush(stdin);
				cin >> updateRate;
				fflush(stdin);

				count = sizeof(unsigned short);

				commandData = new char[count];
				memcpy(commandData, &updateRate, sizeof(unsigned short));
				break;
			}
			case (11): {
				flagBits = FEESERVER_GET_UPDATERATE_FLAG;
				break;
			}
			case (12): {
				unsigned int loglevel = 0;
				char msg[500];
				flagBits = FEESERVER_SET_LOGLEVEL_FLAG;
				system(CLEAR_SCREEN);

				cout << "Possible log level types:" << endl << endl;
				msg[sprintf(msg,
					"INFO [%3d], WARNING [%3d], ERROR [%3d], FAILURE AUDIT [%3d],",
					Msg_Info, Msg_Warning, Msg_Error, Msg_Failure_Audit)] = 0;
				cout << msg << endl;
				msg[sprintf(msg,
					"SUCCESS AUDIT [%3d], DEBUG [%3d], ALARM[%3d].\n",
					Msg_Success_Audit, Msg_Debug, Msg_Alarm)] = 0;

				cout << msg << endl << "Add numbers to combine levels." << endl << endl;
				cout << "insert new loglevel for server " << commandTarget << ": ";
				fflush(stdin);
				cin >> loglevel;
				fflush(stdin);

				count = sizeof(unsigned int);

				commandData = new char[count];
				memcpy(commandData, &loglevel, sizeof(unsigned int));
				break;
			}
			case (13): {
				cout << endl << "Sorry, this feature is not implemented yet." <<
						endl << endl;
				continue;
				break;
			}
			default: {
				cout << "Command does not exist!" << endl;
				continue;
			}
		}

		boost::shared_ptr<FeePacket> pFeePacket =
				createFeePacket(commandData, count, checksumFlag, flagBits);

	//	cout << "Trying to send: " << commandData << endl;

		if (count > 0) {
			cout << "sending Instructions of " << count << " bytes." << endl;
		}
		cout << "Packet ID: " << sendCommand(commandTarget, pFeePacket) << endl << endl;

		delete[] commandData;
        // if FeeServer exited or shutdown, switch to main menu
        if ((choice == 4) || (choice == 5)) {
            return;
        }
	}else {
		cout << "Command does not exist!" << endl;
		cin.clear();
		string str;
		cin >> str;
	}
		// sleep to wait for acknowledges in menue of mock up
		sleep(5);
	} while (choice != 0);
}
#endif

int InterCom::sendCommand(char* target, boost::shared_ptr<FeePacket> command) {
	//lock critical area
	ACE_Guard<ACE_Thread_Mutex> guard(mutex);
	int nRet;
	char* pMarshalledFeePacket = command->marshall();
#ifdef __BENCHMARK
	try{
		char message[256];
		const Logger* logger = Logger::getLogger();
		ACE_Time_Value ts = ACE_OS::gettimeofday();
		struct timeval tStamp = ts.operator timeval();;
		struct tm *ptimes = localtime(&(tStamp.tv_sec));

		message[sprintf(message,"Benchmark ICL Timestamp(Send Command) ID:%d, Target:%s, Time:%.8s - %ld",command->getId(),
			target, asctime(ptimes) + 11, tStamp.tv_usec)]=0;
		logger->createLogMessage(Msg_Debug,SOURCE_LOCAL,message);
		
	}catch(std::runtime_error &excep){
		cerr << "Exception in SendCommandBenchmark: " << excep.what() << endl;
	}
#endif

    // using the non blocking version, because it is called inside a callback
    // routine, (inside callbacks the retVal of sendCommand(...) is not reliable)
    // using the blocking version inside callback would use sendCommandNB(...)
    // anyway !
	mClient->sendCommandNB(target, (void*) pMarshalledFeePacket,
            command->getFeePacketSize());

    nRet = command->getId();
	
    // set log message
 /*   char description[100];
    description[sprintf(description,
            "Send command to '%s' with packet id: %d.", target, nRet)] = 0;
    mLogger->createLogMessage(Msg_Debug, SOURCE_FEE_CLIENT, description);
*/
	// release memory for marshalled command, which is allocated
    // within command->marshall()
	delete[] pMarshalledFeePacket;

	return nRet;
}

boost::shared_ptr<FeePacket> InterCom::createFeePacket(char* payload, int size,
         bool checksumFlag, FlagBits feeServerFlag,  bool huffmanFlag) {
	//locking the feepacket creation part
	ACE_Guard<ACE_Thread_Mutex> guard(mutexFeePack);

	boost::shared_ptr<FeePacket> pFeePacket(new FeePacket(FEE_PACKET_NO_ERRORCODE,
            payload, size, checksumFlag, feeServerFlag, huffmanFlag));

	// !!! later, if huffman and/or checksum is wished this has to be changed !!!
	return pFeePacket;
}
boost::shared_ptr<FeePacket> InterCom::createBroadCastFeePacket(char* payload, int size,
		bool checksumFlag, FlagBits feeServerFlag, vector<char*>* feeserverContainer, bool huffmanFlag) {
	//locking the feepacket creation part
	ACE_Guard<ACE_Thread_Mutex> guard(mutexFeePack);

	boost::shared_ptr<FeePacket> pFeePacket(new FeePacket(FEE_PACKET_NO_ERRORCODE,
            payload, size, feeserverContainer, checksumFlag, feeServerFlag, huffmanFlag));

	// !!! later, if huffman and/or checksum is wished this has to be changed !!!
	return pFeePacket;
}

int InterCom::executeCommand(char* data, int size) {
    int nRet = INTERCOM_SUCCESS;
    // set log entry
    char msg[35];
    msg[sprintf(msg, "InterCom received a command.")] = 0;
    mLogger->createLogMessage(Msg_Debug, SOURCE_LOCAL, msg);

    // interpret command and execute it afterwards !!!
    // has to be implented ??? !!!

    return nRet;
}

void InterCom::setUp() {

	int input = 0;
    int xSize = 0;
    int ySize = 0;
    int zSize = 0;
#ifdef __DEBUG
    char filename[30];
#endif
    int nRet;
	//int count = 0;
	vector<int> xCoordinate;
	/** collection for the server names*/
	std::vector<char* > serverCol;

	mDataAx->setConfigDir(configDir);
	nRet = mDataAx->loadFiles(&serverCol,&mServiceCol);
#ifdef __DEBUG
	if(nRet <= 0)
	{
		do {
			char desc[50];
			cout << "Load FeeServer names from data source (filename, max 30 chars).";
			cout << endl;
			cout << "Enter filename/tablename \"exit\" to quit program." << endl;
			fflush(stdin);
			cin >> filename;
			if (strcmp(filename, "exit") == 0) {
	            exit(0);
			}
			//!!!decision between load Server from File or Db
			nRet = mDataAx->fillCollectionFromFile(filename, &serverCol);
			//nRet = mDataAx->fillCollectionWithNames(filename, &serverCol);
			//nRet = mDataAx->createConnection();
			//nRet = mDataAx->getData(filename, &serverCol);
			//nRet = mDataAx->killConnection();
			// ??? is Logger for log file ready at this point ??? check !!!
			desc[sprintf(desc, "Retrieved %d FeeServer names.", nRet)] = 0;
			mLogger->createLogMessage(Msg_Info, SOURCE_LOCAL, desc);
			if (nRet <= 0) {
				cout << "Loading of configuration failed !" << endl;
				cout << "Try again." << endl;
			}
		
		} while (nRet <= 0);
	
			//system(CLEAR_SCREEN);
		cout << nRet << " servernames added to list." << endl << endl;
		cout << "Insert the max size of the 3 dimensions of the detector." << endl;
		cout << "(TPC: side, segment, sector; TRD: supermodule, stack, layer)";
		cout << endl << endl;
		cout << "If you are uncertain insert 0 -> you receive a flat structure" << endl;
		cout << "The same happens automatically, if the sizes don't match" << endl;
		cout << "with the amount of given FeeServer names." << endl << endl;
		cout << "X (side, supermodule): ";
		cin >> xSize;
		cout << "Y (sector, stack): ";
		cin >> ySize;
		cout << "Z (segment, layer): ";
		cin >> zSize;
		if ((xSize * ySize * zSize) != serverCol.size()) {
	        xSize = 1;
			ySize = 1;
			zSize = serverCol.size();
			cout << "Sizes don't match, reseting size to flat structure." << endl;
			cout << "X and Y are 1, Z is " << zSize << endl;
		}
		try{
			mServerMap.setFeeServer(xSize,ySize,zSize,serverCol,"FEE");
		}catch(std::logic_error& e){
			char text[100];
			strcpy(text,(char*)e.what());
			mLogger->createLogMessage(Msg_Error, SOURCE_LOCAL, text);
			exit(0);
		}

		while (input != 2) {
	        cout << endl;
			cout << "- 1 - Add services to list [values to monitor].\n";
			cout << "- 2 - Start InterCom - Layer.\n";
			cout << "- 3 - About InterComLayer" << endl;
			cout << "- 4 - Exit InterCom - Layer. \n";
			cout << " Command (1, 2, 3 or 4): ";
			
			cin >> input;
			if(!(cin.fail())){

				if (input == 1) {
					cout << "\nLoad FeeService names from file (filename, max 30 chars): ";
					cin >> filename;
					//!!!decision between load Services from File or Db!!!
					nRet = mDataAx->fillCollectionFromFile(filename, &mServiceCol);
					//nRet = mDataAx->fillCollectionWithNames(filename, &mServiceCol);
					system(CLEAR_SCREEN);
					if (nRet <= 0) {
					    cout << "Loading of configuration failed !" << endl;
						cout << "Try again or start InterCom - Layer." << endl << endl;
					} else {
					    cout << nRet << " services added." << endl << endl;
					}
				}
				else if (input == 3) {
		            showAbout();
				} else if (input == 4) {
				    cout << "Exiting ...\n";
					exit(0);
				}
			}else {
				cout << "Command does not exist!" << endl;
				cin.clear();
				string str;
				cin >> str;
			}
		}
	}
	else{
#endif
		//this check is required to avoid errors when using the file configuration without specifying the 
		//position of the single servers.
		if(mDataAx->valueselect != 2){
			if ((mDataAx->xValue * mDataAx->yValue * mDataAx->zValue) != serverCol.size()) {
				xSize = 1;
				ySize = 1;
				zSize = serverCol.size();
				cout << "Sizes don't match, reseting size to flat structure."<<endl;
				cout << "X and Y are 1, Z is " << zSize << endl;
			}else{
				xSize = mDataAx->xValue;
				ySize = mDataAx->yValue;
				zSize = mDataAx->zValue;
			}
			try{
				mServerMap.setFeeServer(xSize,ySize,zSize,serverCol,"FEE");
			}catch(std::logic_error& e){
				char text[100];
				strcpy(text,(char*)e.what());
				mLogger->createLogMessage(Msg_Error, SOURCE_LOCAL, text);
				exit(0);
			}
		}
#	ifdef __DEBUG
	}
#	endif
	//was used if property file exists
	
		//system(CLEAR_SCREEN);
		cout << "Starting InterCom - Layer now ... " << endl << endl;
}

bool InterCom::isDimDnsUp() {
    DimBrowser checker;
    int count = 0;
    char text[100];

    // check if enviromental variable is set
    if (getenv("DIM_DNS_NODE") == 0) {
        strcpy(text, "No enviromental variable for DIM_DNS_NODE is set.");
        mLogger->createLogMessage(Msg_Error, SOURCE_LOCAL, text);
        return false;
    }

    // try connection to DIMDNS
    while (count < CHECK_DNS_TRIES) {
        if (checker.getServers()!=0) {
            strcpy(text, "DIM_DNS_NODE found on ");
            mLogger->createLogMessage(Msg_Info, SOURCE_LOCAL,
                    strcat(text, getenv("DIM_DNS_NODE")));
            return true;
        }
        sleep(CHECK_DNS_TIMEOUT);
        ++count;
    }

    strcpy(text, "Cannot connect to DIM_DNS_NODE on ");
    mLogger->createLogMessage(Msg_Error, SOURCE_LOCAL ,
            strcat(text, getenv("DIM_DNS_NODE")));
    return false;
}


void InterCom::run(bool interactive) {
// !!! here has to be implemented a non - Debug (non - MockUp) version !!!

    // check for DIM_DNS is set and up
    if (!isDimDnsUp()) {
#       ifdef __DEBUG
        cout << "No DIM_DNS found under " << getenv("DIM_DNS_NODE") << endl;
#       endif
        return;
    }

#   ifdef __DEBUG
    // Insertion for automativ mass test section !!!
    char automat = 'N';
 /*   cout << endl;
    cout << "          --> Do you want to run an automatic test ? (Y/N) <--" << endl;
    cout << "      (!! DON'T use this feature, if you are unfamiliar with it !!)" << endl;
    cout << "(When using this feature, you have to quit the InterComLayer with CTRL-C.)"; 
    cout << endl << "(Y/N) ? ";
    cin >> automat;*/
#   endif

#   ifdef __DEBUG
    cout << "Subscribing to servers and servcies." << endl;
#   endif

    // subscribe to services
	subscribe();
	int amount = mFedServer->publishServices();

    // set log - entry about how many services has been published by FedServer
    char description[60];
    description[sprintf(description,
		"FedServer has been started with %d services.", amount)]=0;
    mLogger->createLogMessage(Msg_Success_Audit, SOURCE_LOCAL, description);

#   ifdef __DEBUG
	cout << amount << " FED-Services published" << endl << endl;
    cout << "Please use a DIM-Browser (DIM-Tree or DID) for monitoring values!"
            << endl << "Waiting for connections, please stand by." << endl << endl;

    // used for automated mass test
    while ((automat == 'Y') || (automat == 'y')) {
        doRandomCmnd();
    }
#endif	
    char expertMode = 'n';
    int input = 0;
    dtq_sleep(5);
    if (interactive) {
	while (input != 6) {
#ifdef __DEBUG
        cout << endl;
        cout << "-------------------------------------------------------------" << endl;
		cout << "- 1 - Send instructions to a server." << endl;
        cout << "- 2 - Update FeeServer (Experts only)." << endl;
        cout << "- 3 - Show all FeeServer registered by the InterComLayer." << endl;
        cout << "- 4 - Stop all FeeServer" << endl;
		cout << "- 5 - About InterComLayer" << endl;
#endif
		cout << "- 6 - Exit program." << endl;
        cout << "Command (1, 2, 3, 4 or 5): " << endl;
		cin >> input;
		if(!(cin.fail())){
#ifdef __DEBUG  
			system(CLEAR_SCREEN);
			if (input == 1) {
				// MU stands for Mock Up
				sendCommandMU();
			} else if (input == 2) {
				cout << "BE SURE TO DOWNLOAD A FULLY FUNTIONAL FeeServer !!" << endl;
				cout << "Do you really want to continue (Y/N)? ";
				cin >> expertMode;
				fflush(stdin);
				if (expertMode != 'Y') {
					continue;
				}
				updateFeeServerMU();
			} else if (input == 3) {
				showFeeServers();
			} else if (input == 4) {
				cout << "Are you sure to exit all registered FeeServer? (Y/N) " ;
				cin >> expertMode;
				fflush(stdin);
				if ((expertMode != 'Y') && (expertMode != 'y')) {
					continue;
				}
				stopAllFeeServer();
				sleep(3);
			} else if (input == 5) {
				showAbout();
			}
#endif
		}else {
			cout << "Command does not exist!" << endl;
			cin.clear();
			string str;
			cin >> str;
		}
	  }
	cout << "Preparing for EXIT ..." << endl;
    }
}


bool InterCom::validateServerName(const char* serverName) {
	//lock critical section
	ACE_Guard<ACE_Thread_Mutex> guard(mutex);

    vector<FeeAckInfo* >::iterator it;
    for (it = mFeeAckInfoCollection.begin(); it != mFeeAckInfoCollection.end(); ++it) {
        if (strcmp(serverName, (*it)->getCorrespondingServerName()) == 0) {
            if (!(*it)->hasLink()) {
                // no link to existing server             
                break;
            }
            return true;
        }
    }
    return false;
}


void InterCom::stopAllFeeServer() {
    char target[100];
    int id = 0;
    vector<char* > serverCol = mServerMap.getAllFeeServer();

    vector<char*>::iterator itServer = serverCol.begin();
	while (itServer != serverCol.end()) {
		if ((*itServer != 0) && (validateServerName(*itServer))) {
			target[sprintf(target, "%s_Command", *itServer)] = 0;
            id = sendCommand(target, createFeePacket(0, 0, true,
                    FEESERVER_EXIT_FLAG));
#           ifdef __DEBUG
            cout << "Send EXIT to FeeServer " << *itServer << ", PacketID:"
                    << id << endl;
#           endif
		}
		++itServer;
	}
}


#ifdef __DEBUG
void InterCom::showFeeServers() {
    int count = 0;
	vector<char* > serverCol = mServerMap.getAllFeeServer();

    system(CLEAR_SCREEN);
    cout << " FeeServer list:" << endl;
    cout << "Nr. \tServer \t Link status" << endl;
    cout << "-------------------------------------------------------------";
    cout << endl;
    mServerMap.print();
    vector<char*>::iterator itServer = serverCol.begin();
    while (itServer != serverCol.end()) {
        if (*itServer != 0) {
            cout << ++count << ": \t" << *itServer << " \t ";
            if (validateServerName(*itServer)) {
                cout << "+ Link established" << endl;
            } else {
                cout << "- NO Link" << endl;
            }
		}
		++itServer;
	}
    cout << "-------------------------------------------------------------";
    cout << endl;
}


void InterCom::showAbout() {
	system(CLEAR_SCREEN);
	cout << "----- About InterComLayer -----------------------------------" << endl;
	cout << "  version: " << INTERCOM_VERSION << endl << endl;
	cout << "  for information / support please contact: " << endl;
	cout << "    Center for Technology Transfer and Telecommunications" << endl;
	cout << "    at University of Applied Sciences Worms, Germany" << endl << endl;
	cout << "      Sebastian Bablok <bablok@ztt.fh-worms.de>" << endl;
	cout << "      Benjamin Schockert <schockert@ztt.fh-worms.de>" << endl;
	cout << "      (+49) 06241 - 509 284" << endl;
	cout << "      http://www.ztt.fh-worms.de" << endl;
	cout << "-------------------------------------------------------------" << endl << endl;
}
#endif

#ifdef __DEBUG


void InterCom::updateFeeServerMU() {
    char commandTarget[100];
    char filename[30];
	char* commandData;  
    unsigned long count = 0;
	
	cout << "Insert name of the Server (max. 80 chars): ";
	fflush(stdin);
	cin >> commandTarget;
    fflush(stdin);
    if (!validateServerName(commandTarget)) {
        cout << "Unknown server or no link to server. Please stand by." << endl;
        return;
    }
	strcat(commandTarget, "_Command\0");

    cout << "Insert filename of the FeeServer binary: ";
	cin >> filename;
    fflush(stdin);

    // maybe use here a more C++ like file operation ?!
    // this C like works so far
    FILE* hFile = 0;

    hFile = fopen(filename, "rb");
    if (!hFile) {
        cout << "ERROR opening file!\n";
        return;
    }
    while (getc(hFile) != EOF) {
        ++count;
    }
    fclose(hFile);

    commandData = new char[count];

    hFile = fopen(filename, "rb"); 
    fread(commandData, sizeof(char), count, hFile);
    fclose(hFile); 

    boost::shared_ptr<FeePacket> pFeePacket = 
        createFeePacket(commandData, count, true, FEESERVER_UPDATE_FLAG);

    cout << "Sent Packet for update with ID: " <<
            sendCommand(commandTarget, pFeePacket) << endl << endl;
        
    delete[] commandData;       
}
#endif


void InterCom::subscribe() {
	vector<char* > serverCol = mServerMap.getAllFeeServer();//mpServerCol3D->getAllFeeServer();
	//int count = mClient->subscribeTo(&mServerCol, &mServiceCol);
    int count = mClient->subscribeTo(&serverCol, &mServiceCol);

    // set log - entry about how many services the interCom - Layer has been
    // subcribed to
    char description[80];
    description[sprintf(description, 
            "FeeClient is subscribed to %d FeeServices (Items, ACKs and MSGs).",
			count)]=0;
    mLogger->createLogMessage(Msg_Success_Audit, SOURCE_LOCAL, description);


#ifdef __DEBUG   
	vector<char* >::iterator itAck = serverCol.begin();
	while (itAck != serverCol.end()) {
		cout << "Has subscribed to " << *itAck << " (ACK and MSG channel)." << endl;
		++itAck;
	}
	vector<char* >::iterator itServ = mServiceCol.begin();
	while (itServ != mServiceCol.end()) {
		cout << "Has subscribed to " << *itServ << endl;
		++itServ;
	}

	cout << "got " << count;
	cout << " DIM-Info objects\n";
	cout << "-------------------------------------------------------------\n";

#endif
}

#ifdef __DEBUG
void InterCom::doRandomCmnd() {
    char* nameOfServer = 0;
    char target[100];
    char payload[85] = "Etwas Text um einfach payload zu simulieren und diesen an die MockUp CE zu senden...";
    unsigned int size = 0;
    FlagBits flags;
    unsigned short dummy;
	vector<char* > serverCol = mServerMap.getAllFeeServer();

    payload[84] = 0;
    // set seed for random
    srand((unsigned) time(0));
    
    // sleep before next command
    sleep((rand() % 100));

    // decide which command
    dummy = (((unsigned short) rand()) % 50);
    if (dummy < 40) {
        flags = NO_FLAGS;
        size = 85;
        printf("1");
    } else if (dummy < 44) {
        flags = FEESERVER_GET_ISSUE_TIMEOUT_FLAG;
        payload[0] = 0;
        printf("2");
    } else if (dummy < 48) {
        flags = FEESERVER_GET_UPDATERATE_FLAG;
        payload[0] = 0;
        printf("3");
    } else {
        flags = FEESERVER_RESTART_FLAG;
        payload[0] = 0;
        printf("4");
    }

    // decide which FeeServer
    dummy = (((unsigned short) rand()) % serverCol.size());
    nameOfServer = serverCol[dummy];
    target[sprintf(target, "%s", nameOfServer)] = 0;
    
    if (validateServerName(target)) {
        strcat(target, "_Command\0");
        // create and send test packet
        sendCommand(target, createFeePacket(payload, size, true, flags));
        printf(".");
        fflush(stdout);
    } else {
        printf("|");
        fflush(stdout);
    }

}
#endif


