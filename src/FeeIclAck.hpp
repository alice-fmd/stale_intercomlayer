#ifndef FEE_ICL_ACK
#define FEE_ICL_ACK

#include "dim/dis.hxx"
#include <iostream>
#include "FedMessenger.hpp"



namespace ztt{namespace dcs{
/**
* the structure of the ICL acknowledge channel
*/
#define ACKSTRUCTURE "I:1;C:256"

/**
* the content of the acknowledge channel.
*/
typedef struct IclAck{

	int error;
	char message[256];

	IclAck(){
		error = 0;
		message[sprintf(message,"initialized")]=0;
	}

}IclAck;

/**
* the acknowledge channel of the ICL.
* the class contains the structure where the dim service points to.
* the class is a singleton to be sure that only one ack channel exists.
*
* @author Benjamin Schockert
*/
class FeeIclAck : public DimService 
{
public:
	/**
	* this function returns the actual instance
	* @return pInstance
	*/
	static FeeIclAck* createInstance();	
	/**
	* setAckStruct fill the ack structure and update the corresponding channel
	*/
	void setAckStruct(int,char*);
	virtual ~FeeIclAck(void);
	/**
	* the struct variable
	*/
	IclAck iclAck;
	/**
	* the Dim channel
	*/
	DimService* ackChannel;

private:
	FeeIclAck();
	
};
inline void FeeIclAck::setAckStruct(int errorcode, char* mess){
	iclAck.error=errorcode;
	iclAck.message[sprintf(iclAck.message,mess)]=0;
	ackChannel->updateService(&iclAck,sizeof(IclAck));
}
//end namespace
}}
#endif
