#include "ConfigureFero.hpp"
#include <iostream>
#include "Logger.hpp"
#include "RegExpr.hpp"
#include "InterCom.hpp"
#include "FeeServerMap.hpp"
#include <set>

using namespace std;
using namespace ztt::dcs;

bool ConfigureFero::handleCommand(){
	
	bool nRet = false;
	// create logger to write events to logfile or PVSS
	const Logger* logIt = Logger::getLogger();
	//check that no NULL pointer is given
	if(getData() == NULL){
		try{
			char message[256];
			message[sprintf(message,"Received null pointer from DIM in ConfigureFero, value rejected")] = 0;
			logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		return nRet;
	}
	//check that the received command isn´t empty
	if(getSize()<= 0){
		try {
            
        	char text[100];
		    text[sprintf(text,"Receive an ConfigureFero command with size <= 1, command ignored.")] = 0;
		    logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
        	} catch (std::runtime_error &excep) {
            	// Can't print and send log message,
            	// but this is not so critical (Server-functionality is not affected).
            		cerr << "Exception: " << excep.what() << endl;
      		}
		return nRet;
	}
	
	//change handling of incomming messages to allow an array of unspecified length
	int psize = (getSize()-20)/4;

	ConfFero* conffero = NULL;
	//cast incomming command to configurefero structure
	try{
		conffero = reinterpret_cast<ConfFero*>(getData());
	}catch(bad_cast& e){
		char message[256];
		message[sprintf(message,"Cast operation in ConfigureFero channel failed %s",e.what())] = 0;
		logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		return nRet;
	}
#ifdef __BENCHMARK
	try{
		char message[256];
		const Logger* logger = Logger::getLogger();
		ACE_Time_Value ts = ACE_OS::gettimeofday();
		struct timeval tStamp = ts.operator timeval();;
		struct tm *ptimes = localtime(&(tStamp.tv_sec));

		message[sprintf(message,"Benchmark ICL Timestamp(Receive ConfFero) Time:%.8s - %ld",
			asctime(ptimes) + 11, tStamp.tv_usec)]=0;
		logger->createLogMessage(Msg_Debug,SOURCE_LOCAL,message);
		
	}catch(std::runtime_error &excep){
		cerr << "Exception in ConfigureFeroBenchmark: " << excep.what() << endl;
	}
#endif
	// create regular expression instance
	RegExpr reg;
	// get singleton object of the ICL
	InterCom* icl = InterCom::createInterCom();
	// get the map where the server are stored
	FeeServerMap* pFeeMap = icl->getFeeServerMap();
	// feeservercollection store the single feeservernames
	vector<char*> feeservercollection;
	// list store the tags received from the PVSS
	list<int> tags;
	// hwType store the hardware type stored in the name to create a unique key
	char* hwType = new char[10];
	// servername store the complete target received from PVSS
	char* servername = new char[256];
	// service store the optional attached service name
	char* service = new char[256];
	//extract servername from struct
	servername[sprintf(servername,conffero->target)]=0;
	for (int i=0;i<psize;i++){
		tags.push_back(conffero->tags[i]);
	}

	//check that given name is assembled correctly
	if(!reg.inputValidation(servername)){
		try{
			char message[256];
			message[sprintf(message,"Receive strange targetname for ConfigureFero channel, command rejected")] = 0;
			logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		delete[] hwType;
		delete[] servername;
		delete[] service;

		return nRet;
	
	}
	//extract coordinates of the feeserver position
	string name = servername;
	vector<int> position;
	reg.getValues(name,&position,service,hwType);
	//map coordinates to feeserver name 
	feeservercollection = pFeeMap->getFeeServer(position.at(0),position.at(1),position.at(2),hwType);
	if (!feeservercollection.empty()){
		//create empty FeePacket to get the actual id
		FeePacket packet(&feeservercollection,tags.size());
		//store set content and key in a map in AnswerStruct
		mAnswerStruct.setAnswerStruct(packet.getId(),packet.getAnswerSet());
		localID = packet.getId();
		//store required information in send object
		send.setValues(feeservercollection,tags);
		//starting send thread
		send.activate(THR_DETACHED|THR_NEW_LWP,1,1);
		//start watchDog thread
		mWatchDog.putq(mb);
		//ACE_Thread::spawn((ACE_THR_FUNC)watchDog,(void*)this,THR_DETACHED|THR_NEW_LWP);
		//mark channel busy
		busy = true;
		nRet = true;
		
	}else{
		try {
					char text[100];
					text[sprintf(text, 
						"Unknown FeeServername received by ConfigureFero channel")] = 0;
					logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
				} catch (std::runtime_error &excep) {
				// Can't print and send log message,
				// but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
					cerr << "Exception: " << excep.what() << endl;
#endif
				}
	}
	delete[] servername;
	delete[] service;
	delete[] hwType;
	tags.clear();
	position.clear();
	feeservercollection.clear();

	return nRet;
 }

bool ConfigureFero::setAnswerData(const int& ID){
    bool bRet = false;
	
    // to avoid sending of the current result data when just subscribing to
    // service. No real answer, because no command has been sent first.
	if ((ID == 0)) {
        return bRet;
    }
	
	int ret = 0;
	ret = mAnswerStruct.findAnswerID(ID);
	if (ret == 1){
		bRet = true;
		//wake up watchdog
		//cond.signal();
		mWatchDog.wakeUp();
		//notify that command was sucsessful
		char* text = new char[256];
		Timestamp ts;
		text[sprintf(text,"ConfigureFero Ack;%s",ts.date)]=0;
		iclAck->setAckStruct(0,text);
			
		busy = false;
		delete[] text;
	}else if(ret == 2){
		bRet = true;
	}
  
    return bRet;
}

void* ConfigureFero::watchDog(void* handle){
/*    ConfigureFero* handler = reinterpret_cast<ConfigureFero*>(handle);
	//lock critical section
	handler->mutex.acquire();
    int status = 0;
	int id = handler->localID;
	
	
#ifdef __DEBUG
    cout << "ConfFero watch dog started ..." << endl;
#endif

    // init timer for watch dog
	// see examples ACE example directory  CFF_WATCH_DOG_TIMEOUT
	ACE_Time_Value timer (CFF_WATCH_DOG_TIMEOUT/ACE_ONE_SECOND_IN_MSECS,0L);
	ACE_Time_Value expire_at = ACE_OS::gettimeofday() + timer;
	status = handler->cond.wait(&expire_at);
	if(status == -1){
		//error message
		cout<<"WATCHDOGTIMEOUT in ConfigureFero occured"<<endl;
		handler->mAnswerStruct.freeAnswerStruct(id);
		//told PVSS that time out occured
		char* error = new char[256];
		error[sprintf(error,"ConfigureFero watchdog timeout")]=0;
		handler->iclAck->setAckStruct(Msg_Warning,error);
		delete[] error;
	}else{
		cout<< "watchdog stopped normally"<<endl;
	}
	handler->busy = false;
	handler->mutex.release();
		*/
	return 0;
}

ConfigureFero::~ConfigureFero(){
	mutex.remove();
	delete mb;
	ACE_Thread_Manager::instance()->cancel_task(&mWatchDog);
}
