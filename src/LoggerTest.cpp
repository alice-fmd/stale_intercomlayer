#ifdef __UTEST

#include "LoggerTest.hpp"

#include "InterCom.hpp"
#include "FedServer.hpp"
#include "FedMessenger.hpp"
#include "Timestamp.hpp"

#include <string>
//#include <sys/timeb.h>

using namespace ztt::dcs;

LoggerTest::LoggerTest() {

}

LoggerTest::~LoggerTest() {

}

bool LoggerTest::testLogger() {
    bool bRet = true;

    FedServer aTestServer(0, dA);
    Logger* aLogger = Logger::createLogger(aTestServer.getMessenger());

    if (aLogger->getLogLevel() != 
            (Msg_Alarm + Msg_Warning + Msg_Error + Msg_Info)) {
        bRet = false;
    }

    delete aLogger;

    return bRet;
}

bool LoggerTest::testCreateLogMessage() {
    bool bRet = true;
    Timestamp now;

    FedServer aTestServer(0, dA);
    FedMessenger* aFedMsg = aTestServer.getMessenger();
    Logger* aLogger = Logger::createLogger(aFedMsg);

    aLogger->createLogMessage(Msg_Alarm, "Unit-Test", "Unit - Test of Logger");
    if (aFedMsg->getMessage()->eventType != Msg_Alarm) {
        printf("Something with event - type is not alright !\n");
        bRet = false;
    }
    if (_strncoll(aFedMsg->getMessage()->detector, DETECTOR, 3) != 0) {
        printf("Something with detector is not alright !\n");
        bRet = false;
    }
    if (strcmp(aFedMsg->getMessage()->description, "Unit - Test of Logger") 
            != 0) {
        printf("Something with description is not alright !\n");
        bRet = false;
    }
    if (strcmp(aFedMsg->getMessage()->source, "Unit-Test") != 0 ) {
        printf("Something with source is not alright !\n");
        bRet = false;
    }
    if (!(_strncoll(aFedMsg->getMessage()->date, now.date, 20) == 0)) {
        printf("Something with time is not alright !\n");
        printf("MessageTime: %s , Ctime: %s .\n", aFedMsg->getMessage()->date,
                now.date);
        bRet = false;
    }
/*
    // Testing the time to create a lot of log messages.
    struct __timeb64 startTime;
    struct __timeb64 endTime;
    aLogger.setLogLevel(Msg_Debug);
    _ftime64( &startTime );
    for (int i = 0; i < 540; ++i) {
        aLogger.createLogMessage(Msg_Debug, "Time", "Time Test of logging");
    }
    _ftime64( &endTime );
    printf("Duration of 540 logentries: %d ms\n", endTime.millitm - startTime.millitm);
*/

    delete aLogger;

    return bRet;
}


bool LoggerTest::testRelayLogEntry() {
    bool bRet = true;
    Timestamp now;

    FedServer aTestServer(0, dA);
    FedMessenger* aFedMsg = aTestServer.getMessenger();
    Logger* aLogger = Logger::createLogger(aFedMsg);
    MessageStruct msg(Msg_Alarm, DETECTOR, "Unit-Test", 
            "Unit - Test of Logger", now.date);
    MessageStruct msg2(Msg_Debug, DETECTOR, "Level-Test", 
            "test of remote log level in relay...", now.date);

    aLogger->relayLogEntry(&msg);
    if (aFedMsg->getMessage()->eventType != Msg_Alarm) {
        printf("Something with event - type is not alright (remote) !\n");
        bRet = false;
    }
    if (_strncoll(aFedMsg->getMessage()->detector, DETECTOR, 3) != 0) {
        printf("Something with detector is not alright (remote) !\n");
        bRet = false;
    }
    if (strcmp(aFedMsg->getMessage()->description, "Unit - Test of Logger") 
            != 0) {
        printf("Something with description is not alright (remote) !\n");
        bRet = false;
    }
    if (strcmp(aFedMsg->getMessage()->source, "Unit-Test") != 0 ) {
        printf("Something with source is not alright (remote) !\n");
        bRet = false;
    }
    if (!(_strncoll(aFedMsg->getMessage()->date, now.date, 20) == 0)) {
        printf("Something with time is not alright (remote) !\n");
        printf("MessageTime: %s , Ctime: %s .\n", aFedMsg->getMessage()->date,
                now.date);
        bRet = false;
    }

    aLogger->relayLogEntry(&msg2);
    if (aFedMsg->getMessage()->eventType == Msg_Debug) {
        printf("Remote log level checked seems to fail !\n");
        bRet = false;
    }
    if (strcmp(aFedMsg->getMessage()->source, "Level-Test") == 0 ) {
        printf("Remote log level checked seems to fail !\n");
        bRet = false;
    }

    delete aLogger;

    return bRet;
}

bool LoggerTest::testSetLogLevel() {
    bool bRet = true;

    FedServer aTestServer(0, dA);
    FedMessenger* aFedMsg = aTestServer.getMessenger();
    Logger* aLogger = Logger::createLogger(aFedMsg);

    aLogger->setLogLevel(Msg_Success_Audit);
    if (aLogger->getLogLevel() != (Msg_Success_Audit + Msg_Alarm)) {
        bRet = false;
    }

    delete aLogger;

    return bRet;
}

bool LoggerTest::testSetRemoteLogLevel() {
    bool bRet = true;

    FedServer aTestServer(0, dA);
    FedMessenger* aFedMsg = aTestServer.getMessenger();
    Logger* aLogger = Logger::createLogger(aFedMsg);

    aLogger->setRemoteLogLevel(Msg_Success_Audit);
    if (aLogger->getRemoteLogLevel() != (Msg_Success_Audit + Msg_Alarm)) {
        bRet = false;
    }

    delete aLogger;

    return bRet;
}

bool LoggerTest::testCheckLogLevel() {
    bool bRet = true;

    FedServer aTestServer(0, dA);
    FedMessenger* aFedMsg = aTestServer.getMessenger();
    Logger* aLogger = Logger::createLogger(aFedMsg);

    aLogger->setLogLevel(Msg_Failure_Audit);
    aLogger->createLogMessage(Msg_Warning, "LevelTest", "LogLevel - Test");
    if (strcmp(aFedMsg->getMessage()->source, "LevelTest") == 0 ) {
        bRet = false;
    }

    aLogger->setLogLevel(Msg_Warning);
    aLogger->createLogMessage(Msg_Warning, "LevelTest", "LogLevel - Test");
    if (strcmp(aFedMsg->getMessage()->source, "LevelTest") != 0 ) {
        bRet = false;
    }

    aLogger->setLogLevel(Msg_Warning + Msg_Alarm + Msg_Error + Msg_Debug);
    aLogger->createLogMessage(Msg_Info, "InfoTest", "LogLevel - Test");
    if (strcmp(aFedMsg->getMessage()->source, "InfoTest") == 0 ) {
        bRet = false;
    }
    aLogger->createLogMessage(Msg_Debug, "DebugTest", "LogLevel - Test");
    if (strcmp(aFedMsg->getMessage()->source, "DebugTest") != 0 ) {
        bRet = false;
    }

    aLogger->setLogLevel(Msg_Warning + Msg_Info + Msg_Error + Msg_Debug);
    aLogger->createLogMessage(Msg_Alarm, "AlarmTest", "LogLevel - Test");
    if (strcmp(aFedMsg->getMessage()->source, "AlarmTest") != 0 ) {
        bRet = false;
    }
    aLogger->createLogMessage(Msg_Error, "ErrorTest", "LogLevel - Test");
    if (strcmp(aFedMsg->getMessage()->source, "ErrorTest") != 0 ) {
        bRet = false;
    }

    delete aLogger;

    return bRet;
}  

bool LoggerTest::testCheckRemoteLogLevel() {
    bool bRet = true;
    Timestamp now;

    FedServer aTestServer(0, dA);
    FedMessenger* aFedMsg = aTestServer.getMessenger();
    Logger* aLogger = Logger::createLogger(aFedMsg);
    MessageStruct msg(Msg_Warning, DETECTOR, "LevelTest", "LogLevel - Test",
        now.date);
    MessageStruct msg2(Msg_Info, DETECTOR, "InfoTest", "LogLevel - Test",
        now.date);
    MessageStruct msg3(Msg_Debug, DETECTOR, "DebugTest", "LogLevel - Test",
        now.date);
    MessageStruct msg4(Msg_Alarm, DETECTOR, "AlarmTest", "LogLevel - Test",
        now.date);
    MessageStruct msg5(Msg_Error, DETECTOR, "ErrorTest", "LogLevel - Test",
        now.date);
    
    aLogger->setRemoteLogLevel(Msg_Failure_Audit);
    aLogger->relayLogEntry(&msg); 
    if (strcmp(aFedMsg->getMessage()->source, "LevelTest") == 0 ) {
        bRet = false;
    }

    aLogger->setRemoteLogLevel(Msg_Warning);
    aLogger->relayLogEntry(&msg);
    if (strcmp(aFedMsg->getMessage()->source, "LevelTest") != 0 ) {
        bRet = false;
    }

    aLogger->setRemoteLogLevel(Msg_Warning + Msg_Alarm + Msg_Error + Msg_Debug);
    aLogger->relayLogEntry(&msg2);
    if (strcmp(aFedMsg->getMessage()->source, "InfoTest") == 0 ) {
        bRet = false;
    }

    aLogger->relayLogEntry(&msg3);
    if (strcmp(aFedMsg->getMessage()->source, "DebugTest") != 0 ) {
        bRet = false;
    }

    aLogger->setRemoteLogLevel(Msg_Warning + Msg_Info + Msg_Error + Msg_Debug);
    aLogger->relayLogEntry(&msg4);
    if (strcmp(aFedMsg->getMessage()->source, "AlarmTest") != 0 ) {
        bRet = false;
    }

    aLogger->relayLogEntry(&msg5);
    if (strcmp(aFedMsg->getMessage()->source, "ErrorTest") != 0 ) {
        bRet = false;
    }

    delete aLogger;

    return bRet;
}


bool LoggerTest::testGetLogger() {
    bool bRet = true;
    try {
        const Logger* log = Logger::getLogger();
        bRet = false;
    } catch (std::runtime_error& er) {
        if (strcmp("Logger has not been instanciated yet!", er.what()) != 0) {
            bRet = false;
        }
    }

    FedServer aTestServer(0, dA);
    FedMessenger* aFedMsg = aTestServer.getMessenger();
    Logger* aLogger = Logger::createLogger(aFedMsg);

    try {
        const Logger* log = Logger::getLogger();
        if (log == 0) {
            bRet = false;
        }
        if (log != aLogger) {
            bRet = false;
        }
    } catch (std::runtime_error &er) {
        bRet = false;
    }

    delete aLogger;

    try {
        const Logger* log = Logger::getLogger();
        bRet = false;
    } catch (std::runtime_error &er) {
        if (strcmp("Logger has not been instanciated yet!", er.what()) != 0) {
            bRet = false;
        }
    }

    return bRet;
}


bool LoggerTest::testAll() {
    bool bRet = true;

    printf("\nTesting Logger: \n");

    if (!testLogger()) {
		bRet = false;
		printf(" *** TEST: testLogger failed. *** \n");
	} else {
		printf("Test: testLogger successfull.\n");
	}

    if (!testCreateLogMessage()) {
		bRet = false;
		printf(" *** TEST: testCreateLogMessage failed. *** \n");
	} else {
		printf("Test: testCreateLogMessage successfull.\n");
	}

    if (!testRelayLogEntry()) {
		bRet = false;
		printf(" *** TEST: testRelayLogMessage failed. *** \n");
	} else {
		printf("Test: testRelayLogMessage successfull.\n");
	}

	if (!testCheckLogLevel()) {
		bRet = false;
		printf(" *** TEST: testCheckLogLevel failed. *** \n");
	} else {
		printf("Test: testCheckLogLevel successfull.\n");
	}

    if (!testCheckRemoteLogLevel()) {
		bRet = false;
		printf(" *** TEST: testCheckRemoteLogLevel failed. *** \n");
	} else {
		printf("Test: testCheckRemoteLogLevel successfull.\n");
	}

	if (!testSetLogLevel()) {
		bRet = false;
		printf(" *** TEST: testSetLogLevel failed. *** \n");
	} else {
		printf("Test: testSetLogLevel successfull.\n");
	}

    if (!testSetRemoteLogLevel()) {
		bRet = false;
		printf(" *** TEST: testSetRemoteLogLevel failed. *** \n");
	} else {
		printf("Test: testSetRemoteLogLevel successfull.\n");
	}

    if (!testGetLogger()) {
		bRet = false;
		printf(" *** TEST: testGetLogger failed. *** \n");
	} else {
		printf("Test: testGetLogger successfull.\n");
	}


    return bRet;

}



#endif