#include "WatchDog.hpp"
#include <boost/lexical_cast.hpp>

using namespace std;
using namespace ztt::dcs;

int WatchDog::svc(){
	// get the messageblock 
	ACE_Message_Block* mb;
	while(this->getq(mb) != -1){
		this->processTimeOut(mb);
	}
	return 0;
}
void WatchDog::processTimeOut(ACE_Message_Block* mb){
	//the mutex has to be locked because cond.wait(..) releases the mutex.
	mutex.acquire();
	int id = 0;
	//cast char* into int
	memcpy(&id,	 mb->base(),sizeof(int));
	mb->reset();
#ifdef __DEBUG
    cout << "Watch dog started ... "<<id<< endl;
#endif
    // init timer for watch dog
	// see examples ACE example directory
	ACE_Time_Value timer (2000L/ACE_ONE_SECOND_IN_MSECS,0L);
	ACE_Time_Value expire_at = ACE_OS::gettimeofday() + timer;
	int status = condition.wait(&expire_at);
	if(status == -1){
		//error message
#ifdef __DEBUG
		cout<<"WATCHDOGTIMEOUT in "<< mcChannelName <<" with ID: " << id <<" occured"<<endl;
#endif
		string str;
		Timestamp ts;
		char* error = new char[256];
		error[sprintf(error,"%s timeout;%s by server/s:",mcChannelName,ts.date)]=0;
		str = error;
		map<int,char*> container = pAnswerStruct->freeAnswerStruct(id);
		if(container.size() != 0){
			for(map<int,char*>::iterator iter = container.begin();iter != container.end();iter++){	
				cout<<"Timeout name:"<<iter->second<<endl;
				str += iter->second;
				str += ",";
			}
		}
		//told PVSS that time out occured
		if(str.length() > 255){
			str.copy(error,255);
			error[256] = 0;
		}else{
			str.copy(error,string::npos);
			error[str.length()]=0;
		}
		mpFeeIclAck->setAckStruct(Msg_Warning,error);
		*mpCommandChannelBusyFlag = false;
		delete[] error;
	}else{
#ifdef __DEBUG
		cout<< "watchdog stopped normally"<<endl;
#endif
	}
	mutex.release();
}
