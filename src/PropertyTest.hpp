#ifdef __UTEST

#ifndef PROPERTYTEST
#define PROPERTYTEST

#include "Property.h"

class PropertyTest {
public:
	PropertyTest();

	~PropertyTest();

	bool testPropertyAll();

	bool testPropertyEmpty();

	bool testPropertyFile();

	bool testPropertyDatabase();

};


#endif
#endif