#ifdef __UTEST
#ifndef DATAACCESSTEST
#define DATAACCESSTEST

#include "DataAccess.hpp"

namespace ztt { namespace dcs {

class DataAccessTest {
public:
	DataAccessTest();

	~DataAccessTest();

	bool testConnectionPool();

	bool testConnection();

	bool testKillConnection();

	bool testgetDbInfoFromFile();

	bool testAll();
};

}}

#endif
#endif