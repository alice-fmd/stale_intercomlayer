#include <CommandCoderBase.hpp>
#include <vector>
#include <string>


/**
 * Base class for command coders 
 */
class DummyCommandCoder : public CommandCoderBase
{
public:
  /** Constructor of dummy command coder */
  DummyCommandCoder() 
  {
    _errors.push_back("none");
    _data.push_back(0xdead);
    _data.push_back(0xbeaf);
  }
  
  /** 
   * Create a data block 
   * @param target Where to send it 
   * @param tag The tag 
   * @return  Error code 
   */
  virtual int createDataBlock(char *target, int tag) { return _data.size(); }
  /** 
   * Get the command block 
   * @return  data block 
   */
  virtual long int * getDataBlock() { return &(_data[0]); }
  /** 
   * Get list of error strings 
   * @return  List of error  strings 
   */
  virtual std::vector<std::string> getError() { return _errors; }
private:
  /** The data that we will return */
  std::vector<long>   _data;
  /** The errors we will return - by value (sigh!) */
  std::vector<std::string> _errors;
};

CommandCoderBase* CommandCoderBase::instance = new DummyCommandCoder;

/*
 * EOF
 */
