#include "Property.h"
#include <boost/lexical_cast.hpp>

int Property::getDatafromFile(const std::string& confdir)
{
	int index;
	string temp;

#if 0
	if (strcmp(filename,"") == 0)
	{
#ifdef __DEBUG
		cout << "Filename empty" << endl;
#endif
		select = 0;
        return select;
	}
#endif
	filename =  confdir;
	filename += "Property.txt";
	
	ifile.open(filename.c_str(),ios_base::in);
	if(!ifile){
#ifdef __DEBUG
		cout << "Error while opening file "<< filename <<" or file does not exist\n" << endl;
#endif
		select = 0;
		return select;
	}else {
		while(!ifile.eof() && !ifile.bad())
		{
			char* name = new char[255];
			
			ifile.getline(name,256);
			index = strlen(name);
			name[index+1] = '\0';
			dataName.push_back(name);

		}
		ifile.close();
	}
	itFileName=dataName.begin();
	char* row = *itFileName;

	while(itFileName != dataName.end())
	{
		if(strstr(row,"File")||strstr(row,"FILE")||strstr(row,"file"))
		{
			select = 1;
		}
		if(strstr(row,"Database")||strstr(row,"DATABASE")||strstr(row,"database"))
		{
			select = 2;
		}
		if(strstr(row,"logging"))
		{
			string temp;
			temp = row;
			index = temp.find("=");
			temp.erase(0,index+1);
			temp.copy(row,string::npos);
			if(strstr(row,"YES")||strstr(row,"yes"))
			{
				logging=true;
			}
			else
			{
				logging = false;
			}
		}
		if(strstr(row,"grouped")){
			string temp;
			temp = row;
			index = temp.find("=");
			temp.erase(0,index+1);
			temp.copy(row,string::npos);
			if(strstr(row,"YES")||strstr(row,"yes"))
			{
				grouped = true;
			}
			else
			{
				grouped = false;
			}

		}
		if(strstr(row,"filenameserver"))
		{
			cutString(row);
			std::vector<char*>::iterator itTemp;
			itTemp=list.begin();
			list.insert(itTemp,row);
		}
		if(strstr(row,"filenameservices"))
		{
			cutString(row);
			list.push_back(row);
		}
		if(strstr(row,"filenamedb"))
		{
			cutString(row);
			list.push_back(row);
		}
		if(strstr(row,"detector_root")){
			cutString(row);
			try{
				x = boost::lexical_cast<int>(row);
			}catch(boost::bad_lexical_cast& e){
				cout << "Error while casting detector_root "<<e.what()<<endl;
			}
		}
		if(strstr(row,"detector_leaf")){
			cutString(row);
			try{
				z = boost::lexical_cast<int>(row);
			}catch(boost::bad_lexical_cast& e){
				cout << "Error while casting detector_leaf "<<e.what()<<endl;
			}
		}
		if(strstr(row,"detector_node")){
			cutString(row);
			try{
				y = boost::lexical_cast<int>(row);
			}catch(boost::bad_lexical_cast& e){
				cout << "Error while casting detector_node "<<e.what()<<endl;
			}
		}
	/*	if(strstr(row,"grouping_criteria")){
			string temp;
			temp = row;
			index = temp.find("=");
			temp.erase(0,index+1);
			temp.copy(groupingCriteria,string::npos);
		}*/
		itFileName++;
		row = *itFileName;
	}
	return select;
}
/*
* erase the keywords to get the content
*/
int Property::cutString(char* row)
{
			int index = 0;
			string temp;
			temp = row;
			index = temp.find("=");
			temp.erase(0,index+1);
			temp.copy(row,string::npos);
			row[temp.length()]= 0;
		return 0;			
}
Property::~Property(){
	if(!dataName.empty())
	{
		vector<char*>::iterator itTer;
		itTer = dataName.begin();
		while(itTer != dataName.end())
		{
			if(*itTer != 0)
			{
				delete[] *itTer;
			}
			itTer++;
		}
		dataName.clear();
	}
	if(!list.empty())
	{
		list.clear();
	}
}

