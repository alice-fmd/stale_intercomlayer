#ifndef COMMANDCODERBASE_HH
#define COMMANDCODERBASE_HH

#include <vector>
#include <string>

/**
 * Base class for command coders 
 */
class CommandCoderBase
{
public:
  /** 
   * Create a data block 
   * @param target Where to send it 
   * @param tag The tag 
   * @return  Error code 
   */
  virtual int createDataBlock(char *target, int tag) = 0;
  /** 
   * Get the command block 
   * @return  data block 
   */
  virtual long int * getDataBlock() = 0;
  /** 
   * Get list of error strings 
   * @return  List of error  strings 
   */
  virtual std::vector<std::string> getError() = 0;
  /** 
   * Get singleton object 
   * @return The singleton object 
   */
  static CommandCoderBase* createInstance() {return instance;};

private:
  /** The singleton object */
  static CommandCoderBase* instance;
};
#endif
