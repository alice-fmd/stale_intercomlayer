#ifndef ZTT_DCS_INTER_COM_HPP
#define ZTT_DCS_INTER_COM_HPP

#include "FeeClient.hpp"
#include "FeePacket.hpp"
#include "FeeItemInfo.hpp"
#include "FeeAckInfo.hpp"
#include "FeeMsgInfo.hpp"
#include "Logger.hpp"
#include <boost/scoped_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include "FeeAckInfo.hpp"
#include "FeeServerMap.hpp"
#include "FedServer.hpp"
#include "DataAccess.hpp"

#include <vector>



namespace ztt { namespace dcs {

/**
 * Defines the current version of the InterComLayer.
 */
#define INTERCOM_VERSION "0.7.2"


#ifdef WIN32
#define CLEAR_SCREEN "cls"
#else
#define CLEAR_SCREEN "clear"
#endif


/*
 * Remember interCom - error defines can have values from -60 to -79.
 * The area -70 to -79 is especially reserved for FeeAckInfos.
 * This area is reserved for interCom. Success is always 0.
 */

/**
 * Define for successfully executed command for interCom - Layer.
 */
#define INTERCOM_SUCCESS 0

/**
 * Define for interCom - Layer is in wrong state for this command.
 */
#define INTERCOM_WRONG_STATE -60

/**
 * Define for how many times the connection request to the DIM_DNS_NODE should
 * be tried.
 */
#define CHECK_DNS_TRIES 3

/**
 * Define for the sleep between tries for connection requests to DIM_DNS_NODE.
 * (in seconds).
 */
#define CHECK_DNS_TIMEOUT 5


//class FedServer;
//class DataAccess;

/**
 * The InterCom is the main object for the InterComLayer.
 * This represents the connection point for FedServer, FeeClient and DataAccess.
 * It contains also the collections of the different FeeInfo objects for the
 * comunication with the different FeeServers. It takes care of the
 * initialisation, data retrieving, data exchanging and the command handling,
 * and cleans up, when finished. The main logic is implemented here.
 * To bee done: -> Maybe should be realized as singleton.
 *
 * @author Christian Kofler, Sebastian Bablok
 * @date   2003-05-27
 */
class InterCom {
	public:

  	static void setConfigDir(const std::string& configDir);
        /**
         * Method to create a single InterCom - object (Singleton).
         * This method takes care, that only one InterCom - object is created
         * at one time. If this object already exists, it returns the pointer
         * to this object (see also "Design Patterns, GoF, - Singleton).
         * This function must be static, because it is called without having an
         * InterCom - object.
         *
         * @return pointer to the one and only InterCom - object.
         */
        static InterCom* createInterCom();

		/**
		 * Destructor for the InterCom
		 */
		~InterCom();

		/**
		 * Function to subscribe to services.
		 */
		void subscribe();

		/**
		 * Function to run the interCom layer after SetUp.
		 */
		void run(bool interactive=true);

		/**
		 * SetUp - includes function for the Mock Up version.
         * Release setUp will be implemented, when database is defined.
		 */
		void setUp();

		/**
		 * Incoming service data (items and ACK) are identified andhandled
		 * here.
		 *
		 * @param current pointer to the DimInfo object, containing the data
		 */
//		void returnResult(DimInfo* current);
         // !! Has been deprecated. Use instead the polymorph calls in
		 // FeeInfo !!

#ifdef __DEBUG
		/**
		 * MOCK-UP !!!
		 * type in to whom and what you want to send.
		 * MU stands for MockUp -> so this is only used in the debug mode
		 * to control the FeeClient and insert a command for sending down.
		 */
		void sendCommandMU();


        /**
         * MockUp for testing FeeCom SW with specified random commands.
         */
        void doRandomCmnd();
#endif

		/**
		 * Function to send a command, wrapped in a FeePacket, to a FeeServer.
		 *
		 * @param target the servername to which the command should be send
		 * @param pFeePacket pointer to the command, that should be send
		 *
         * @return the ID of the send command (ID of FeePacket) [1 - 65530]
		 */
		int sendCommand(char* target, boost::shared_ptr<FeePacket> pFeePacket);

        /**
         * Receives commands for the InterCom-Layer itself, interprets these
         * commands and executes them afterwards. The return value provides
         * the success - message.
         *
         * @param data the command - data, which the interCom must interpret.
         * @param size the size of the command - data.
         *
         * @return success message [0] or failure status [-60 ; -79] (for
         *          details see interCom defines).
         */
        int executeCommand(char* data, int size);

		/**
		 * Function to create a FeePacket, wrapping command, for sending it
         * to a FeeServer. If this command is destinated for the FeeServer
         * itself (rather than for the CE), the feeServerFlag has to be declared
         * here.
		 *
		 * @param payload the payload, representing a command, that should be
         *          inserted in this FeePacket
		 * @param size the size of the payload in bytes
         * @param checksumFlag indicates, if a checksum should be used.
         *          (default value of flag is false)
         * @param feeServerFlag contains flag bits, indicating commands to feeserver
         *          (default value of flag is NO_FLAGS)
         * @param huffmanFlag indicates, if huffmann coding should be used.
         *          (default value of flag is false)
		 *
		 * @return pointer to the created the command object
		 */
		boost::shared_ptr<FeePacket> createFeePacket(char* payload, int size,
                bool checksumFlag = false, FlagBits feeServerFlag = NO_FLAGS,
				bool huffmanFlag = false);

		boost::shared_ptr<FeePacket> createBroadCastFeePacket(char* payload, int size,
                bool checksumFlag = false, FlagBits feeServerFlag = NO_FLAGS,
				std::vector<char*>* feeserverCollection = NULL,bool huffmanFlag = false);
		/**
		 * Getter for the FeeItemInfoCollection.
		 *
		 * @return pointer to the FeeItemInfoCollection
		 */
		std::vector<FeeItemInfo* >* getFeeItemInfoCollection();

        /**
		 * Getter for the FeeAckInfoCollection.
		 *
		 * @return pointer to the FeeAckInfoCollection
		 */
		std::vector<FeeAckInfo* >* getFeeAckInfoCollection();

        /**
         * Getter for the FeeMsgInfoCollection.
		 *
		 * @return pointer to the FeeMsgInfoCollection
		 */
        std::vector<FeeMsgInfo* >* getFeeMsgInfoCollection();
        /**
         * Function to set a log entry.
         * This function provides the logger with the needed infos for a log
         * message and creates an entry. Used by the other parts of the
         * InterCom - Layer to set log-entries.
         *
         * @param type the loglevel type.
         * @param source where message original comes from.
         * @param description the message itself.
         */
        void setLogEntry(unsigned int type, char* source, char* description);
		 /**
         * Checks a given server name, if InterCom - Layer has been susbcribed
         * to it (Compares the name with list of servers).
         *
         * @param serverName the server name to check.
         *
         * @return true, if server is known to InterCom - Layer.
         */
        bool validateServerName(const char* serverName);
		/**
		* Returns the adress of the current FeeMap object.
		*/
		FeeServerMap* getFeeServerMap();
		/**
		* This function copy a initialized FeeServerMap to the local one.
		*
		* @param FeeServerMap contain the initialized FeeServerMap
		*/
		void setFeeServerMap(FeeServerMap);

	private:

        /**
		 * Constructor for central InterComLayer object.
		 * The connection to the other areas of this layer (FED-Server,
		 * FeeClient and DataBase) are initialized here. The ID is started
		 * here too. This constructor is private, so an object must be created
         * via the create - method (Singleton).
		 */
		InterCom();

        /**
         * This is the one and only InterCom - object.
         * (see Singleton, GoF, Desing Patterns).
         */
        static InterCom* pInstance;

        /**
         * Checks if enviromental var for DIM_DNS_NODE is set and DNS running.
         * Retries connection three times with a sleep of 5 seconds, if all
         * tries, it returns false. A log message is also provided.
         *
         * @return true if a running DIM_DNS is found under the given path
         *          (enviromental variable DIM_DNS_NODE), else false.
         */
        bool isDimDnsUp();

        /**
         * Function that exits all running FeeServer, to which the 
         * InterComLayer is registered. NOTE: there may be running FeeServer
         * to which the InterComLayer is not registered, these FeeServer remain
         * untouched.
         */
        void stopAllFeeServer();

	    /**
		* This mutex block the sending area of the InterComLayer
		*/
		ACE_Thread_Mutex mutex;
		/**
		* This mutex block the creating FeePacket area of the InterComLayer
		*/
		ACE_Thread_Mutex mutexFeePack;


#ifdef __DEBUG
        /**
         * Shows, who has paticipated in developing the InterComLayer.
         */
		void showAbout();

        /**
         * Shows all FeeServers, to which the InterComLayer is registered.
         * In addition, the current LINK status is shown.
         */
        void showFeeServers();

        /**
         * Mockup for updating FeeServer.
         */
        void updateFeeServerMU();
#endif


		/**
		 * Container for pointers of the FeeItemInfo - objects, which represent
         * the monitoring services. After subscribing it should contain all
         * services for monitoring purpose (not the Ack channel).
		 */
		std::vector<FeeItemInfo*> mFeeItemInfoCollection;

        /**
		 * Container for pointers of the FeeAckInfo - objects, which represent
         * the result channels for commands. After subscribing it should contain
         * all these services.
		 */
		std::vector<FeeAckInfo*> mFeeAckInfoCollection;

        /**
		 * Container for pointers of the FeeMsgInfo - objects. These channels are
         * used to deliver messages from the FeeServers. After subscribing it
         * should contain all msg channels (to each FeeServer one).
		 */
        std::vector<FeeMsgInfo*> mFeeMsgInfoCollection;

		/**
		 * Smart - pointer (scoped) to the FeeClient.
		 * Used for the communication with the FeeServers.
		 */
		boost::scoped_ptr<FeeClient> mClient;

		/**
		 * Smart - pointer (scoped) to the FED-Server.
		 * Used for the communication with the PVSS-Client.
		 */
		boost::scoped_ptr<FedServer> mFedServer;

		/**
		 * Smart - pointer (shared) to the DataAccess object.
		 * Used for the communication with the database.
		 */
		boost::shared_ptr<DataAccess> mDataAx;

        /**
		 * Smart - pointer (scoped) to the Logger object.
		 * Used for the communication with the logging logic.
		 */
		boost::scoped_ptr<Logger> mLogger;

        /**
         * The collection of known FeeServer. This container store them in key/value pairs. 
		 * The access to the desired is done by a generated key.
         * FeeServer is described by the class template itself.
         *
         * This member is to replace the vector below !!
		 */
		FeeServerMap mServerMap;

		/** collection for the server names */
		//std::vector<char* > mServerCol;

		/** collection for the service names */
		std::vector<char* > mServiceCol;
		

};  // end of class

inline std::vector<FeeItemInfo* >* InterCom::getFeeItemInfoCollection() {
	// ev. change pointer to const pointer ???
	return &mFeeItemInfoCollection;
}

inline std::vector<FeeAckInfo* >* InterCom::getFeeAckInfoCollection() {
	// ev. change pointer to const pointer ???
	return &mFeeAckInfoCollection;
}

inline std::vector<FeeMsgInfo* >* InterCom::getFeeMsgInfoCollection() {
	// ev. change pointer to const pointer ???
	return &mFeeMsgInfoCollection;
}

inline void InterCom::setLogEntry(unsigned int type, char* source, char* description) {
    mLogger->createLogMessage(type, source, description);
}
inline FeeServerMap* InterCom::getFeeServerMap(){
	return &mServerMap;
}
inline void InterCom::setFeeServerMap(const FeeServerMap map){
	mServerMap = map;
}
} } // end of namespace


#endif
