#ifndef PROPERTY
#define PROPERTY

#include <iostream>
#include <string>
#include <vector>
#include <fstream>	
#include <ctype.h>

using namespace std;

/**
* defining the standard configuration file name
*/
#ifdef WIN32
static const char* const PROPERTYFILENAME = "Property.txt";
#endif
#ifndef WIN32
static const char* const PROPERTYFILENAME = CONFIGDIR"Property.txt";
#endif
/**
* Allow flexible configuration path adaption by compile flags
*/
/*
*Property loads the above defined file and returns the required values
*
*@author Benjamin Schockert
*/
class Property
{
	public:
		/*
		*save the content of the property file
		*/
		vector<char*> dataName;
		vector<char*> list;
		/*
		*open and read the property file
		*/
		ifstream ifile;
		~Property();
		/*
		*save the property file name
		*/
		// const char* filename ;
		std::string filename;

		/*
		*iterator to traverse the dataName vector
		*/
		vector<char *>::iterator itFileName;
		/*
		*save, depending from the content of the property file, the corresponding
		*value for, File not found=0, File=1, Database=2.
		*/
		int select;
		/*
		* Constructor for the Property class. It assign the name of 
		* the propertyfile to the corresponding variable.
		*/
		Property():x(0),y(0),z(0),grouped(true),logging(false),select(0)		  ,groupingCriteria(NULL),filename(PROPERTYFILENAME){};
		/**
		* Copy constructor of the Property class
		*/
		Property(const Property& prop){
			x = prop.x;
			y = prop.y;
			z = prop.z;
			grouped = prop.grouped;
			logging = prop.logging;
			select = prop.select;
			dataName = prop.dataName;
			list = prop.list;
			filename = prop.filename;
			// strcpy(const_cast<char*>(filename), prop.filename);
			itFileName = prop.itFileName;
			//ifile = prop.ifile;
			strcpy(groupingCriteria, prop.groupingCriteria);
		}
		Property& operator=(const Property& prop){

			if(this != &prop){
				x = prop.x;
				y = prop.y;
				z = prop.z;
				grouped = prop.grouped;
				logging = prop.logging;
				select = prop.select;
				dataName = prop.dataName;
				list = prop.list;
				filename = prop.filename;
				// strcpy(const_cast<char*>(filename), prop.filename);
				itFileName = prop.itFileName;
				//ifile = prop.ifile;
				strcpy(groupingCriteria, prop.groupingCriteria);
			}
			return *this;
		}
		/*
		* The getDatafromFile function read the property file and checks
		* where the reauired values are. If the keyword "File" was found
		* the corresponding Files where loaded and the required names where 
		* assigned to the Server/Service names. If the keyword is "Database"
		* the database functions will called.
		*
		* @return value of selected method
		*/
		int getDatafromFile(const std::string& confdir);
		/**
		* The cutString method is used to truncate the keyword of the property file and 
		* returns the value that is defined in the file. 
		*/
		int cutString(char* row);
		/**
		* The logging variable is used to enable/disable the logging of the single services.
		*/
		bool logging;
		/**
		* The grouped variable enable/disable the grouping of several services to one channel.
		*/
		bool grouped;
		/**
		* The variables x,y,z represent the composition of the tree structure in the InterComLayer.
		* With this tree structure the InterComLayer is able to adress the FeeServer at their position
		* in the detector.
		*/
		int x;
		int y;
		int z;
		/**
		* just an idea to solve the grouping problem
		*/
		char* groupingCriteria;
	
};


#endif
