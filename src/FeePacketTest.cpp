#ifdef __UTEST

#include "FeePacketTest.hpp"
#include "FeePacket.hpp"
#include <memory>
#include <cstring>
#include <cstdio>
#include <cstdlib>

using namespace ztt::dcs;

FeePacketTest::FeePacketTest() {
	mpFeePacket = NULL;
}

void FeePacketTest::tearDown() {
	delete mpFeePacket;
}


bool FeePacketTest::testGetID() {
	bool bRet = true;
    unsigned int id;
//	mpFeePacket = new FeePacket(mLastID, 0, "HeLlO", 5, false, false, false);
    mpFeePacket = new FeePacket(0, "HeLlO", 5);

    id = mpFeePacket->getId(); 
    tearDown();
    
    mpFeePacket = new FeePacket(0, "HeLlO", 5);

	if (mpFeePacket->getId() != (++id)) {
		bRet = false;
		printf("Get ID - Test failed \n");
	}
	tearDown();
	return bRet;
}


bool FeePacketTest::testGetPayload() {
	bool bRet = true;
//	mpFeePacket = new FeePacket(mLastID, 0, "HeLlO AlIcE", 11, false, false, false);
    mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11);

	if (_strncoll(mpFeePacket->getPayload(), "HeLlO AlIcE", 
            mpFeePacket->getPayloadSize()) != 0) {
		bRet = false;
		printf("Get Payload - Test failed \n");
        printf("Payload: %s ; should be: %s .\n", mpFeePacket->getPayload(),
                "HeLlO AlIcE");
	}
	tearDown();
	return bRet;
}

bool FeePacketTest::testGetPayloadSize() {
	bool bRet = true;
//	mpFeePacket = new FeePacket(mLastID, 0, "HeLlO AlIcE", 11, false, false, false);
	mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11);

	if (mpFeePacket->getPayloadSize() != 11) {
		bRet = false;
		printf("Get PayloadSize - Test failed \n");
	}
	tearDown();
	return bRet;
}

bool FeePacketTest::testGetFeePacketSize() {
	bool bRet = true;
//	mpFeePacket = new FeePacket(mLastID, 0, "HeLlO AlIcE", 11, false, false, false);
	mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11);

	if (mpFeePacket->getFeePacketSize() != 23) {
		bRet = false;
		printf("Get FeePacketSize - Test failed \n");
	}
	tearDown();
	return bRet;
}

bool FeePacketTest::testFlags() {
    bool bRet = true;
//	mpFeePacket = new FeePacket(mLastID, 0, "HeLlO AlIcE", 11, false, false, false);
    mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11);
	if (!(!mpFeePacket->isHuffmanSet() && !mpFeePacket->isChecksumSet() &&
            mpFeePacket->getFlags() == NO_FLAGS)) {
		bRet = false;
		printf("Flag - Test failed (false, NoFlag, false) \n");
	}
	tearDown();
//	mpFeePacket = new FeePacket(mLastID, 0, "HeLlO AlIcE", 11, true, false, false);
    mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11, true);
	if (!(!mpFeePacket->isHuffmanSet() && mpFeePacket->isChecksumSet() &&
            mpFeePacket->getFlags() == CHECKSUM_FLAG)) {
		bRet = false;
		printf("Flag - Test failed (false, NoFlag, true) \n");
	}
	tearDown();
//	mpFeePacket = new FeePacket(mLastID, 0, "HeLlO AlIcE", 11, false, false, true);
    mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11, false, NO_FLAGS,
            true);
    // NOTE: even if the checksum is intentionally not set, it is set 
    // automatically here, because the huffman-flag is set.
	if (!(mpFeePacket->isHuffmanSet() && mpFeePacket->isChecksumSet() &&
            mpFeePacket->getFlags() == (HUFFMAN_FLAG + CHECKSUM_FLAG))) {
		bRet = false;
		printf("Flag - Test failed (true, NoFlag, true) \n");
	}
	tearDown();
//	mpFeePacket = new FeePacket(mLastID, 0, "HeLlO AlIcE", 11, true, false, true);
    mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11, true, NO_FLAGS,
            true);
	if (!(mpFeePacket->isHuffmanSet() && mpFeePacket->isChecksumSet() &&
            mpFeePacket->getFlags() == (HUFFMAN_FLAG + CHECKSUM_FLAG))) {
		bRet = false;
		printf("Flag - Test failed (true, NoFlag, true) \n");
	}
	tearDown();
//  mpFeePacket = new FeePacket(mLastID, 0, "HeLlO AlIcE", 11, true, true, true);
    mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11, false,
            FEESERVER_UPDATE_FLAG, true);
    // NOTE: even if the checksum is intentionally not set, it is set 
    // automatically here, because the update-flag is set. 
	if (!(mpFeePacket->isHuffmanSet() && mpFeePacket->isChecksumSet() && 
            mpFeePacket->getFlags() == (FEESERVER_UPDATE_FLAG + HUFFMAN_FLAG +
            CHECKSUM_FLAG))) {
		bRet = false;
		printf("Flag - Test failed (true, UpdateFlag, true) \n");
	}
	tearDown();

    mpFeePacket = new FeePacket(0, "HeLlO AlIcE", 11, true, 
            FEESERVER_RESTART_FLAG, true);
	if (!(mpFeePacket->isHuffmanSet() && mpFeePacket->isChecksumSet() &&
            mpFeePacket->getFlags() == (FEESERVER_RESTART_FLAG + HUFFMAN_FLAG +
            CHECKSUM_FLAG))) {
		bRet = false;
		printf("Flag - Test failed (true, RestartServerFlag, true) \n");
	}
	tearDown();

	return bRet;
}

bool FeePacketTest::testValid() {
    bool bRet = true;
//  mpFeePacket = new FeePacket(mLastID, 0, "MarshallTes", 11, true, false, false);
    mpFeePacket = new FeePacket(0, "MarshallTes", 11, true);
    if (!mpFeePacket->isValid()) {
        bRet = false;
    }
    tearDown();

    return bRet;
}


bool FeePacketTest::testMarshall() {
	bool bRet = true;
	char* data;
	unsigned int testID;
	short testErrorCode;
	unsigned short testFlag;
	char* payload;
    int packetID;
//	mpFeePacket = new FeePacket(mLastID, 0, "MarshallTes", 11, true, false, false);
    mpFeePacket = new FeePacket(0, "MarshallTes", 11, true);
    packetID = mpFeePacket->getId();
	data = mpFeePacket->marshall();

	payload = new char[12];
	memcpy(&testID, data, 4);
	memcpy(&testErrorCode, data + 4, 2);
	memcpy(&testFlag, data + 6, 2);
	memcpy(payload, data + 12, 11);

	payload[11] = 0;
	
	/*
	printf("UTEST: ErrorCode: \"%s\"\n", testErrorCode);
	printf("UTEST: payload: \"%s\"\n", payload);
	*/
	
	if (testID != packetID) {
		bRet = false;
		printf("MarshallTest: wrong ID\n");
	}
	if (testErrorCode != 0) {
		bRet = false;
		printf("MarshallTest: wrong error code\n");
	}
	if (testFlag != 0x2) {
		bRet = false;
		printf("MarshallTest: wrong flag\n");
	}
	if (strcmp(payload, "MarshallTes") != 0) {
		bRet = false;
		printf("MarshallTest: wrong payload\n");
	}
	tearDown();

//  mpFeePacket = new FeePacket(mLastID, 0, "MarshallTes", 11, false, true, true);
    mpFeePacket = new FeePacket(0, "MarshallTes", 11, false, 
            FEESERVER_UPDATE_FLAG, true);

    packetID = mpFeePacket->getId();
	data = mpFeePacket->marshall();

	payload = new char[12];
	memcpy(&testID, data, 4);
	memcpy(&testErrorCode, data + 4, 2);
	memcpy(&testFlag, data + 6, 2);
	memcpy(payload, data + 12, 11);

	payload[11] = 0;
	
	/*
	printf("UTEST: ErrorCode: \"%s\"\n", testErrorCode);
	printf("UTEST: payload: \"%s\"\n", payload);
	*/
	
	if (testID != packetID) {
		bRet = false;
		printf("MarshallTest: wrong ID\n");
	}
	if (testErrorCode != 0) {
		bRet = false;
		printf("MarshallTest: wrong error code\n");
	}
    if (testFlag != (FEESERVER_UPDATE_FLAG + HUFFMAN_FLAG + CHECKSUM_FLAG)) {
		bRet = false;
		printf("MarshallTest: wrong flag\n");
	}
	if (strcmp(payload, "MarshallTes") != 0) {
		bRet = false;
		printf("MarshallTest: wrong payload\n");
	}
	tearDown();

	delete data;
	return bRet;
}

bool FeePacketTest::testUnmarshall() {
	bool bRet = true;
	char* data;
    int packetID;
	
//	---- test false - NO_FLAGS - false -----------------
//	mpFeePacket = new FeePacket(mLastID, -1, "Unmarshall", 11, false, false, false);
    mpFeePacket = new FeePacket(-1, "Unmarshall", 11);
    packetID = mpFeePacket->getId();

/*
	printf("\nFLAG - TEST before Marshall!");
	printf("\nFlag-Bitset: %x ", mpFeePacket->getFlags());
	if (mpFeePacket->isHuffmanSet()) {
		printf("\nHuffman flag is set.\n");
	} else {
		printf("\nNO Huffman flag.\n");
	}
	if (mpFeePacket->isChecksumSet()) {
		printf("Checksum flag is set.\n\n");
	} else {
		printf("NO Checksum flag.\n\n");
	}
*/
	data = mpFeePacket->marshall();
	delete mpFeePacket;
	mpFeePacket = FeePacket::unmarshall(data, 23);

	if (mpFeePacket->getPayloadSize() != 11) {
		bRet = false;
		printf("UnmarshallTest: wrong payload Length\n");
	}
	if (mpFeePacket->getId() != packetID) {
		bRet = false;
		printf("UnmarshallTest: wrong ID\n");
	}
	if (mpFeePacket->getErrorCode() != -1) {
		bRet = false;
		printf("UnmarshallTest: wrong error code\n");
	}
//	if (!(!mpFeePacket->isHuffmanSet() && !mpFeePacket->isChecksumSet() && 
//            !mpFeePacket->isFeeServerCommandSet())) {
    if (!(!mpFeePacket->isHuffmanSet() && !mpFeePacket->isChecksumSet() && 
            mpFeePacket->getFlags() == NO_FLAGS)) {
		bRet = false;
		printf("Unmarshall - Flag - Test failed (false - false - false) \n");
	}

/*

	printf("\nFLAG - TEST after Unmarshall!");
	printf("\nFlag-Bitset: %x ", mpFeePacket->getFlags());
	if (mpFeePacket->isHuffmanSet()) {
		printf("\nHuffman flag is set.\n");
	} else {
		printf("\nNO Huffman flag.\n");
	}
	if (mpFeePacket->isChecksumSet()) {
		printf("Checksum flag is set.\n\n");
	} else {
		printf("NO Checksum flag.\n\n");
	}
*/		
	if (strcmp(mpFeePacket->getPayload(), "Unmarshall") != 0) {
		bRet = false;
		printf("MarshallTest: wrong payload\n");
	}
	tearDown();
	delete data;


	// ---- test false - NO_FLAGS - true -------- 
    // Note: Result is true - NO_FLAGS - true
//  mpFeePacket = new FeePacket(mLastID, -1, "Unmarshall", 11, false, false, true);
	mpFeePacket = new FeePacket(-1, "Unmarshall", 11, false, NO_FLAGS, true); 

    packetID = mpFeePacket->getId();
	data = mpFeePacket->marshall();
	delete mpFeePacket;
	mpFeePacket = FeePacket::unmarshall(data, 23);

	if (mpFeePacket->getPayloadSize() != 11) {
		bRet = false;
		printf("UnmarshallTest: wrong payload Length\n");
	}
	if (mpFeePacket->getId() != packetID) {
		bRet = false;
		printf("UnmarshallTest: wrong ID\n");
	}
	if (mpFeePacket->getErrorCode() != -1) {
		bRet = false;
		printf("UnmarshallTest: wrong error code\n");
	}
    // NOTE: even if the checksum is intentionally not set, it is set 
    // automatically here, because the huffman-flag is set.
	if (!(mpFeePacket->isHuffmanSet() && mpFeePacket->isChecksumSet() && 
            mpFeePacket->getFlags() == (HUFFMAN_FLAG + CHECKSUM_FLAG))) {
		bRet = false;
		printf("Unmarshall - Flag - Test failed (false - false - true) \n");
	}
	
	if (strcmp(mpFeePacket->getPayload(), "Unmarshall") != 0) {
		bRet = false;
		printf("MarshallTest: wrong payload\n");
	}
	tearDown();
	delete data;

	// ---- test true - true - false --------
//	mpFeePacket = new FeePacket(mLastID, -1, "Unmarshall", 11, true, true, false);
    mpFeePacket = new FeePacket(-1, "Unmarshall", 11, true, FEESERVER_UPDATE_FLAG);

    packetID = mpFeePacket->getId();
	data = mpFeePacket->marshall();
	delete mpFeePacket;
	mpFeePacket = FeePacket::unmarshall(data, 23);

	if (mpFeePacket->getPayloadSize() != 11) {
		bRet = false;
		printf("UnmarshallTest: wrong payload Length\n");
	}
	if (mpFeePacket->getId() != packetID) {
		bRet = false;
		printf("UnmarshallTest: wrong ID\n");
	}
	if (mpFeePacket->getErrorCode() != -1) {
		bRet = false;
		printf("UnmarshallTest: wrong error code\n");
	}
	if (!((!mpFeePacket->isHuffmanSet()) && mpFeePacket->isChecksumSet() && 
            mpFeePacket->getFlags() == (FEESERVER_UPDATE_FLAG + CHECKSUM_FLAG))) {
		bRet = false;
		printf("Unmarshall - Flag - Test failed (true - true - false) \n");
	}
	
	if (strcmp(mpFeePacket->getPayload(), "Unmarshall") != 0) {
		bRet = false;
		printf("MarshallTest: wrong payload\n");
	}
	tearDown();
	delete data;

    // ---- test true - NO_FLAGS - false --------
//	mpFeePacket = new FeePacket(mLastID, -1, "Unmarshall", 11, true, false, false);
    mpFeePacket = new FeePacket(-1, "Unmarshall", 11, true);

	data = mpFeePacket->marshall();
	delete mpFeePacket;
    data[FEE_PACKET_HEADER_SIZE + 3] = '*';
	mpFeePacket = FeePacket::unmarshall(data, 23);

    if (mpFeePacket->isValid()) {
		bRet = false;
		printf("Valid test or checksum failed\n");
	}

	tearDown();
	delete data;

	// ---- test true - NO_FLAGS - true ---------
//	mpFeePacket = new FeePacket(mLastID, -1, "Unmarshall", 11, true, false, true);
    mpFeePacket = new FeePacket(-1, "Unmarshall", 11, true, NO_FLAGS, true);

    packetID = mpFeePacket->getId();
	data = mpFeePacket->marshall();
	delete mpFeePacket;
	mpFeePacket = FeePacket::unmarshall(data, 23);

	if (mpFeePacket->getPayloadSize() != 11) {
		bRet = false;
		printf("UnmarshallTest: wrong payload Length\n");
	}
	if (mpFeePacket->getId() != packetID) {
		bRet = false;
		printf("UnmarshallTest: wrong ID\n");
	}
	if (mpFeePacket->getErrorCode() != -1) {
		bRet = false;
		printf("UnmarshallTest: wrong error code\n");
	}
//	if (!(mpFeePacket->isHuffmanSet() && mpFeePacket->isChecksumSet() && 
//            !mpFeePacket->isFeeServerCommandSet())) {
    if (!(mpFeePacket->isHuffmanSet() && mpFeePacket->isChecksumSet() && 
            mpFeePacket->getFlags() == (HUFFMAN_FLAG + CHECKSUM_FLAG))) {
		bRet = false;
		printf("Unmarshall - Flag - Test failed (true - false - true) \n");
	}
	
	if (strcmp(mpFeePacket->getPayload(), "Unmarshall") != 0) {
		bRet = false;
		printf("MarshallTest: wrong payload\n");
	}
	tearDown();
	delete data;

	return bRet;
}

bool FeePacketTest::testAll() {
	bool bRet = true;
	printf("Testing FeePacket: \n");
	
    if (!testGetID()) {
		bRet = false;
		printf(" *** TEST: testGetIDTest failed. *** \n");
	} else {
		printf("Test: testGetIDTest successfull.\n");
	}

	if (!testGetPayload()) {
		bRet = false;
		printf(" *** TEST: testGetPayload failed. *** \n");
	} else {
		printf("Test: testGetPayload successfull.\n");
	}
	if (!testGetPayloadSize()) {
		bRet = false;
		printf(" *** TEST: testGetPayloadSize failed. *** \n");
	} else {
		printf("Test: testGetPayloadSize successfull.\n");
	}
	if (!testGetFeePacketSize()) {
		bRet = false;
		printf(" *** TEST: testGetFeePacketSize failed. *** \n");
	} else {
		printf("Test: testGetFeePacketSize successfull.\n");
	}
	if (!testFlags()) {
		bRet = false;
		printf(" *** TEST: testFlags failed. *** \n");
	} else {
		printf("Test: testFlags successfull.\n");
	}
    if (!testValid()) {
		bRet = false;
		printf(" *** TEST: testValid failed. *** \n");
	} else {
		printf("Test: testValid successfull.\n");
	}
	if (!testMarshall()) {
		bRet = false;
		printf(" *** TEST: testMarshall failed. *** \n");
	} else {
		printf("Test: testMarshall successfull.\n");
	}
	if (!testUnmarshall()) {
		bRet = false;
		printf(" *** TEST: testUnmarshall failed. *** \n");
	} else {
		printf("Test: testUnmarshall successfull.\n");
	}
	
	return bRet;

}

#endif
