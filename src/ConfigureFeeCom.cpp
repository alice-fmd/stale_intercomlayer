#include "ConfigureFeeCom.hpp"
#include <iostream>
#include <string>
#include "InterCom.hpp"
#include "RegExpr.hpp"
#include "FeeServerMap.hpp"
#include "ace/Strategies_T.h"

#include <set>


using namespace std;
using namespace ztt::dcs;

bool ConfigureFeeCom::handleCommand(){
	
	bool nRet = false;
	// create a logger to log events to a logfile or to PVSS
	const Logger* logIt = Logger::getLogger();
	// check that the command is valid
	if(getSize()<=0){
		try {
            char text[100];
            text[sprintf(text, 
                "Received an ConfigureFeeCom command with size <= 1, command ignored.")] = 0;
            logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
        } catch (std::runtime_error &excep) {
            // Can't print and send log message,
            // but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
            cerr << "Exception: " << excep.what() << endl;
#endif
        }
        return nRet;
	}
	// check that the command contain any data
	if(getData()== 0){
		try{
			char message[256];
			message[sprintf(message,"Received null pointer from DIM in ConfigureFeeCom, value rejected")] = 0;
			logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		return nRet;
	}
	ContConfigureFeeCom* contChannel = NULL;
	try{
		//get content from command channel
		contChannel = reinterpret_cast<ContConfigureFeeCom*>(getData());
	}catch(bad_cast& e){
		char message[256];
		message[sprintf(message,"Cast operation in ConfigureFeeCom channel failed %s",e.what())] = 0;
		logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		return nRet;
	}
#ifdef __BENCHMARK
	try{
		char message[256];
		const Logger* logger = Logger::getLogger();
		ACE_Time_Value ts = ACE_OS::gettimeofday();
		struct timeval tStamp = ts.operator timeval();;
		struct tm *ptimes = localtime(&(tStamp.tv_sec));

		message[sprintf(message,"Benchmark ICL Timestamp(Receive ConfFeeCom) Time:%.8s - %ld",
			asctime(ptimes) + 11, tStamp.tv_usec)]=0;
		logger->createLogMessage(Msg_Debug,SOURCE_LOCAL,message);
		
	}catch(std::runtime_error &excep){
		cerr << "Exception in ConfigureFeeComBenchmark: " << excep.what() << endl;
	}
#endif

	// returns the singleton ICL object
	InterCom* icl = InterCom::createInterCom();
	//store the complete target name
	char* servername = NULL;
	// store the service name attached to the target name
	char* servicename = NULL;
	//store the different type of commands for the FeeServer
	char* commandData = NULL;
	// this value store the hardware type 
	char* hwType = new char[10];
	// store the lenght of each payload
	unsigned long count = 0;
	// store the given flag of the PVSS command
	int flag = 0;
	// store the single server names
	vector<char*> feeservercollection;
	// checks that the given target name is valid and extract other important informations
	RegExpr reg;
	// the new internal structure store all servernames in a map container
	FeeServerMap* pFeeMap = icl->getFeeServerMap();
	//get targetname from command
	servername = new char[sizeof(contChannel->target)];
	servername[sprintf(servername,contChannel->target)]=0;
	//check that the target name was assambled correctly
	if(!reg.inputValidation(servername)){
		try{
			const Logger* logger = Logger::getLogger();
			char message[256];
			message[sprintf(message,"Received strange targetname in ConfigureFeeCom channel, command rejected")] = 0;
			logger->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		delete[] servername;
		delete[] hwType;
		return nRet;
	}
	// store the service name attached to the target name
	servicename = new char[256];
	//check where the target is located
	if(strstr(servername,"local")){
		servicename[sprintf(servicename,"local")]=0;
	}else if(strstr(servername,"remote")){
		servicename[sprintf(servicename,"remote")]=0;
	}
	else{
		vector<int> val;
		string s = servername;
		
		//regular expression class to extract required information
		reg.getValues(s,&val,servicename,hwType);
		//get the desired feeservernames from the collection
		feeservercollection = pFeeMap->getFeeServer(val.at(0),val.at(1),val.at(2),hwType);	//p3DCollection->getChosenFeeServer(val.at(0),val.at(1),val.at(2));
		val.clear();
	}
	switch(contChannel->commandID){
		case(SET_FEESERVER_NAMES):
			//this command is the same like a reboot!therefor it is not implemented, yet.
			break;
		case(SET_FEESERVICE_NAMES):
			//this command is the same like a reboot!therefor it is not implemented, yet.
			break;
			//show dimtree that something changed
		case(SET_LOG_LEVEL):{
			if(strcmp(servicename,"local") == 0){
				const_cast<Logger*>(logIt)->setLogLevel(contChannel->iTag);
			}else if(strcmp(servicename,"remote") == 0){
				const_cast<Logger*>(logIt)->setRemoteLogLevel(contChannel->iTag);
			}else{
				flag = FEESERVER_SET_LOGLEVEL_FLAG;
				count = sizeof(int);
				commandData = new char[count];
				memcpy(commandData,&contChannel->iTag,sizeof(int));
			}
			break;
			}
		case(GET_LOG_LEVEL):{
			if(strcmp(servicename,"local") == 0){
				unsigned int loglevel = const_cast<Logger*>(logIt)->getLogLevel();
				char* text = new char[256];
				text[sprintf(text,"local log level of Intercomlayer is %d",loglevel)]=0;
				iclAck->setAckStruct(0,text);
				delete[] text;
			}else if (strcmp(servicename,"remote") == 0){
				unsigned int loglevel = const_cast<Logger*>(logIt)->getRemoteLogLevel();
				char* text = new char[256];
				text[sprintf(text,"remote log level of Intercomlayer is %d",loglevel)]=0;
				iclAck->setAckStruct(0,text);
				delete[] text;
			}else{
				flag = FEESERVER_GET_LOGLEVEL_FLAG;
			}
			break;
			}
		case(SET_UPDATE_RATE):{
			flag = FEESERVER_SET_UPDATERATE_FLAG;
			count = sizeof(int);
			commandData = new char[count];
			memcpy(commandData,&contChannel->iTag,sizeof(int));
			break;
			}
		case(GET_UPDATE_RATE):
			flag = FEESERVER_GET_UPDATERATE_FLAG;
			break;
		case(SET_TIME_OUT):{
			flag = FEESERVER_SET_ISSUE_TIMEOUT_FLAG;
			count = sizeof(int);
			commandData = new char[count];
			memcpy(commandData,&contChannel->iTag,sizeof(int));
			break;
			}
		case(GET_TIME_OUT):
			flag = FEESERVER_GET_ISSUE_TIMEOUT_FLAG;
			break;
		case(SET_DEAD_BAND):{
			flag = FEESERVER_SET_DEADBAND_FLAG;
			count = sizeof(float) + strlen(servicename);
			commandData = new char[count];
			memcpy(commandData, &contChannel->fTag, sizeof(float));
			memcpy(commandData + sizeof(float), servicename, strlen(servicename));
			break;
			}
		case(GET_DEAD_BAND):{
			flag = FEESERVER_GET_DEADBAND_FLAG;
			count = strlen(servicename);
			commandData = new char[count];
			memcpy(commandData,servicename,strlen(servicename));
			break;
			}
		default:{
				try {
					char text[100];
					text[sprintf(text, 
						"Received an unknown ConfigureFeeCom command %d, command ignored.",contChannel->commandID)] = 0;
					logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
				} catch (std::runtime_error &excep) {
				// Can't print and send log message,
				// but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
					cerr << "Exception: " << excep.what() << endl;
#endif
				}
				delete[] servername;
				delete[] servicename;
				delete[] hwType;
				return nRet;
			}
	}
	//if feeserver collection is empty target of command was icl
	if(!feeservercollection.empty())
	{	
		boost::shared_ptr<FeePacket> feepacket =  icl->createBroadCastFeePacket(commandData,count,true,flag,&feeservercollection);

		mAnswerStruct.setAnswerStruct(feepacket->getId(),feepacket->getAnswerSet());
		keyWatchDog = feepacket->getId();

		send.setFeePacket(feepacket);

		//send command to corresponding feeserver
		send.activate(THR_DETACHED|THR_NEW_LWP,1,1);
		
		mWatchDog.putq(mb);

		busy = true;
		nRet = true;
		feepacket.reset();
	}else{
		try {
					char text[100];
					text[sprintf(text, 
						"Unknown FeeServername received by ConfigureFeeCom channel")] = 0;
					logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
				} catch (std::runtime_error &excep) {
				// Can't print and send log message,
				// but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
					cerr << "Exception: " << excep.what() << endl;
#endif
				}
	}
	delete[] servername;
	delete[] servicename;
	delete[] hwType;
	
	//check that no error occur in the thread
	feeservercollection.clear();
	if(count>0){
		delete[] commandData;
	}
	return nRet;
}


bool ConfigureFeeCom::setAnswerData(const int& ID){
	
	bool bRet = false;
    //int status=0;
	
    // to avoid sending of the current result data when just subscribing to
    // service. No real answer, because no command has been sent first.
	if ((ID == 0)) {
        return bRet;
    }
	int ret = 0;
	ret = mAnswerStruct.findAnswerID(ID);
	if (ret == 1){
		//wake up watchdog
		//cond.signal();
		//cond.signal();
		mWatchDog.wakeUp();
		bRet = true;
		//notify that command was sucsessful
		char* text = new char[256];
		Timestamp ts;
		text[sprintf(text,"ConfigureFeeCom Ack;%s",ts.date)]=0;
		iclAck->setAckStruct(0,text);

		busy = false;	
		delete[] text;
	}else if(ret == 2){
		bRet = true;
	}

    return bRet;
}

void* ConfigureFeeCom::watchDog(void* objectHandler){/*
	ConfigureFeeCom* handler = reinterpret_cast<ConfigureFeeCom*>(objectHandler);
	//locking critical section
	handler->mutex.acquire();
    int status = 0;
	int id = handler->keyWatchDog;
	
	
#ifdef __DEBUG
    cout << "Watch dog started ..." << endl;
#endif

    // init timer for watch dog
	// see examples ACE example directory
	ACE_Time_Value timer (CFC_WATCH_DOG_TIMEOUT/ACE_ONE_SECOND_IN_MSECS,0L);
	ACE_Time_Value expire_at = ACE_OS::gettimeofday() + timer;
	status = handler->cond.wait(&expire_at);
	if(status == -1){
		//error message
		cout<<"WATCHDOGTIMEOUT in ConfigureFeeCom occured"<<endl;
		//evtl. melden welcher Server nicht geantwortet hat(Logger||icl_messagechannel)
		handler->mAnswerStruct.freeAnswerStruct(id);
		//told PVSS that time out occured
		char* error = new char[256];
		error[sprintf(error,"ConfigureFeecom watchdog timeout")]=0;
		handler->iclAck->setAckStruct(Msg_Warning,error);
		delete[] error;
	}else{
#ifdef __DEBUG
		cout<< "watchdog stopped normally"<<endl;
#endif
	}
	handler->busy = false;
	handler->mutex.release();
	
	
*/
	return 0;
}
ConfigureFeeCom::~ConfigureFeeCom(){
	mutex.remove();
	mb->release();
	ACE_Thread_Manager::instance()->cancel_task(&mWatchDog);
}

