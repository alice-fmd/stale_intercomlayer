#include "FeeItemInfo.hpp"
#include "Timestamp.hpp"

#include <fstream>
#include "Logger.hpp"
#include "DataAccess.hpp"
#include <iostream>

#ifdef __DEBUG
#include "InterCom.hpp"
#endif

using namespace std;
using namespace ztt::dcs;


bool FeeItemInfo::retrieveServiceValue() {
	//check for null pointer
	if(getName() == NULL){
		try{
			const Logger* logger = Logger::getLogger();
			char message[256];
			message[sprintf(message,"Received null pointer from DIM in FeeItemInfo, value rejected")] = 0;
			logger->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		return false;
		
	}
	if (getFloat() == FEE_ITEM_NO_LINK) {
		return false;
	}
	DataAccess* data = DataAccess::createDataAccess();
	// save value from current->getFloat()
	char origin[256];
	origin[sprintf(origin,getName())]=0;
	if(data->grouped){
		if(feeGroup != NULL){
			GroupedMessage message(getInt(),getFloat(),origin);
			feeGroup->setGroupedChannel(&message);
		}
	}
	//else{
	// the else part is commented out to solve a problem in the grouped mode.
	// mValue is in the grouped mode not required but is used to check the 
	// connection to the service. If mValue is set to something this bug will
	// not appear.
	mValue = getFloat();
	//}
	// trigger update on FED-Server for this item, if service exists there
	if (mpFedService != NULL) {
		mpFedService->updateService();
	}
#ifdef __DEBUG
	if (data->logging)
	{
//-----------------------------------------------------------------------------
		// writing service value to logfile
		// open result file for writing and appending to last value
		char filename[100];

		// ToDo: check Itemname for unallowed chars for filenames !!!

		filename[sprintf(filename, "Value-%s.txt", getName())] = 0;
		std::ofstream* resultFile = new std::ofstream(filename, ios_base::out
    			| ios_base::app);
    
		if (!(*resultFile)) {
			cout << "Unable to open file: "<< filename << " for storing Item-values!" << endl;
		} else {
			char entry[150];
			Timestamp ts;
			if (mValue == FEE_ITEM_NO_LINK) {
				entry[sprintf(entry, "InterComLayer has no link to Value of %s - \t%s.\n",
						getName(), ts.date)] = 0;
			} else {
				entry[sprintf(entry, "Value of %s: \t%f - \t%s.\n", getName(), getFloat(),
        			ts.date)] = 0;
			}
		 resultFile->write(entry, strlen(entry));
			
			// delete pointer to result file   
			delete resultFile;
		}
	}
#endif
// ----------------------------------------------------------------------------
	return true;
}

void FeeItemInfo::initialize() {
	mValue = FEE_ITEM_NO_LINK;
}


