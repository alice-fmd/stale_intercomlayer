#ifdef __UTEST

#include "RegExprTest.hpp"

using namespace ztt::dcs;

RegExprTest::RegExprTest(void)
{
}

RegExprTest::~RegExprTest(void)
{
}
bool RegExprTest::TestInputValidation(char* test){

	bool bRet = false;
	RegExpr reg;
		
	if(reg.inputValidation(test)){
		bRet = true	;
	}
	return bRet;

}
bool RegExprTest::TestInputValidationAll(){

	char * test = new char[256];

	test[sprintf(test,"trd-fee_0_0_0")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<<test<<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"tpc-fee_0_0_0")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<<test<<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TRD-FEE_9_0_0")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<<test<<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE_0_123_0")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<<test<<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE_0_0_123")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE_0_0")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-fee_0_0_0")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE_0_0_0Item1")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TpC-FeE_0_0_0")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"tRd-fEe_0_0_0")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"")]=0;
	if(TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with" << test << "passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE_0_0_99")]=0;
	if(!TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}
	test[sprintf(test,"TPC-FEE_0_99_0")]=0;
	if(!TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE_8_0_0")]=0;
	if(!TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE_0_0_0_Item8")]=0;
	if(!TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}

	test[sprintf(test,"TPC-FEE_8_99_99_ITEM789")]=0;
	if(!TestInputValidation(test)){
		std::cout<< "Test of Input Validation with "<< test <<" failed" << std::endl;
	}
	else{
		std::cout << "Test of InputValidation with " << test << " passed"<<std::endl;
	}
	delete[] test;

	return true;
}

#endif