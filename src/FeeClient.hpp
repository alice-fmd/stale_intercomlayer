#ifndef ZTT_DCS_FEECLIENT_HPP
#define ZTT_DCS_FEECLIENT_HPP

#include "dim/dic.hxx"
#include <vector>
#include <boost/scoped_ptr.hpp>


namespace ztt { namespace dcs {

class InterCom;
class FeePacket;

/**
 * This class represents the FeeClient as a part of the interComLayer.
 * It takes care of subscribing to services and handling the communication 
 * with the FeeServers.
 *
 * @author Christian Kofler, Sebastian Bablok
 */
class FeeClient : public DimClient {
	public:

		/**
		 * Constructor of the FeeClient.
		 * This client holds the connection to the FeeServer.
		 *
		 * @param pInterCom pointer to the InterCom - object, that handles this layer
		 */
		FeeClient(InterCom* pInterCom) : mpInterCom(pInterCom) {};
		/**
		* Copy constructor of FeeClient
		*/
		FeeClient(const FeeClient& feeCl) : mpInterCom(feeCl.mpInterCom){};
			
		FeeClient& operator=(const FeeClient& feeCl){
			return *this;
		}

		/**
		 * Destructor of the FeeClient
		 */
		~FeeClient();

		/**
		 * The infoHandler for incomming data of the FeeServer.
		 */
		void infoHandler();

		/**
		 * Function to subscribe to certain services.
		 * Gets a vector with char* containing: "ServerName", concatenates "/Acknowledge"
		 * and subscribes to it. Same with "/Mesage".
		 * additionally gets a vector with char* containing: "ServerName/ServiceName".
		 *
		 * @param pServers pointer to vector with all servernames.
		 * @param pServices pointer to conatiner of all services of all servers.
		 *
		 * @return amount of subscribed services.
		 */
		int subscribeTo(std::vector<char* >* servers, std::vector<char* >* pServices);


	private:

		/**
		 * Pointer to the InterCom - object, that handles this layer.
		 * DON'T use a smart pointer here, can cause problems about deleting !!
		 */
		InterCom* const mpInterCom;


};  // end of class

} } // end of namespace
#endif
