#ifdef __UTEST

#ifndef ZTT_DCS_COMMANDTEST_HPP
#define ZTT_DCS_COMMANDTEST_HPP

#include "FeePacket.hpp"

namespace ztt { namespace dcs {

/**
 * This class makes unit tests of our command object
 *
 * @author Christian Kofler, Sebastian Bablok
 */
class FeePacketTest {
	private:
		FeePacket* mpFeePacket;


	public:
		void tearDown();
		FeePacketTest();
		bool testGetID();
		bool testMarshall();
		bool testFlags();
        bool testValid();
		bool testUnmarshall();
		bool testGetPayload();
		bool testGetPayloadSize();
		bool testGetFeePacketSize();
		bool testAll();

}; // end class

} } // end namespace

#endif

#endif
