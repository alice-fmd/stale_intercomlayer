#ifdef __UTEST

#ifndef FEE_SERVER_MAP_TEST
#define FEE_SERVER_MAP_TEST

namespace ztt{namespace dcs{

class FeeServerMapTest
{
public:
	FeeServerMapTest(void);
	~FeeServerMapTest(void);
	bool setServer();
	bool getServer();
	bool broadCasting();
	bool testAll();


};
//end namespace
}}
#endif
#endif