#ifdef __UTEST

#include "FedMessengerTest.hpp"

#include "FedServer.hpp"
#include "InterCom.hpp"
#include "Timestamp.hpp"

#include <string>

using namespace ztt::dcs;

FedMessengerTest::FedMessengerTest() {

}

FedMessengerTest::~FedMessengerTest() {

}


bool FedMessengerTest::testFedMessenger() {
    bool bRet = true;
    char cmpALI[3] = {'A','L','I'};

    FedServer aTestServer(0, dA); // newly inserted
    FedMessenger aFedMsg("ALI", "FedMessengerTest", &aTestServer);
    MessageStruct* theMsg = aFedMsg.getMessage();

    for (int i = 0; i < 3; ++i) {
        if (theMsg->detector[i] != cmpALI[i]) {
		    bRet = false;
		    printf("MessageStruct has wrong detector name. \n");
	    }
    }

    return bRet;
}

bool FedMessengerTest::testSetMessage() {
    bool bRet = true;
    char cmpALI[3] = {'A','L','I'};
    char cmpUTE[3] = {'U','T','E'};

    FedServer aTestServer(0, dA);  // newly inserted
    FedMessenger aFedMsg("ALI", "FedMessengerTest", &aTestServer);
    MessageStruct* theMsg = aFedMsg.getMessage();

    for (int i = 0; i < 3; ++i) {
        if (theMsg->detector[i] != cmpALI[i]) {
		    bRet = false;
		    printf("MessageStruct has wrong detector name before setting. \n");
	    }
    }

    MessageStruct nextMsg("UTE");
    aFedMsg.setMessage(&nextMsg);
    theMsg = aFedMsg.getMessage();

    for (int j = 0; j < 3; ++j) {
        if (theMsg->detector[j] != cmpUTE[j]) {
		    bRet = false;
		    printf("MessageStruct has wrong detector name after setting. \n");
	    }
    }

    return bRet;
}

bool FedMessengerTest::testMessageStruct() {
    bool bRet = true;
    char cmpUTE[3] = {'U','T','E'};
    Timestamp now;

    MessageStruct aMsg("UTE");

    for (int i = 0; i < 3; ++i) {
        if (aMsg.detector[i] != cmpUTE[i]) {
		    bRet = false;
		    printf("MessageStruct has wrong detector name. \n");
	    }
    }
    if (strcmp(aMsg.source, "FedServer") != 0) {
		bRet = false;
		printf("MessageStruct has wrong source name. \n");
	}
    if (strcmp(aMsg.description, "FedServer started") != 0) {
		bRet = false;
		printf("MessageStruct has wrong description name. \n");
	}
    if (aMsg.eventType != Msg_Info) {
		bRet = false;
		printf("MessageStruct has wrong event type. \n");
	}


    if (strcmp(aMsg.date, now.date) != 0) {
		bRet = false;
		printf("MessageStruct has wrong date. \n");
	}


    return bRet;
}

bool FedMessengerTest::testAssignMessageStruct() {
    bool bRet = true;
    char cmpUTE[3] = {'U','T','E'};
    Timestamp now;
    
    MessageStruct newMsg("UTE");
    MessageStruct aMsg("ALI");

    aMsg.eventType = Msg_Alarm;
    strcpy(aMsg.source, "HumptyDumpty");
    strcpy(aMsg.description, "another string");
    aMsg = newMsg;
    newMsg.eventType = Msg_Debug;
    strcpy(newMsg.detector, "NEW");
    strcpy(newMsg.source, "CheshireCat");
    strcpy(newMsg.description, "nonsense");

    for (int i = 0; i < 3; ++i) {
        if (aMsg.detector[i] != cmpUTE[i]) {
		    bRet = false;
		    printf("MessageStruct has wrong detector name. \n");
	    }
    }
    if (strcmp(aMsg.source, "FedServer") != 0) {
		bRet = false;
		printf("MessageStruct has wrong source name. \n");
	}
    if (strcmp(aMsg.description, "FedServer started") != 0) {
		bRet = false;
		printf("MessageStruct has wrong description name. \n");
	}
    if (aMsg.eventType != Msg_Info) {
		bRet = false;
		printf("MessageStruct has wrong event type. \n");
	}   

    if (strcmp(aMsg.date, now.date) != 0) {
		bRet = false;
		printf("MessageStruct has wrong date. \n");
        printf("Time: %s .\n", now.date);
	}

    return bRet;
}

bool FedMessengerTest::testAll() {
    bool bRet = true;

    printf("\nTesting MessageStruct: \n");
    if (!testMessageStruct()) {
		bRet = false;
		printf(" *** TEST: testMessageStruct failed. *** \n");
	} else {
		printf("Test: testMessageStruct successfull.\n");
	}
    if (!testAssignMessageStruct()) {
		bRet = false;
		printf(" *** TEST: testAssignMessageStruct failed. *** \n");
	} else {
		printf("Test: testAssignMessageStruct successfull.\n");
	}

    printf("\nTesting FedMessenger: \n");
	if (!testFedMessenger()) {
		bRet = false;
		printf(" *** TEST: testFedMessenger failed. *** \n");
	} else {
		printf("Test: testFedMessenger successfull.\n");
	}
	if (!testSetMessage()) {
		bRet = false;
		printf(" *** TEST: testSetMessage failed. ***\n");
	} else {
		printf("Test: testSetMessage successfull.\n");
	}

//    printf("Size of MessageStruct is : %d\n", sizeof(MessageStruct));

    return bRet;

}

#endif
