#ifndef CONTROLFERO
#define CONTROLFERO

#include "FedCommand.hpp"
#include <iostream>
#include <vector>
#include "SendWithoutCoCo.hpp"
#include "FeeIclAck.hpp"
#include <boost/scoped_ptr.hpp>
#include "AnswerStruct.hpp"
#include "WatchDog.hpp"

#include "ace/Synch.h"

#define CONTROLFERO_STRUCTURE "C:20;C"
#define CTF_WATCH_DOG_TIMEOUT 5000L

namespace ztt{namespace dcs{
	/**
	* probably required if pvss use struct
	*/
	
	typedef struct ControlFeroStruct{
	char name[20];
	char payload[1000];

	ControlFeroStruct& operator=(const ControlFeroStruct& rhs){
			if(this==&rhs){
				return *this;
			}
			name[sprintf(name,rhs.name)]=0;
			memcpy(payload,rhs.payload,sizeof(rhs.payload));
			
			return *this;
			
		}
	}ControlFeroStruct;

	/**
	* the controlfero class represent the ControlFero command channel
	* this channel is used to send single configuration packages to 
	* one or more server.
	* 
	* @date 03.02.06
	* @author Benjamin Schockert
	*/
	class ControlFero:public FedCommand{
	
	public:
		/**
		* the constructor of controlfero initialze required values and create
		* the command channel.
		*/
		ControlFero(char* name,DimCommandHandler* handler):FedCommand(name,CONTROLFERO_STRUCTURE,handler),mutex(),
			cond(mutex),busy(false),localID(0),iclAck(FeeIclAck::createInstance()),mWatchDog("ControlFero",&mAnswerStruct,&busy){
				mWatchDog.activate(THR_DETACHED|THR_NEW_LWP,5,1);
				mb = new ACE_Message_Block((char*)&localID);
		};
		virtual ~ControlFero();
		/**
		* the commandHandler method handle incomming commands and
		* send the processed data further to the feeserver.
		*/
		virtual bool handleCommand();
		/**
		* the setAnswerData method stops the watchdog thread when 
		* the right conditions where given.
		*/
		virtual bool  setAnswerData(const int& ID);
		/**
		* the watchdog thread take care that the main thread will not hang
		* if an answer didn�t come in time.
		*/
		static void* watchDog(void* objectHandler);
		/**
		* the isBusy method is needed to check from the FeeAckInfo object
		* that an channel wait for an answer or not.
		*/
		bool isBusy();

		
	private:
		/**
		* the mutex value to lock critical sections
		*/
		ACE_Thread_Mutex mutex;
		/**
		* the condition value to wakeup watchdog
		*/
		ACE_Condition<ACE_Thread_Mutex> cond;
		/**
		* the SendFeeCommands object to send the processed command
		* to the feeserver (is a singleton to simplify synchronisation).
		*/
		SendWithoutCoCo send;
		/**
		* the InterCom layer acknowledge channel object
		*/
		boost::scoped_ptr<FeeIclAck> iclAck;
		/**
		* value that mark an channel busy
		*/
		bool busy;
		/**
		* localID store the first ID of each command and is used as key.
		*/
		int localID;
		/**
		* mAnswerStruct contain a syncronized map.
		*/
		AnswerStruct mAnswerStruct;
		WatchDog mWatchDog;
		ACE_Message_Block* mb;
	};
	inline bool ControlFero::isBusy(){
		return busy;
	}

}}

#endif
