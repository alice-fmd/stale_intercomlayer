#ifndef ZTT_DCS_FED_SERVER_HPP
#define ZTT_DCS_FED_SERVER_HPP

#include "dim/dis.hxx"
#include "FedMessenger.hpp"
#include "FedCommand.hpp"
#include "ConfigureFeeCom.hpp"
#include "ConfigureFero.hpp"
#include "ControlFeeCom.hpp"
#include "ControlFero.hpp"
#include "DataAccess.hpp"


#include <vector>

namespace ztt { namespace dcs {


/**
 * Represents state of FedServer, when initialized.
 */
#define FED_SERVER_INITIALIZED 0x0

/**
 * Represents state of FedServer, when running.
 */
#define FED_SERVER_RUNNING 0x1

/**
 * Represents state of FedServer, when stopped.
 */
#define FED_SERVER_STOPPED 0x2


class InterCom;

/**
 * This Server provides the contact to the PVSS-Client of the DCS-Group.
 * The PVSS has a Module which acts as a DIM-Client. It is part of the
 * interComLayer. The FedServer has different states:<br><ul>
 * <li> FED_SERVER_INITIALIZED - initial state, server is not yet running.</li>
 * <li> FED_SERVER_RUNNING - server has been started and is running.</li>
 * <li> FED_SERVER_STOPPED - server has been stopped.</li></ul><br>
 *
 * @author Sebastian Bablok, Christian Kofler
 */
class FedServer : public DimServer {
	public:

		/**
		 * Constructor for the FED - DIMServer (contacting PVSS).
		 * 
		 * @param pInterCom pointer to the InterCom - object, that handles this
		 *			layer.
         * @param pDA pointer to the DataAccess - object, that handles 
		 *			the data access (DB or from file).
		 */
        FedServer(InterCom* pInterCom, boost::shared_ptr<DataAccess> pDA) :
                    mpInterCom(pInterCom), mDataAccess(pDA), 
						mState(FED_SERVER_INITIALIZED),iclAck(FeeIclAck::createInstance()){
			//create Message channel
            char* msgName = new char[strlen(FED_SERVER_NAME) + 10];
            msgName[sprintf(msgName, "%s_MSG", FED_SERVER_NAME)] = 0;
            mpFedMessenger = new FedMessenger(DETECTOR, msgName, this);
            delete[] msgName;
			//create FedApi channel
			configFeeCom.reset(new ConfigureFeeCom("ConfigureFeeCom",this));
			configFero.reset( new ConfigureFero("ConfigureFero",this));
			controlFeeCom.reset(new ControlFeeCom("ControlFeeCom",this));
			controlFero.reset(new ControlFero("ControlFero",this));
			//create FeeIclAck channel
        };

		/**
		 * Destructor of the FED - DIMServer.
		 */
		virtual ~FedServer();

		/**
		 * Publishes all services that the FeeClient has been subscribed to and
		 * creates a RPC - Service for each FeeServer, the FeeClient is 
		 * connected to. The service objects are stored into the 
		 * mpFedServiceCollection, the RPC - Services in mpFedRpcCollection.
		 *
		 * @return number of service and RPC - service, that have been published
		 */
		int publishServices();

		virtual void commandHandler();

		/**
		 * Executes an incomming command from FedClient (RPC - call) and expects
		 * the result for further delivery as return value.
		 *
		 * @param pTarget the FeeServer to which the command should be sent.
		 * @param pIncomingCmnd the data of the incoming command
		 * @param size the size of the incoming command in bytes
		 *
		 * @return the id of the send command to the FeeServer.
		 */
		int executeCommand(char* pTarget, char* pIncomingCmnd, int size);

        /**
         * Getter - method for the FedMessenger.
         *
         * @return pointer to the FedMessenger.
         */
        FedMessenger* getMessenger();

        /**
         * Getter for the State of the FedServer.
         *
         * @return state of the FedServer.
         */
        int getState() const;

		/**
		 * The exit handler has to be overwritten to avoid an exit() call 
		 * from the DIM framework. Whenever an exit Command is sent, this
		 * handler will be called.
		 *
		 * A call of addExitHandler(this) is neccessary (e.g. in 
		 * publishServices()) to satisfy C-Style baseframe.
		 */
		void exitHandler() {};

        /**
         * Function sends received command further to all registered FeeServers.
         *
         * @param cmnd the command, to send further
         * @param size the size of the command
         *
         * @return number of FeeServers, to which the command was sent.
         */
        int broadcastCommand(void* cmnd, int size);


	private:
		/*
		* The FEDAPI command channel objects.
		*/
		boost::shared_ptr<ConfigureFeeCom> configFeeCom;
		boost::shared_ptr<ConfigureFero> configFero;
		boost::shared_ptr<ControlFeeCom> controlFeeCom;
		boost::shared_ptr<ControlFero> controlFero;
		/**
		 * Pointer to the InterCom - object, that handles this layer.
		 */
         InterCom* const mpInterCom;

         /**
          * Pointer to the data access.
          */
         boost::shared_ptr<DataAccess> mDataAccess;

		/**
		 * Container for all published services by FedServer for monitoring
         * items, (the answer service for the Rpc - calls is NOT included
         * here).
		 */
		 std::vector<boost::shared_ptr<DimService> > mFedServiceCollection;

        /**
         * The message service for the FedServer.
         */
        FedMessenger* mpFedMessenger;


		boost::scoped_ptr<FeeIclAck> iclAck;
        /**
         * State of FedServer (can be .... ??? )
         */
        int mState;
        /**
         * Function declares all needed commands to the InterComLayer itself,
         * that are necessary to control and configure the InterComLayer.
         */
     //   void declareInterComCommands();


		/**
		 * Function interprets an incoming command, which is destinated for the
		 * FeeServer. It assembles the corresponding FeePacket including the 
		 * appropriate flag and fills a smart pointer with the packet.
		 *
		 * @param cmd the incoming command
		 * @param size the size of the command
		 * @param commandPacket the assembled FeePacket
		 *
		 * @return FEE_OK on success, else error code
		 */
	//	int assembleFeeServerCommand(char* cmd, unsigned int size, 
	//				boost::shared_ptr<FeePacket>* commandPacket);
		
};  // end of class


inline int FedServer::getState() const {
    return mState;
}

} } // end of namespace


#endif
