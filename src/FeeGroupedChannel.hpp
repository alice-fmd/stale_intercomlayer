#ifndef FEEGROUPEDCHANNEL
#define FEEGROUPEDCHANNEL

#include <string>

namespace ztt { namespace dcs {
//define the structure of the Groupedchannel
#define BUNDLE_STRUCTURE "I:1;F:1;C:256;"
#define SERVICE_NAME 256

/**
* This structure represent the structur of the grouped service channel.
* The int and float values represent the measured values of the hardware.
* The char array is the name of the updated service
* 
* @author Benjamin Schockert
*/

typedef struct GroupedMessage{

	int iVal;
	float fVal;
	char cName[256];
	//the standard constructor of the struct
	GroupedMessage(){
		iVal = 0;
		fVal =0;
		std::memcpy(cName,"initial",256);
	}		
	//overloaded constructor that set all members
	GroupedMessage(int serviceInt,float serviceFloat, char* serviceName){
		iVal = serviceInt;
		fVal = serviceFloat;
		std::strcpy(cName,serviceName);
	}
	//assignment operator vor the struct
	GroupedMessage(const GroupedMessage& orig){
		iVal = orig.iVal;
		fVal = orig.fVal;
		memcpy(cName,orig.cName,256);
	}
	 GroupedMessage& operator=(const GroupedMessage& rhs) {
		 try{
			if (this == &rhs) {
			    return *this;
			}
			iVal = rhs.iVal;
			fVal = rhs.fVal;
			std::memcpy(cName, rhs.cName,SERVICE_NAME);
		 }catch(...){

		 }

        return *this;
    }

}GroupedMessage;
/**
* class that store the values of a GroupedMessage struct
*
*/
class FeeGroupedChannel{

public:
	FeeGroupedChannel(){};
	~FeeGroupedChannel(){};
	FeeGroupedChannel(const FeeGroupedChannel& orig){
		gMsg = orig.gMsg;
	}
	FeeGroupedChannel& operator=(const FeeGroupedChannel& rhs){
		try{
			if(this == &rhs){
				return *this;
			}
			gMsg = rhs.gMsg;
			
		}catch(...){}
		return *this;
	}
	GroupedMessage gMsg;
	//method who fill the local structure with the assigned structure
	void setGroupedChannel(GroupedMessage* mes){
		gMsg = *mes;
	}
};
}}//end of namespace
#endif

