#ifndef WATCH_DOG
#define WATCH_DOG

#include "ace/Task.h"
#include "ace/Synch.h"
#include "FeeIclAck.hpp"
#include "AnswerStruct.hpp"
#include "FedCommand.hpp"


namespace ztt{
	namespace dcs{



/**
* The WatchDog class is used to detect any timeouts in the command channels.
* When a timeout occurs the WatchDog notifies the PVSS.
*
* @date 2006-11-09
* @author Benjamin Schockert
*/

class WatchDog : public ACE_Task<ACE_MT_SYNCH>
{
public:
	/**
	* the watchDog constructor initalize all required values.
	*
	* @param channelName specify the name of the command channel
	* @param pAnswerStruct points to the list of ack ids. Is required to delete after a timeout the 
	*        corresponding entry in the list.
	*/
	WatchDog(const char* channelName, AnswerStruct* pAnswerStr,bool* commandBusy):mutex(),condition(mutex),pAnswerStruct(pAnswerStr),
		mpFeeIclAck(FeeIclAck::createInstance()),mcChannelName(channelName),mpCommandChannelBusyFlag(commandBusy){};
	~WatchDog(void){
		mutex.remove();
	};
	/**
	* svc is the entry point of the threads.
	*/
	virtual int svc(void);
	/**
	* wakeUp notify the sleeping thread that the answer has arrived.
	*/
	void wakeUp(void);
	/*
	* processTimeOut is the main function of the watchDog. It calculates the timeout.
	*
	* @param mb contain the id of the commmand. mb is used to transport this information into the thread.
	*/
	void processTimeOut(ACE_Message_Block* mb);
	void setCommandHandler(FedCommand* fedCom);
	

private:
	//mcChannelName store the name of the correspondend command channel
	const char* mcChannelName;
	//condition is used to notify the thread via a condition value
	ACE_Condition<ACE_Thread_Mutex> condition;
	//mutex is required to initialize the condition value.
	ACE_Thread_Mutex mutex;
	//points to the ack id list of each command channel
	AnswerStruct* pAnswerStruct;
	//mpFeeIclAck points to the acknowledge channel of the InterComLayer
	FeeIclAck* mpFeeIclAck;
	bool* mpCommandChannelBusyFlag;

};
inline void WatchDog::wakeUp(){
	condition.signal();
}
	}}

#endif
