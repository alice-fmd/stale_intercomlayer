#ifndef DATABASEBASECLASS
#define DATABASEBASECLASS

#include <vector>
#include <iostream>
#include <string.h>

/**
 * This class is the interface for the required database actions.
 * With this interface we are able to change very easily the
 * database if required. All functions a pure virtuell and must 
 * be implemented by the inherited classes.
 * 
 * To implement a variable DB interface the Strategy Design Pattern
 * of GOF is used.
 *
 * @author Benjamin Schockert
 */
namespace ztt { 
	namespace dcs {

class Database
{
public:
	/**
	* initialize the connection to the database
	*
	*/
	virtual int createConnection() = 0;
	/**
	* load the exact position of the single server from the database and load the corresponding servername
	*
	* @param tablename specify the table/view of the servernames
	* @param collection store the loaded servernames in a smart_ptr
	* @param posX store the x coordinates of the server
	* @param posY store the y coordinates of the server
	* @param posZ store the z coordinates of the server
	*/
	virtual int getDataFromDB(char* tablename, std::vector<char* >* collection, std::vector<int>*posX,std::vector<int>*posY,std::vector<int>*posZ) = 0;
	/**
	* load the Server-/Service names from Database
	*
	* @param tablename of the table
	* @param smart pointer which returns the values
	*/
	virtual int getDataFromDB(char* tablename, std::vector<char* >* collection) = 0;
	/**
	* write files to Database
	* 
	* @param tablename where data should be stored
	* @param data which should written to database
	*/
	virtual int setData(char* tablename, char* data) = 0;
	/**
	* kill the connection pool
	*/
	virtual int killConnection() = 0;
	
};
}}
#endif

