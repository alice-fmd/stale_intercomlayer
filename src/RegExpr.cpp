
#include "RegExpr.hpp"

using namespace std;
using namespace ztt::dcs;

RegExpr::RegExpr(void)
{
}

RegExpr::~RegExpr(void)
{
}
int RegExpr::getValues(string destination,vector<int>* values,char* service,char* hwTyp)
{
	int count=0;
	string s;
	boost::regex e ("(([*0-9])+)");

	boost::sregex_token_iterator i(destination.begin(),destination.end(),e,1);
	boost::sregex_token_iterator j;
	
	while(i!=j){
		try{
			//cout << *i << endl;
			string test=*i;
			int m = test.find("*") ;
			if(m != -1){
				values->push_back(-1);
			}else{
                values->push_back(boost::lexical_cast<int>(*i));
			}
			i++;
			count++;
			
		}catch(boost::bad_lexical_cast &e){
			cout<<"lexical_cast went wrong"<<e.what()<< endl;
			break;
		}
	}
	//extract the hardware information of the target name
	string temp = destination;
	int hwF = temp.find_first_of("-");
	// deliever the first part after the hardware type 
	int hwB = temp.find_first_of("_")-1;
	// calculate the length of the name
	int dist = hwB-hwF;
	// erase the first part of the target name
	temp.erase(0,hwF+1);
	//copy hardware name to char array
	memcpy(hwTyp,temp.data(),dist);
	hwTyp[dist] = 0;
	//extract the service name
	int cut=0;
	while(cut!=4){
		int index=destination.find_first_of("_");
		destination.erase(0,index+1);
		cut++;
	}
	if(!destination.empty()){
		destination.copy(service,string::npos);
		service[destination.length()]=0;
	}
	
	return count;

}
bool RegExpr::inputValidation(char* name){
	boost::regex e("^(TRD|TPC|PHOS|FMD)-[A-Z]{2,3}_[*0-8]_[*0-9]{1,2}_[*0-9]{1,2}((_[a-zA-Z0-9_*]*)?)$");

	boost::cmatch what;
	return (boost::regex_match(name,what,e)) ? true : false;
	/*if(boost::regex_match(name,what,e)){
		return true;
	}else{
		return false;
	}*/
}

