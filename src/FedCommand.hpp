#ifndef FED_COMMAND
#define FED_COMMAND

#include "dim/dis.hxx"
namespace ztt { namespace dcs {

/**
 * Fed Command abstract base class
 */
class FedCommand:public DimCommand{
public:
	/**
        *  @param name Name of command
        *  @param format Command format
        *  @param handle Command handler
        */
       FedCommand(char* name,char* format,DimCommandHandler* handle):DimCommand(name,format,handle){};
	/**
        * Destructor
        */
	virtual ~FedCommand(){};
	/**
        * Called when asked to handle a command
        * @return @c true on success, @c false otherwise
        */
	virtual bool handleCommand()=0;
       /**
        * Set the answer data
        * @param ID Answer ID.
        * @return @c true on success, @c false otherwise
        */
	virtual bool  setAnswerData(const int& ID)=0;

};

}}
#endif

