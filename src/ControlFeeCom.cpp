#include "ControlFeeCom.hpp"
#include "InterCom.hpp"
#include "FeeServerMap.hpp"
#include <iostream>
#include "RegExpr.hpp"

using namespace std;
using namespace ztt::dcs;

ControlFeeCom::~ControlFeeCom(){
	
}


bool ControlFeeCom::handleCommand(){
  
	bool nRet = false;
	// get the logger
	const Logger* logIt = Logger::getLogger();
	// check that the command is not empty
	if(getData()== 0){
		try{
			char message[256];
			message[sprintf(message,"Received null pointer from DIM in ControlFeeCom, value rejected")] = 0;
			logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		return nRet;
	}
	if (getSize() <= 0) {
        try {
            char text[100];
            text[sprintf(text, 
                "Received an ControlFeeCom command with size <= 1, command ignored.")] = 0;
            logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
        } catch (std::runtime_error &excep) {
            // Can't print and send log message,
            // but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
            cerr << "Unable to use Logger, no log message provided." << endl;
            cerr << "Exception: " << excep.what() << endl;
#endif
        }
        return nRet;
    }
	ContFeeComContent* tast = NULL;
	try{
		tast = reinterpret_cast<ContFeeComContent*>(getData());
	}catch(bad_cast& e){
		char message[256];
		message[sprintf(message,"Cast operation in ControlFeeCom channel failed %s",e.what())] = 0;
		logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		return nRet;
	}

	int flag = 0;
	// get the ICL singleton object
    InterCom* icl= InterCom::createInterCom();
	// stores the feeservernames which are adressed by the given target name
	vector<char*>feeservername;
	// the feeservercollection is not longer used in the ICL
	FeeServerMap* pFeeMap = icl->getFeeServerMap();
	// RegExpr parses the target name and extract the coordinates and other
	// important values
	RegExpr reg;
	// hwType store the hardware type specified in the target name
	char* hwType = new char[10];
	//check that cast has worked correctly
	/*if(tast->target==NULL){
		try{
			const Logger* logIt = Logger::getLogger();
			char message[256];
			message[sprintf(message,"Cast operation in ControlFeeCom channel failed, command rejected")] = 0;
			logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		return nRet;
	}*/
	// extract the target name
	char* pMessage = new char[sizeof(tast->target)];
	char* servicename = new char[256];
	memcpy(pMessage,&tast->target,256);
	//check that given name where assembled correct
	if(!reg.inputValidation(pMessage)){
		try{
			const Logger* logger = Logger::getLogger();
			char message[256];
			message[sprintf(message,"Received strange targetname for ControlFeeCom, command rejected")] = 0;
			logger->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		delete[] pMessage;
		delete[] servicename;
		delete[] hwType;
		
		return nRet;

	}

	string s = pMessage;
	vector<int> val;
	// extract the different informations stored in the target name
	reg.getValues(pMessage,&val,servicename,hwType);
	// get the single server names
	feeservername = pFeeMap->getFeeServer(val.at(0),val.at(1),val.at(2),hwType);

	//check that the vector is not empty
	if (!feeservername.empty()){			
		switch(tast->commandID){
			case(FEESERVER_UPDATE_FLAG):
				flag=FEESERVER_UPDATE_FLAG;
				break;
			case(FEESERVER_RESTART_FLAG):
				flag=FEESERVER_RESTART_FLAG;
				break;
			case(FEESERVER_REBOOT_FLAG):
				flag=FEESERVER_REBOOT_FLAG;
				break;
			case(FEESERVER_SHUTDOWN_FLAG):
				flag=FEESERVER_SHUTDOWN_FLAG;
				break;
			case(FEESERVER_EXIT_FLAG):
				flag=FEESERVER_EXIT_FLAG;
				break;
			default:{
						try {
							char text[100];
							text[sprintf(text, 
								"Received unknown command value in ControlFeeCom %d",tast->commandID)] = 0;
							logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
						} catch (std::runtime_error &excep) {
						// Can't print and send log message,
						// but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
							cerr << "Unable to use Logger, no log message provided." << endl;
							cerr << "Exception: " << excep.what() << endl;
#endif
						}				
						delete[] pMessage;
						delete[] servicename;
						delete[] hwType;
						return nRet;
					}
			}	
		//send command to corresponding feeserver
		send.setFeePacket(icl->createBroadCastFeePacket(0,0,0,flag,&feeservername));
               	send.activate(THR_DETACHED|THR_NEW_LWP,1,1);
		nRet = true;
	}else{
		try {
					char text[100];
					text[sprintf(text, 
						"Unknown FeeServername received by ControlFeeCom channel")] = 0;
					logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
				} catch (std::runtime_error &excep) {
				// Can't print and send log message,
				// but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
					cerr << "Exception: " << excep.what() << endl;
#endif
				}
	}
	delete[] pMessage;
	delete[] servicename;
	delete[] hwType;
	val.clear();
	//check that not the vector of the send thread is erased.
	feeservername.clear();
	
	return nRet;
}


bool ControlFeeCom::setAnswerData(const int& ID=0){return false;}

void* ControlFeeCom::watchDog(void* handle){return 0;}
