#ifdef __UTEST

#ifndef SET_GROUPED_CHANNEL_TEST
#define SET_GROUPED_CHANNEL_TEST

#include "FeeGroupedChannel.hpp"



class SetGroupedChannelTest
{
public:
	SetGroupedChannelTest(void);
	~SetGroupedChannelTest(void);
	bool TestCreateGroupedMessage();
	bool TestAssignmentOperator(ztt::dcs::GroupedMessage*);
	bool TestOverloadConstructor(int,float,char*);
	bool TestAll();

};

#endif
#endif