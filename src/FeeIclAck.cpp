#include "FeeIclAck.hpp"

using namespace std;
using namespace ztt::dcs;

FeeIclAck* FeeIclAck::createInstance()
{
	static FeeIclAck pInstance;

	return &pInstance;
}

FeeIclAck::FeeIclAck()
{
	char* name = new char[256];
	name[sprintf(name,"%s_ACK",FED_SERVER_NAME)] = 0;
	ackChannel = new DimService(name,ACKSTRUCTURE,&iclAck,sizeof(IclAck));

	delete[] name;
}

FeeIclAck::~FeeIclAck(void)
{
	delete ackChannel;
}
