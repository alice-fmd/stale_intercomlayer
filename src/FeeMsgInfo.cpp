#include "FeeMsgInfo.hpp"

#include "Logger.hpp"

#ifdef __DEBUG
#include <iostream>
using namespace std;
#endif

using namespace ztt::dcs;


FeeMsgInfo::~FeeMsgInfo() {
    // don't delete this, this pointer is shared with FeeAckInfo
    // use smart_pointer here, would help a lot
	// delete[] mpServerName;

}

bool FeeMsgInfo::retrieveServiceValue() {
    MessageStruct msg(DETECTOR);
    void* pData;

    if (getSize() != sizeof(MessageStruct)) {
        // this means an error or no Link, so message struct cannot be filled
#       ifdef __DEBUG
        cout << "Message channel from FeeServer has an ERROR." << endl;
#       endif
        return false;
    }
	//check that getData contains values
	if(getData()== NULL){
		try{
			const Logger* logger = Logger::getLogger();
			char message[256];
			message[sprintf(message,"Received null pointer from DIM in FeeMsgInfo, value rejected")] = 0;
			logger->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
			cerr << "Exception: " << excep.what() << endl;
#endif
		}
		return false;
		
	}

	pData = getData();
#ifdef __DEBUG
	cout <<"Size of FeeMsgInfo "<< getSize() << endl;
#endif
	msg = *(static_cast<MessageStruct*> (pData));

#   ifdef __DEBUG
    cout << "Received Message: " << endl << " ";
    cout << msg.date << " [" << msg.eventType << "] " << msg.detector << "/"
            << msg.source << " " << msg.description << endl;
#   endif

    try {
        Logger::getLogger()->relayLogEntry(&msg);
    } catch (std::runtime_error &excep) {
        // can we do anything to inform about missing Logger ???
        // but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
        cerr << "Exception: " << excep.what() << endl;
#endif
    }

    return true;
}


void FeeMsgInfo::initialize() {
}

