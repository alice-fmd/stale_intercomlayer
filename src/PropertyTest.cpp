#ifdef __UTEST

#include "PropertyTest.hpp"

PropertyTest::PropertyTest()
{
}
PropertyTest::~PropertyTest()
{
}
bool PropertyTest::testPropertyEmpty()
{
	bool bRet = true;
	Property prop;
	prop.filename="";
	if (prop.getDatafromFile()> 0)
	{
		bRet = false;
	}
	prop.filename="Prop.txt";

	if (prop.getDatafromFile()> 0)
	{
		bRet = false;
	}
	return bRet;
}

bool PropertyTest::testPropertyFile()
{
	bool bRet = true;
	Property prop;
	prop.filename="testfile.txt";
	if (prop.getDatafromFile()!= 1)
	{
		bRet = false;
	}
	return bRet;
}

bool PropertyTest::testPropertyDatabase()
{
	bool bRet = true;
	Property prop;
	prop.filename="testdatabase.txt";
	if (prop.getDatafromFile()!= 2)
	{
		bRet = false;
	}
	return bRet;
}

bool PropertyTest::testPropertyAll()
{
	bool bRet = true;

	if (!testPropertyEmpty()) {
		bRet = false;
		printf(" *** TEST: testPropertyEmpty failed. *** \n");
	} else {
		printf("Test: testPropertyEmpty successfull.\n");
	}
	if (!testPropertyFile()) {
		bRet = false;
		printf(" *** TEST: testPropertyFile failed. *** \n");
	} else {
		printf("Test: testPropertyFile successfull.\n");
	}
	if (!testPropertyDatabase()) {
		bRet = false;
		printf(" *** TEST: testPropertyDatabase failed. *** \n");
	} else {
		printf("Test: testPropertyDatabase successfull.\n");
	}

	return bRet;
}
#endif