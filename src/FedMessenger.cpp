#include "FedMessenger.hpp"
#include "FedServer.hpp"

using namespace ztt::dcs;

           
FedMessenger::~FedMessenger() {

}

void FedMessenger::setMessage(MessageStruct* pMsg) {

    // write an assignment operator for MessageStructs - done
    mMessage = *pMsg;

    // test, if server is running
    if (mpServer->getState() == FED_SERVER_RUNNING) {
        updateService();
    }
    
}

void FedMessenger::setLogLevel(unsigned int level) {
    mLogLevel = level | Msg_Alarm;
}


#ifdef __UTEST
MessageStruct* FedMessenger::getMessage() {
    return &mMessage;
}
#endif
