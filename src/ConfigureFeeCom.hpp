#ifndef CONFIGUREFEECOM
#define CONFIGUREFEECOM

#include "SendWithoutCoCo.hpp"
#include "FedCommand.hpp"
#include "FeeIclAck.hpp"
#include "AnswerStruct.hpp"
#include <boost/scoped_ptr.hpp>
#include "ace/Synch.h"
#include "WatchDog.hpp"

/**
* the channel structure of the configurefeecom command channel
*/
#define CONFIGUREFEECOM_STRUCTURE "I:1;I:1;F:1;C:256"
/**
* define the time limit after a thread stucks
*/
#define CFC_WATCH_DOG_TIMEOUT 10000L
/**
* load the feeserver names the intercomlayer has suscribe to
*/
#define SET_FEESERVER_NAMES 0x0001
/**
* load the feeservice names the intercomlayer has suscribe to
*/
#define SET_FEESERVICE_NAMES 0x0002
/**
* required to set the log level of the intercomlayer and feeserver
*/
#define SET_LOG_LEVEL 0x0003
/**
* request the loglevel of intercomlayer and feeserver
*/
#define GET_LOG_LEVEL 0x0004
/**
* set the update rate for the corresponding service
*/
#define SET_UPDATE_RATE 0x0005
/**
* request the update rate of the corresponding service
*/
#define GET_UPDATE_RATE 0x0006
/**
* set timelimit for watchdog after it is mark as "not responding"
*/
#define SET_TIME_OUT 0x0007
/**
* get the watchdog timeout limit
*/
#define GET_TIME_OUT 0x0008
/**
* set new deadband for specified service
*/
#define SET_DEAD_BAND 0x0009
/**
* get deadband of specified service
*/
#define GET_DEAD_BAND 0x000A
//________________________________________________________________________________________
/**
 * Bitset to signal command for FeeServer - sets the deadband for a specified item.
 */
#define FEESERVER_SET_DEADBAND_FLAG 0x0080	// dec 128

/**
 * Bitset to signal command for FeeServer - gives back the deadband of a 
 * specified item.
 */
#define FEESERVER_GET_DEADBAND_FLAG 0x0100		// dec 256

/**
 * Bitset to signal command for FeeServer - sets the timeout for the issue call
 */
#define FEESERVER_SET_ISSUE_TIMEOUT_FLAG 0x0200		// dec 512

/**
 * Bitset to signal command for FeeServer - gives back the issue timeout
 */
#define FEESERVER_GET_ISSUE_TIMEOUT_FLAG 0x0400		// dec 1024

/**
 * Bitset to signal command for FeeServer - sets the update rate for monitoring
 */
#define FEESERVER_SET_UPDATERATE_FLAG 0x0800		// dec 2048

/**
 * Bitset to signal command for FeeServer - gives back the update rate for monitoring
 */
#define FEESERVER_GET_UPDATERATE_FLAG 0x1000		// dec 4096

/**
 * Bitset to signal command for FeeServer - sets the loglevel for the FeeServer
 */
#define FEESERVER_SET_LOGLEVEL_FLAG 0x2000		// dec 8192

/**
 * Bitset to get the loglevel of the feeserver
 */
#define FEESERVER_GET_LOGLEVEL_FLAG 0x4000		// dec 16384
//_________________________________________________________________________________________
/** 
 * Container of FeeCom 
 */
typedef struct ContConfigureFeeCom {
	/** Command Id */
	int commandID;
	/** Tag number */
	int iTag;
	/** The tag */
	float fTag;
	/** Target */
	char target[256];
  
  /** 
   * Assignement operator 
   * @param rhs Object to assign from
   * @return  Reference to this object
   */
	ContConfigureFeeCom& operator=(const ContConfigureFeeCom& rhs){
			if(this==&rhs){
				return *this;
			}
			commandID=rhs.commandID;
			iTag=rhs.iTag;
			fTag=rhs.fTag;
			memcpy(target,rhs.target,256);
			
			return *this;
			
		}
}ContConfigureFeeCom;

namespace ztt {	namespace dcs{

/**
* ConfigureFeeCom is the command channel for the configurefeecom command.
* it handle the command and decide where the target of the command is located.
* to increase the safety of the application a watchDog thread is started that 
* check the elapsed time of the answer. It also handle the answers of a broadcast
* and single commands.
*
* @date 10.01.05
* @author Benjamin Schockert
*/
class ConfigureFeeCom:public FedCommand{

public:
      /**
       * constructor of configurefeecom initialize and create the
       * required variables
       * @param name Name 
       * @param handler The handler used 
       */
      ConfigureFeeCom(char* name,DimCommandHandler* handler):FedCommand(name,CONFIGUREFEECOM_STRUCTURE,handler),keyWatchDog(0),
		mutex(),cond(mutex),iclAck(FeeIclAck::createInstance()),mWatchDog("ConfigureFeeCom",&mAnswerStruct,&busy),busy(false){
		//create and start watchDog threadpool
		mWatchDog.activate(THR_DETACHED|THR_NEW_LWP,5,1);
		//create new message block
		mb = new ACE_Message_Block((char*)&keyWatchDog);
	};
	/**
	* the copy constructor
	*/
/*	ConfigureFeeCom(const ConfigureFeeCom& conf):cond(conf.mutex){
		send = conf.send;
		expectedAnswer = conf.expectedAnswer;
		iclAck = conf.iclAck;
		busy = conf.busy;
	};*/
	/**
	* the assignment operator
	*/
/*
	ConfigureFeeCom operator=(const ConfigureFeeCom& conf){
		send = conf.send;
		expectedAnswer = conf.expectedAnswer;
		iclAck = conf.iclAck;
		busy = conf.busy;
	};*
	/**
	* destructor of configurefeecom
	*/
      virtual ~ConfigureFeeCom();
      /**
      * this method handle the incoming commands and start the proper
      * action
      */
      virtual bool handleCommand();
      /**
      *
      */
      virtual bool setAnswerData(const int& ID);
      /** 
       * Start the watch dog 
       * @param objectHandler Handler 
       * @return 
       */
      static void* watchDog(void* objectHandler);
      /** 
       * @return Whether we're busy 
       */
      bool isBusy();
	
private:
	/**
	* Task to send commmands with out using the command coder 
	*/
	SendWithoutCoCo send;
	/**
	* mutex to lock the critical section
	*/
	ACE_Thread_Mutex mutex;
	/**
	* mb is used to transport the answer id into the threads
	*/
	ACE_Message_Block* mb;
	/**
	* condition value to wakeup watchdog when operation was ok
	*/
	ACE_Condition<ACE_Thread_Mutex> cond;
	/**
	* vector store the expected answer id�s
	*/
	//std::vector<int> expectedAnswer;
	/**
	* handle to the intercom acknowledge channel
	*/
	boost::scoped_ptr<FeeIclAck> iclAck;
	
	/**
	* store the first expected ID. This is used as key.
	*/
	int keyWatchDog;
	/**
	* mAnswerStruct hold a thread safe structure to synchronize the access from the WatchDog,
	* CommandChannel and getAnswer.
	*/
	AnswerStruct mAnswerStruct;

	/** 
	* Watch dog 
	*/
	WatchDog mWatchDog;
	/**
	* mark channel busy
	*/
	bool busy;
	
	
		
};
inline bool ConfigureFeeCom::isBusy(){
	return busy;
}

//end namespace
}}

#endif

