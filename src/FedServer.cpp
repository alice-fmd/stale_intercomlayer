#include "FedServer.hpp"
#include "InterCom.hpp"
#include "FeeItemInfo.hpp"
#include "FeeAckInfo.hpp"
#include "FedCommand.hpp"
#include "fee_errors.h"
#include <string>



using namespace ztt::dcs;
using namespace std;


FedServer::~FedServer() {
    mState = FED_SERVER_STOPPED;
	// stop server
	this->stop();

	delete mpFedMessenger;
	configFeeCom.reset();
	configFero.reset();
	controlFeeCom.reset();
	controlFero.reset();

}

int FedServer::publishServices() {
    // amount is 1 at startup, because FedMessenger is already declared in
    // the constructor (=> 1st service of FedServer)
	int amount = 1;
	int count = 0;

	// add the FedServer itself as exit handler to avoid exit() call of framework
	addExitHandler(this);
	//InterComLayer Acknowledge channel
	//iclAck = FeeIclAck::createInstance();

	//define for every feeserver the acknowledge channel
	//go through the FeeServer names
	vector<FeeAckInfo*>* pAckCol = mpInterCom->getFeeAckInfoCollection();
	vector<FeeAckInfo*>::iterator itAckServ = pAckCol->begin();
	//get FeeItemInfo reference
	vector<FeeItemInfo*>* pItemCol = mpInterCom->getFeeItemInfoCollection();
	vector<FeeItemInfo*>::iterator itItemServ;

	while (itAckServ != pAckCol->end()) {
			FeeAckInfo* info = *itAckServ;

			//Starting of implementation of ACK-Channel for all servers
			//-----------------------------------------------------------------------
			// creat answer channel for FEDAPI
			char* answerName = new char[strlen(info->getCorrespondingServerName()) + strlen(FED_SERVER_NAME) + 15];
			sprintf(answerName, "%s_", FED_SERVER_NAME);
			strcat(answerName, info->getCorrespondingServerName());
			strcat(answerName, "_ACK\0");
			
			boost::shared_ptr<DimService> mpAnswerChannel(new DimService(answerName, ACK_STRUCTURE, &info->sAckStruct,sizeof(AckStructure)));
			//set answer and command channels
			info->setFedApiChannels(configFeeCom,controlFero,configFero);
			info->setAckChannel(mpAnswerChannel);
			//assign the correct services to the corresponding channels
			if(mDataAccess->grouped){
				boost::shared_ptr<FeeGroupedChannel> feegroup(new FeeGroupedChannel());
				
				char* name = new char[256];
				name[sprintf(name, "%s_%s_Services", FED_SERVER_NAME,info->getCorrespondingServerName())] = 0;
				//create group name service
				//DimService* service = new DimService(name,BUNDLE_STRUCTURE,&feegroup->gMsg,sizeof(GroupedMessage));
				boost::shared_ptr<DimService> service(new DimService(name,BUNDLE_STRUCTURE,&feegroup->gMsg,sizeof(GroupedMessage)));
				itItemServ = pItemCol->begin();
				//set for every service of a server the corresponding broadcast channel
				while (itItemServ != pItemCol->end()) {
					//assign Server and Servicenames and extract the servernames from the servicename
					string sFeeItemInfo = (*itItemServ)->name;
					string sAckInfo = info->getCorrespondingServerName();
					int dist = sFeeItemInfo.find_first_of("_");	
					string tmp(sFeeItemInfo,0,dist);
				
					if(sAckInfo.compare(tmp) == 0 ){

						(*itItemServ)->setFeeGroupChannel(feegroup);
						(*itItemServ)->setCorrespondingFedService(service);
						count++;
					}
					++itItemServ;
				}
				mFedServiceCollection.push_back(service);
				delete[] name;
				feegroup.reset();
				service.reset();
			}
		delete[] answerName;
		mpAnswerChannel.reset();
		++amount;
		++itAckServ;
	}   
	if(!mDataAccess->grouped){
		itItemServ = pItemCol->begin();
		while (itItemServ != pItemCol->end()) {
			//FeeItemInfo* pItemInfo = *itItemServ;
			// publish all items from FeeClients
			char* name = new char[strlen((*itItemServ)->getName()) + strlen(FED_SERVER_NAME)+ 2];
			name[sprintf(name, "%s_%s", FED_SERVER_NAME, (*itItemServ)->getName())] = 0;
			//for debugging only!!
			//ofs<<name<<endl;
			// create a dim service with the reference to float of the 
			// corresponding info item object
			boost::shared_ptr<DimService> service(new DimService(name, (*itItemServ)->getRefFloatValue()));
			// pass the pointer of new service object to corresponding info 
			// item object in order to call updateService directly when new data 
			//has arrived (task of client area)
			(*itItemServ)->setCorrespondingFedService(service);
			// store new service in collection
			mFedServiceCollection.push_back(service);
			delete[] name;
			service.reset();
			count++;
			++amount;
			++itItemServ;
		}
	}
	//if count is 0 no services of server are published
	if(count == 0){
		const Logger* log = Logger::getLogger();
		char message[100];
		message[sprintf(message,"No Service published!Please check service list")]=0;
		log->createLogMessage(Msg_Alarm,SOURCE_LOCAL,message);
		cout << "No Services published!Please check service list!"<<endl;
		exit(0);
	}

	// start FED-Server 
	this->start(FED_SERVER_NAME);
    mState = FED_SERVER_RUNNING;
	
	return amount;
}
FedMessenger* FedServer::getMessenger() {
    return mpFedMessenger;
}
void FedServer::commandHandler(){
	
	try{
		FedCommand* fed = dynamic_cast<FedCommand*>(getCommand());
		if(!fed->handleCommand()){
			  char msg[255];

			 msg[sprintf(msg, "Received invalid command data or target name %s ",fed->getName())] = 0;
			 mpInterCom->setLogEntry(Msg_Error, SOURCE_FEE_CLIENT, msg);

			return;
		}
	}catch(bad_cast& e){
			char msg[255];
			msg[sprintf(msg, "Received no valid FedApi command %s", 
				e.what())] = 0;		
			mpInterCom->setLogEntry(Msg_Error, SOURCE_FEE_CLIENT, msg);
	}
}

