#ifndef FEE_SERVER_MAP
#define FEE_SERVER_MAP

#include <map>
#include <vector>
#include <string>
#include <stdexcept>
#include "ace/Synch.h"

namespace ztt { namespace dcs {

/**
* The FeeServerMap class store the feeserver names with a unique key in an associativ
* container. This idea was introduced to control different hardware types by the ICL.
* To contact the different hardware types the naming convention of the FEDAPI is used.
*
* @date 23-08-06
* @author Benjamin Schockert
*/

class FeeServerMap
{
public:
	//standard constructor
	FeeServerMap():xCoor(0),yCoor(0),zCoor(0),mutex(){};
	//copy
	FeeServerMap(int x,int y, int z):mutex(){
		if((x<=0)||(y<=0)||(z<=0)){
			 throw std::logic_error(
            "At least one dimension size is zero or below!");
		}else{
			xCoor = x;
			yCoor = y;
			zCoor = z;
		}
	};
	//copy constructor
	FeeServerMap(const FeeServerMap& t):mutex(){
		collection = t.collection;
		resultSet = t.resultSet;
		xCoor = t.xCoor;
		yCoor = t.yCoor;
		zCoor = t.zCoor;
	}
	//destructor clean the container
	~FeeServerMap(void){
		collection.clear();
		resultSet.clear();
		mutex.remove();
	}
	//overload assignment operator
	FeeServerMap& operator=(const FeeServerMap& t){
		if(this != &t){
			collection = t.collection;
			resultSet = t.resultSet;
			xCoor = t.xCoor;
			yCoor = t.yCoor;
			zCoor = t.zCoor;
		}
		return *this;
	}
	/**
	* This function store the servernames in the map.
	*
	* @param std::vector<int> x store the x coordinates of the several servers
	* @param std::vector<int> y store the y coordinates of the several servers
	* @param std::vector<int> z store the z coordinates of the several servers
	* @param std::vector<char*> server contain the all servernames
	* @param char* name is required to create a unique key
	*/
	void setFeeServer(std::vector<int> x,std::vector<int> y,std::vector<int> z,std::vector<char*> server,
					  char* name);
	void setFeeServer(int x, int y, int z, std::vector<char*> server, char* name);
	/**
	* This function returns a vector with the desired feeserver.
	*
	* @param int x store the x coordinates of the desired servers
	* @param int y store the y coordinates of the desired servers
	* @param int z store the z coordinates of the desired servers
	* @param char* store the name of the hardware to create the key
	*/
	std::vector<char*> getFeeServer(int,int,int,char*);

	std::vector<char*> getAllFeeServer();

        void print() const;
private:
	/**
	* The map store all feeserver with a unique key.
	*/
	std::map<std::string,std::string> collection;
	/**
	* The vector contain the result of the getFeeServer call
	*/
	std::vector<char*> resultSet;
	/**
	* The int values contain the upper bounds of the structure
	*/
	int xCoor;
	int yCoor;
	int zCoor;

	ACE_Mutex mutex;
};
//end namespace
}}
#endif

