#include "DataAccess.hpp"
#include "Logger.hpp"
#include "fee_errors.h"
#include "Property.h"
#include "Database.hpp"
#include "OracleDB.hpp"
#include "FeeServerMap.hpp"
#include "InterCom.hpp"

#ifdef __DEBUG
#include <iostream>
#endif

#include <fstream>
#ifndef CONFIGDIR
# define CONFIGDIR ""
#endif

using namespace ztt::dcs;
using namespace std;



DataAccess* DataAccess::createDataAccess()
{
	/**
	* the Singleton object (see Effective C++,Scott Meyers)
	* will be destroyed at the end of the programm
	*/
	static DataAccess pInstance;

	return &pInstance;
}

DataAccess::DataAccess() :grouped(true),	logging(false),xValue(0), yValue(0), zValue(1), configDir(CONFIGDIR) {
}

DataAccess::~DataAccess() {
	tablenameServer.clear();
	tablenameService.clear();
	dbOptions.clear();
	posX.clear();
	posY.clear();
	posZ.clear();
}

int DataAccess::fillCollectionFromFile(char* tag,vector<char* >* collection)
{
    // here starts the method with loading names from file
    ifstream* hFile;

    std::string filename;
#ifndef WIN32
    filename += configDir;
#endif
    filename += tag;
    hFile = new ifstream(filename.c_str());

    if (!hFile->is_open()) {
#ifdef __DEBUG
		//TODO 
		//include logger
      cout << "ERROR opening file: " << filename << endl;
#endif
        delete hFile;
        return DATA_NOT_FOUND;
    }

    while ((!hFile->bad()) && (!hFile->eof())) {
        char line[256];
        char* ptrDelimiter;
        char* name;
        int size;
        hFile->getline(line, 256);

        // skip empty lines
        if (strlen(line) == 0) {
            continue;
        }
        ptrDelimiter = strstr(line, "#\0");
        if (ptrDelimiter != 0) {
            *ptrDelimiter = '\0';
        }
        ptrDelimiter = strstr(line, " \0");
        if (ptrDelimiter != 0) {
            *ptrDelimiter = '\0';
        }
        size = strlen(line);

        // add entry only when it is at least 1 char long
        if (size == 0) {
            continue;
        }
        name = new char[size + 1];
        memcpy(name, line, size);
        name[size] = '\0';
        // store name
        collection->push_back(name);
    }

    hFile->close();
    delete hFile;

    return collection->size();
}

int DataAccess::getDbInfoFromFile(char* tag)
{
	string temp;
	int index;

	if (strcmp(tag,"")== 0)
	{
#ifdef __DEBUG
  		cout << "no file name for Database file specified" << endl;
#endif
		return DATA_NOT_FOUND;
	}
	std::string filename;
#ifndef WIN32
	filename += configDir;
#endif
	filename += tag;
	ifstream fin (filename.c_str(), ios_base::in);
	if(!fin)
	{
#ifdef __DEBUG
		//!!!??add logger in case of error??!!!
		cout << "Something went wrong while loading Database files" << endl;
#endif
		return DATA_NOT_FOUND;
	}
	while(!fin.eof() && !fin.bad())
	{
		char * name = new char[255];
		fin.getline(name,256);
		index = strlen(name);
		name[index+1]= '\0';
		dbOptions.push_back(name);
	}
	fin.close();
	//load the content of the file into the corresponding variables
	vector<char *>::iterator itDb = dbOptions.begin();
	char* row = *itDb;
	while(itDb != dbOptions.end())
	{
		if(strstr(row,"username")){
			string temp;
			temp=row;
			index = temp.find("=");
			temp.erase(0,index+1);
			userName = temp;
		}
		if(strstr(row,"password")){
			string temp;
			temp=row;
			index = temp.find("=");
			temp.erase(0,index+1);
			password = temp;
		}
		if(strstr(row,"database")){
			string temp;
			temp=row;
			index = temp.find("=");
			temp.erase(0,index+1);
			database = temp;
		}
		if(strstr(row,"tablenameserver")){
			string temp;
			temp=row;
			index = temp.find("=");
			temp.erase(0,index+1);
			temp.copy(row,string::npos);
			row[temp.length()]=0;
			tablenameServer.push_back(row);
		}
		if(strstr(row,"tablenameservices")){
			string temp;
			temp=row;
			index = temp.find("=");
			temp.erase(0,index+1);
			temp.copy(row,string::npos);
			row[temp.length()] = 0;
			tablenameService.push_back(row);

		}
		itDb++;
		row = *itDb;
	}
	
	return dbOptions.size();
}
int DataAccess::loadFiles(vector<char* >* server,vector<char* >* service)
{
	Property prop;
	
	int nRet = 0;
	std::vector<char*>::iterator itFileName;
	// decide which operation should done
	valueselect = prop.getDatafromFile(configDir);
	//get an iterator on the first value of the vector
	itFileName=prop.list.begin();
	// assign logging status to local variable
	logging = prop.logging;
	//assign grouping flag to local variable
	grouped = prop.grouped;
	//assign detector coordinate x to local variable
	xValue = prop.x;
	//assign detector coordinate y to local variable
	yValue = prop.y;
	//assign detector coordinate z to local variable
	zValue = prop.z;
	//which action should be done(read from stdin,from file,from db)
	if(valueselect == 0){
		return 0;
	}
	if(valueselect == 1){
		//load servernames from file
		nRet = fillCollectionFromFile(*itFileName,server);
		if(nRet <= 0){
			return 0;
		}
		++itFileName;
		//load servicenames from file
		nRet = fillCollectionFromFile(*itFileName,service);
		if(nRet <= 0){
			return 0;
		}
	}
	if(valueselect == 2){
		
		//the try block is required because the FeeMap can throw exceptions in case of invalid values
		try{
			InterCom* icl = InterCom::createInterCom();
			//create and fill the map with the values loaded from the database
			//after filling the map copy the values to the map object stored 
			//in the InterComLayer main class
			//avoid misconfiguration in central part of ICL
			FeeServerMap feemap(xValue,yValue,zValue);		
			//feemap = new FeeServerMap(xValue,yValue,zValue);
			//load the datbase values from file
			nRet = getDbInfoFromFile(*itFileName);
			if(nRet <= 0){
				return 0;
			}
			// define the databasse interface (Strategy Pattern)
			Database *database = new OracleDB();
			// instanciate the connection pool
			nRet = database->createConnection();
			if(nRet <= 0){
				return 0;
			}
			list<char*>::iterator itServer;
			list<char*>::iterator itService;

			itServer = tablenameServer.begin();
			itService = tablenameService.begin();

			while((itServer != tablenameServer.end()) && (itService != tablenameService.end())){
				server->clear();
				posX.clear();
				posY.clear();
				posZ.clear();

				//get the servername with position from the table
				nRet = database->getDataFromDB(*itServer,server,&posX,&posY,&posZ);
				if(nRet <= 0){
					return 0;
				}
				feemap.setFeeServer(posX,posY,posZ,*server,*itServer);
				
				//get the servicenames from the table
				nRet = database->getDataFromDB(*itService,service);
				if(nRet <= 0){
					return 0;
				}
				itServer++;
				itService++;
			}
			// close the connection pool
			database->killConnection();
			if(nRet <= 0){
				return 0;
			}
			//copy the filled map into the main object of the ICL
			
			icl->setFeeServerMap(feemap);
		}catch(std::logic_error& e){
			const Logger* logIt = Logger::getLogger();
			char text[100];
			text[sprintf(text,(char*)e.what())]=0;
			cout<<text<<endl;
			logIt->createLogMessage(Msg_Alarm, SOURCE_LOCAL,text);
			return 0;
		}
			

	}
	return server->size();
	
}

