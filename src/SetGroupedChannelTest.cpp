#ifdef __UTEST

#include "SetGroupedChannelTest.hpp"
#include <iostream>


using namespace ztt::dcs;
using namespace std;

SetGroupedChannelTest::SetGroupedChannelTest(void)
{
}

SetGroupedChannelTest::~SetGroupedChannelTest(void)
{
}
bool SetGroupedChannelTest::TestCreateGroupedMessage(){

	bool nRet = false;

	FeeGroupedChannel* test = new FeeGroupedChannel();

	if((test->gMsg.iVal==0)&&(test->gMsg.fVal==0.0)&&(strcmp(test->gMsg.cName,"")==0)){
		nRet=true;
	}
	return nRet;
}
bool SetGroupedChannelTest::TestAssignmentOperator(GroupedMessage* mess){

	bool bRet=false;
	FeeGroupedChannel* gMesg=new FeeGroupedChannel();

	gMesg->setGroupedChannel(mess);

	if((gMesg->gMsg.iVal==mess->iVal)&&(gMesg->gMsg.fVal==mess->fVal)&&(strcmp(gMesg->gMsg.cName,mess->cName))){
		bRet = true;
	}
	return bRet;
}

bool SetGroupedChannelTest::TestOverloadConstructor(int i,float f,char* target){

	bool bRet=false;

	GroupedMessage gMess(i,f,target);
	if((gMess.iVal==i)&&(gMess.fVal==f)&&(strcmp(gMess.cName,target))){
		bRet=true;
	}
	return bRet;
}
bool SetGroupedChannelTest::TestAll(){

	if(TestCreateGroupedMessage()){
		cout<<"Test create GroupedMessage sucessfull"<<endl;
	}else{
		cout<<"Test create GroupedMessage failed"<<endl;
	}
	GroupedMessage mess;
	mess.iVal=12;
	mess.fVal=22,45;
	mess.cName[sprintf(mess.cName,"Das ist ein Test")]=0;
	if(TestAssignmentOperator(&mess)){
		cout<<"Test assignment operator GroupedMessage sucessfull"<<endl;
	}else{
		cout<<"Test assignment operator GroupedMessage failed"<<endl;
	}
	
	int i = 34;
	float f=55;
	char* test = new char[20];
	test[sprintf(test,"Test2")] = 0;
	if(TestOverloadConstructor(i,f,test)){
		cout<<"Test overload constructor GroupedMessage sucessfull"<<endl;
	}else{
		cout<<"Testoverload constructor GroupedMessage failed"<<endl;
	}
	return true;
}
#endif