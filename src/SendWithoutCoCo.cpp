#include "SendWithoutCoCo.hpp"
#include "InterCom.hpp"
#include "Logger.hpp"

using namespace ztt::dcs;
using namespace std;

int SendWithoutCoCo::svc(void){

	//copy transmitted values to local values
	/* TODO remove copy logic */
	boost::shared_ptr<FeePacket> feePacket;
	feePacket.swap(packet);
	//vector<char*> serverContainer = servernames;
	//servernames.clear();
	const Logger* log = Logger::getLogger();
	//this value is required to differ between
	//a normal command and a broadcast
	int number = 0;
	InterCom* icl = InterCom::createInterCom();
	int mResult;
	char* server= new char[256];
	//set the iterator to the begin of the server vector
	vector<char*>::iterator iter = feePacket->feeServerNames.begin();

	while(iter != feePacket->feeServerNames.end()){
		/*int id = packet->getId();
		cout<<" Packet ID: "<<id<<endl;*/
		server[sprintf(server,*iter)]=0;

		if(icl->validateServerName(server)){
			if(number != 0){
				feePacket->increaseID();
			}
			strcat(server,"_Command");
			mResult = icl->sendCommand(server,feePacket);
		}else{
#ifdef __DEBUG
			cout<<"no link to server: "<< server <<endl;
#endif
			try{
				char text[100];
				text[sprintf(text,"no link to server: %s",server)]=0;
				log->createLogMessage(Msg_Warning,SOURCE_LOCAL,text);
			}catch(std::runtime_error &excep){
				cout << excep.what() << endl;
			}
		}
		number++;
		iter++;
	}
	delete[] server;
	//serverContainer.clear();

	return 0;
}
SendWithoutCoCo::~SendWithoutCoCo(){
}
