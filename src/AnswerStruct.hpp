#ifndef ANSWER_STRUCT
#define ANSWER_STRUCT

#include "ace/Map_Manager.h"
#include "ace/Synch.h"
#include "ace/Process_Semaphore.h"

#include <set>
#include <map>


namespace ztt{
	namespace dcs{


class AnswerStruct
{
public:
	/**
	* AnswerStruct constructor which initialize the used semaphore.
	*/
	AnswerStruct(void):mut(){};//sem(1,"AnswerStruct"){};
	/**
	* AnswerStruct destructor remove the semaphore and tidy up the map.
	*/
	~AnswerStruct(void){
		//sem.remove();
		idMap.clear();
		mut.remove();
	};
	/**
	* setAnswerStruct function fill the map. 
	*
	* @param key contain the key to the corresponding set.
	* @param id contain the single ids of a command.
	*/
	void setAnswerStruct(const int key,const std::map<int, char*>& id);
	/**
	* findAnswerID look in the map for the id.
	*
	* @param id contain the id of a answer received from a FeeServer.
	*/
	int findAnswerID(const int& id);
	/**
	* freeAnswerStruct delete a entry in the map. This function is called
	* by the watchDog of each CommandChannel.
	*
	* @param key reference to the key.
	*/
	std::map<int,char*> freeAnswerStruct(const int& key);
private:
	/**
	* sem is a semaphore which synchronize the access to the map.
	*/
	//ACE_Process_Semaphore sem; 
	ACE_Thread_Mutex mut;
	/**
	* idMap is the map.
	*/
	std::map<int,std::map<int,char*> > idMap;
	/**
	* the assignment operator and the copy constructor are
	* private to avoid any copies of the object.
	*/
	AnswerStruct& operator=(const AnswerStruct&){return *this;};
	AnswerStruct(const AnswerStruct&){};

};
//end namespace
}}
#endif
