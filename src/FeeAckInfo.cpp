#include "FeeAckInfo.hpp"
#include "FeePacket.hpp"
#include "FeeClient.hpp"
#include "Logger.hpp"

#include "fee_errors.h"

#include <fstream>


#include <iomanip>
#ifdef __DEBUG
#include <iostream>
#include <time.h>
#include <sys/types.h>
#include <sys/timeb.h>
#endif

using namespace std;
using namespace ztt::dcs;

FeeAckInfo::~FeeAckInfo() {
    if (mpServerName != 0) {
		delete[] mpServerName;
	}
	mpConfigureFeeCom.reset();
	mpConfigureFero.reset();
	mpControlFero.reset();
	mpAckService.reset();
	/*if (sAckStruct != 0) {
		delete[] sAckStruct;
	}*/
}

bool FeeAckInfo::retrieveServiceValue() {

    if (getSize() < FEE_PACKET_HEADER_SIZE) {
		// to short -> service value cannot contain own protocol definition
		
		
		//check that getData has a content and is no null pointer
		if(getData() == NULL){
			try{
				const Logger* logger = Logger::getLogger();
				char message[256];
				message[sprintf(message,"Received null pointer from DIM in FeeAckInfo, value rejected")] = 0;
				logger->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
			}catch(std::runtime_error &excep) {
				// Can't print and send log message,
				// but this is not so critical (Server-functionality is not affected).
				cerr << "Exception: " << excep.what() << endl;
			}
			return false;
		}
		// test for "No link"
		char testData[FEE_ACK_NO_LINK_SIZE + 1];
		// was passiert eigentlich mit dem Speicher der mit getData alloziiert
		// wird wann, wo und wer l�sst den wieder frei ?
        // -> DimFramework takes care of it !
		memcpy((void*) testData, (void*) getData(), FEE_ACK_NO_LINK_SIZE);
			testData[FEE_ACK_NO_LINK_SIZE] = 0;
		// compare to "No Link",
		// if yes, notify of server down
		if (strcmp(testData, FEE_ACK_NO_LINK) == 0) {
            // notify FedServer of lost link !!!

			ackState = FEE_ACK_LOST_LINK;
			// no link to service
			return false;
		}

#ifdef __DEBUG
		cout << "Size < HEADER_SIZE\n";
		cout << "Stream: " << getString() << endl;
#endif

		// Link available but header and payload to short
        // notify FedServer of bad data !!!  

        // making log entry
        try {
            const Logger* aLogger = Logger::getLogger();
            char msg[200];

            msg[sprintf(msg, "Received strange acknowledge data from %s: %s.",
                    getName(), getString())] = 0;
            aLogger->createLogMessage(Msg_Warning, getName(), msg);
        } catch (std::runtime_error &excep) {
            // Can't print and send log message,
            // but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
            cerr << "Unable to use Logger, no log message provided." << endl;
            cerr << "Exception: " << excep.what() << endl;
#endif
        }

		ackState = FEE_ACK_STRANGE_DATA;
		return true;
	}

	ackState = FEE_ACK_LINK_OK;
	if(getData() == 0){
		try{
			const Logger* logger = Logger::getLogger();
			char message[256];
			message[sprintf(message,"Received null pointer from DIM in FeeAckInfo, value rejected")] = 0;
			logger->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		return false;
		
	}
	boost::scoped_ptr<FeePacket> pResult(FeePacket::unmarshall(
                (char*) getData(), getSize()));
	 if (!pResult->isValid()) {
        cout << "packet has invalid checksum!" << endl;
    }
#ifdef __DEBUG
    cout << "ACK from " << mpServerName << ": ";
	//!!!using isValid outside of debug block!!!
#endif
	if ((mpConfigureFeeCom == NULL )||(mpControlFero == NULL)||(mpConfigureFero == NULL)||(!pResult->isValid())){
		return false;
	}
#ifdef __BENCHMARK
	try{
		char message[256];
		const Logger* logger = Logger::getLogger();
		ACE_Time_Value ts = ACE_OS::gettimeofday();
		struct timeval tStamp = ts.operator timeval();
		struct tm *ptimes = localtime(&(tStamp.tv_sec));

		message[sprintf(message,"Benchmark ICL Timestamp(Receive ACK) ID:%d, Target:%s, Time:%.8s - %ld",pResult->getId(),
			mpServerName, asctime(ptimes) + 11, ts.usec())]=0;
		logger->createLogMessage(Msg_Debug,SOURCE_LOCAL,message);
		
	}catch(std::runtime_error &excep){
		cerr << "Exception in FeeAckInfoBenchmark: " << excep.what() << endl;
	}
#endif
    if (mpConfigureFeeCom->isBusy()||mpControlFero->isBusy()||mpConfigureFero->isBusy()) {
        // Check errorCode
		
        recErrorCode = pResult->getErrorCode();
		switch(recErrorCode){
			case(FEE_OK):{
#ifdef __DEBUG
			  cout << "Received 'OK'.\n";
#endif
				char res[256];
				int arraySize = (256+pResult->getPayloadSize());
				char* payloadarray = new char[arraySize];
				if ((pResult->getFlags() & FEESERVER_GET_DEADBAND_FLAG) >
					NO_FLAGS) {
					float db;
					char name[50];
					memcpy(&db, pResult->getPayload(), sizeof(db));
					memcpy(name, pResult->getPayload() + sizeof(db),
					pResult->getPayloadSize() - sizeof(db));
					name[pResult->getPayloadSize() - sizeof(db)] = 0;
					res[sprintf(res, "%s: Deadband of %s is %f.", getName(),
								name, db)] = 0;
				} else if ((pResult->getFlags() & FEESERVER_GET_ISSUE_TIMEOUT_FLAG) >
							NO_FLAGS) {
					unsigned long to;
					memcpy(&to, pResult->getPayload(), sizeof(to));
					res[sprintf(res, "%s: Timeout is %lu [ms].", getName(),
								to)] = 0;
				} else if ((pResult->getFlags() & FEESERVER_GET_UPDATERATE_FLAG) >
							NO_FLAGS) {
					unsigned short ur;
					memcpy(&ur, pResult->getPayload(), sizeof(ur));
					res[sprintf(res, "%s: update rate is %d [ms].", getName(),
							ur)] = 0;
				}else if ((pResult->getFlags() & FEESERVER_GET_LOGLEVEL_FLAG) >
							NO_FLAGS) {
					unsigned int ll;
					memcpy(&ll, pResult->getPayload(), sizeof(ll));
					res[sprintf(res, "%s: loglevel is %d [ms].", getName(),
								ll)] = 0;
        		}else {
					Timestamp ts;
					res[sprintf(res,"%s;%s",getName(),ts.date)]=0;
				}
				//copy string into array
				int lengthres = sprintf(payloadarray,"%s;%d;",res, pResult->getPayloadSize());
				payloadarray[lengthres]=0;
				//copy the payload received from the feeserver into the array. payload can be text or binaries.
				memcpy(payloadarray+(lengthres+1),pResult->getPayload(),pResult->getPayloadSize());
				AckStructure sAck(pResult->getErrorCode(),arraySize,payloadarray);
				setAckStructure(&sAck);
				if(mpConfigureFeeCom->isBusy()){
					if(mpConfigureFeeCom->setAnswerData(pResult->getId())){
							mpAckService->updateService();
					}
				}if(mpControlFero->isBusy()){
					if (mpControlFero->setAnswerData(pResult->getId())){
							mpAckService->updateService();
					}
				}if(mpConfigureFero->isBusy()){
					if(mpConfigureFero->setAnswerData(pResult->getId())){
							mpAckService->updateService();
					}
				} else {
					// call was NOT successfull -> wrong ID, do something ???
				}
				delete[] payloadarray;
			break;
			}
			// timeout occured in CE
			case(FEE_TIMEOUT):{
				try{
					const Logger* logger = Logger::getLogger();
					char message[256];
					message[sprintf(message," TimeOut occured in CE.")] = 0;
					logger->createLogMessage(Msg_Warning,SOURCE_FEE_CLIENT,message);
				}catch(std::runtime_error &excep) {
					// Can't print and send log message,
					// but this is not so critical (Server-functionality is not affected).
					cerr << "Exception: " << excep.what() << endl;
				}  //LOGGER???
				char result[256];
				result[sprintf(result,"%d TimeOut occured in CE",recErrorCode)]=0;
				if(mpConfigureFeeCom->isBusy()){
					if(mpConfigureFeeCom->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
				}
				}if(mpControlFero->isBusy()){
					if (mpControlFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
				}
				}if(mpConfigureFero->isBusy()){
					if(mpConfigureFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
				}
				}else {}	// call was NOT successfull -> wrong ID, do something ???
			break;
			}
			case(FEE_CHECKSUM_FAILED):{
				try{
					const Logger* logger = Logger::getLogger();
					char message[256];
					message[sprintf(message,"FeeServer received corrupted data, checksum failed.")] = 0;
					logger->createLogMessage(Msg_Warning,SOURCE_FEE_CLIENT,message);
				}catch(std::runtime_error &excep) {
					// Can't print and send log message,
					// but this is not so critical (Server-functionality is not affected).
					cerr << "Exception: " << excep.what() << endl;
				}
				char result[256];
				result[sprintf(result,"%d FeeServer received corrupted data, checksum failed.", recErrorCode)]=0;
				
				if(mpConfigureFeeCom->isBusy()){
					if(mpConfigureFeeCom->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpControlFero->isBusy()){
					if (mpControlFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpConfigureFero->isBusy()){
					if(mpConfigureFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}else {
						// call was NOT successfull -> wrong ID, do something ???
					}
				break;
			}
			case(FEE_NULLPOINTER):{
				   // FeeServer received null pointer as command
				try{
					const Logger* logger = Logger::getLogger();
					char message[256];
					message[sprintf(message,"FeeServer received null pointer as command.")] = 0;
					logger->createLogMessage(Msg_Warning,SOURCE_FEE_CLIENT,message);
				}catch(std::runtime_error &excep) {
					// Can't print and send log message,
					// but this is not so critical (Server-functionality is not affected).
					cerr << "Exception: " << excep.what() << endl;
				}

				char result[256];
				result[sprintf(result,"%d FeeServer received null pointer as command.",recErrorCode)]=0;
				
				if(mpConfigureFeeCom->isBusy()){
					if(mpConfigureFeeCom->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpControlFero->isBusy()){
					if (mpControlFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpConfigureFero->isBusy()){
					if(mpConfigureFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}else {
						// call was NOT successfull -> wrong ID, do something ???
				}
				break;
			}
		    // command could not be identified
			case(FEE_INVALID_PARAM):{
				try{
					const Logger* logger = Logger::getLogger();
					char message[256];
					message[sprintf(message,"FeeServer received strange FeePacket/-header.")] = 0;
					logger->createLogMessage(Msg_Warning,SOURCE_FEE_CLIENT,message);
				}catch(std::runtime_error &excep) {
					// Can't print and send log message,
					// but this is not so critical (Server-functionality is not affected).
					cerr << "Exception: " << excep.what() << endl;
				}

				char result[256];
				result[sprintf(result,"%d FeeServer received strange FeePacket/-header.",recErrorCode)]=0;
				
				if(mpConfigureFeeCom->isBusy()){
					if(mpConfigureFeeCom->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpControlFero->isBusy()){
					if (mpControlFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpConfigureFero->isBusy()){
					if(mpConfigureFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}else {
						// call was NOT successfull -> wrong ID, do something ???
					}
				break;
			}
			case(FEE_CE_NOTINIT):{
				try{
					const Logger* logger = Logger::getLogger();
					char message[256];
					message[sprintf(message,"CE has not been initialized.")] = 0;
					logger->createLogMessage(Msg_Warning,SOURCE_FEE_CLIENT,message);
				}catch(std::runtime_error &excep) {
					// Can't print and send log message,
					// but this is not so critical (Server-functionality is not affected).
					cerr << "Exception: " << excep.what() << endl;
				}

				char result[256];
				result[sprintf(result,"%d CE has not been initialized.",recErrorCode)]=0;
				
				if(mpConfigureFeeCom->isBusy()){
					if(mpConfigureFeeCom->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpControlFero->isBusy()){
					if (mpControlFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpConfigureFero->isBusy()){
					if(mpConfigureFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}else {
						// call was NOT successfull -> wrong ID, do something ???
					}
				break;
			}
			default:{
				// unknown error code
				try{
					const Logger* logger = Logger::getLogger();
					char message[256];
					message[sprintf(message,"Received unknown error code.")] = 0;
					logger->createLogMessage(Msg_Warning,SOURCE_FEE_CLIENT,message);
				}catch(std::runtime_error &excep) {
					// Can't print and send log message,
					// but this is not so critical (Server-functionality is not affected).
					cerr << "Exception: " << excep.what() << endl;
				}

				char result[256];
				result[sprintf(result,"%d Received unknown error code.",recErrorCode)]=0;
				
				if(mpConfigureFeeCom->isBusy()){
					if(mpConfigureFeeCom->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpControlFero->isBusy()){
					if (mpControlFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}if(mpConfigureFero->isBusy()){
					if(mpConfigureFero->setAnswerData(pResult->getId())){
						mpAckService->updateService(result);
					}
				}else {
						// call was NOT successfull -> wrong ID, do something ???
					}
				break;
			}
        }
    }

#ifdef __DEBUG
    int size = pResult->getPayloadSize();
    char* data = new char[size + 1];
	memcpy(data, pResult->getPayload(), size);
	data[size] = 0;
//	cout << "\n im gSV (\"getServiceValue\"): incoming data (char*): " <<
//                data << endl;
	cout << endl;
	delete[] data;
#endif

    // ??? do something with the pResult !!! then change boost ptr type
	return true;
}

void FeeAckInfo::initialize() {

	ackState = FEE_ACK_LOST_LINK;
}



