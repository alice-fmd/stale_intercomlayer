#ifndef ZTT_DCS_DATA_ACCESS_HPP
#define ZTT_DCS_DATA_ACCESS_HPP

#include <vector>
#include <list>
#include <boost/shared_ptr.hpp>
#include <string>


namespace ztt { 
	namespace dcs {

#define DATA_NOT_FOUND -1


/**
 * This class is used for the database access.
 * Handling the read-out and store data to the database.
 *
 * @author Benjamin Schockert,Sebastian Bablok,Christian Kofler
 */
class DataAccess {
	public:
		/** 
		 * Destructor for database - access handler.
		 */
		~DataAccess();

        /**
         * Retrieves data from a given filename and returns it via smart 
         * pointer.
         * 
         * @param filename the name of the data - file.
         * @param data the returned data [OUT]
         * @param size the size of the returned data [OUT]
         *
         * @return 0, if operation successful, else an error code.
         */
      /*  int getDataFromFile(char* filename, boost::shared_ptr<char>* data,
            int* size);
*/
        /**
         * Retrives the desired FeeServer or FeeService names via Database.
         *
         * @param tag for now this should be the filename, later on this tag 
         *          will identify a set of FeeServers / FeeServices given by
         *          the database.
         * @param collection pointer to vector, which receives the FeeServer /
         *          FeeService names.
         *
         * @return number of names added to vector; DATA_NOT_FOUND (-1)
         *          indicates missing access (filename wrong, DB error)
         */
         int fillCollectionWithNames(char* tag, std::vector<char* >* collection);
		 
		 /*
		 * Retrieves data from a given filename and returns it via smart pointer
		 * 
		 *
		 * @param tag the name of the file
		 * @param collection pointer to vector, which receives the FeeServer /
         *          FeeService names.
		 */
		 int fillCollectionFromFile(char* tag, std::vector<char* >* collection);

		 /*
		 * Retrieves the data required for the database from a file and save the 
		 * to the corresponding variables 
		 *
		 * @param tag the name of the file
		 * @param server/service pointer to vector, which receives the FeeServer /
         *          FeeService names.
		 */
		 int getDbInfoFromFile(char* tag);
		 /*
		 *loading property settings
		 */
		 int loadFiles(std::vector<char* >* server,std::vector<char* >* service);
		/*
		* enable logging of service values
		* required for FeeItemInfo
		*/
		 bool logging;
		/**
		* the flag to decide which kind of channel should be created
		*/
		 bool grouped;
		 int valueselect;
		 int xValue;
		 int yValue;
		 int zValue;
		/*
		* the Singleton function 
		*/
		 static DataAccess* createDataAccess();
  		/* Set the configuration directory.  Must end in a
		 * slash '/'.  
		 * @param dir Directory
		 */
  		void setConfigDir(const std::string& dir) { configDir = dir; }
		/*
		* save the username for db connection
		* !!!check if const is better!!!
		*/
		 std::string userName;
		/*
		* save the password for db connection
		*/
		 std::string password;
		/*
		* save the databasename for db connection
		*/
		 std::string database;
		/*
		* variables to save the given tablenames
		*/
		std::list<char*> tablenameServer;
		std::list<char*> tablenameService;
		//char* groupingCriteria;

		std::vector<int> posX;
		std::vector<int> posY;
		std::vector<int> posZ;

#ifndef __UTEST
	private:
#endif
		/**
		 * Constructor for database - access handler.
		 */
		DataAccess();
		/*
		* save the content of db property file
		*/
		std::vector<char *> dbOptions;
                /*
		 * Configuration directory 
		 */ 
  	        std::string configDir;
  
};  // end of class

} } // end of namespace


#endif
