#ifndef FEE_LOGLEVELS_H
#define FEE_LOGLEVELS_H

/**
 * Define for the even - type "INFO" .
 */
//#define MSG_INFO    1
const int Msg_Info = 1;
/**
 * Define for the even - type "WARNING" .
 */
//#define MSG_WARNING 2
const int Msg_Warning = 2;
/**
 * Define for the even - type "ERROR" .
 */
//#define MSG_ERROR   4
const int Msg_Error = 4;
/**
 * Define for the even - type "FAILURE_AUDIT" .
 */
//#define MSG_FAILURE_AUDIT 8
const int Msg_Failure_Audit = 8;
/**
 * Define for the even - type "SUCCESS_AUDIT" .
 */
//#define MSG_SUCCESS_AUDIT 16
const int Msg_Success_Audit = 16;
//#define MSG_SUCCESS_AUDIT 10
/**
 * Define for the even - type "DEBUG" .
 */
//#define MSG_DEBUG   32
const int Msg_Debug = 32;
//#define MSG_DEBUG   20
/**
 * Define for the even - type "ALARM" .
 */
//#define MSG_ALARM   64
const int Msg_Alarm = 64;
//#define MSG_ALARM   40

/**
 * Defines the maximum value of cumulated event - types.
 */
//#define MSG_MAX_VAL 127
const int Msg_Max_Val = 127;


/**
 * Size of the detector field in messages (in byte).
 */
//#define MSG_DETECTOR_SIZE       4
const int Msg_Detector_Size = 4;
/**
 * Size of the source field in messages (in byte).
 */
//#define MSG_SOURCE_SIZE         256
const int Msg_Source_Size = 256;
/**
 * Size of the description field in messages (in byte).
 */
//#define MSG_DESCRIPTION_SIZE    256
const int Msg_Description_Size = 256;
/**
 * Size of the date field in messages (in byte).
 */
//#define MSG_DATE_SIZE           20
const int Msg_Date_Size = 20;

/**
 * The structure of a message in DIM service.
 */
#define MSG_DIM_STRUCTURE       "I:1;C:4;C:256;C:256;C:20;"

#endif
