#ifndef INTERCOM_MAIN_HPP
#define INTERCOM_MAIN_HPP
#ifndef __STRING__
# include <string>
#endif

namespace ztt 
{
  namespace dcs 
  {
    class InterCom;
    
    /** Class to run InterCom */
    class Main 
    {
    public:
      /** Constructor */
      Main(int argc=0, char** argv=0);
      /** Actual main method 
	  @return return value of program */
      virtual int run();
      /** Static function to run this class */ 
      static int main(int argc=0, char** argv=0);
    protected:
      /** Do unit testing */
      void unitTest(const std::string& what=std::string());
      /** Intercom layer interface */
      InterCom* mIcLayer;
      /** Tests to run */
      std::string mUTests;
    };      
  }
}
#endif
//
// EOF
//


    
