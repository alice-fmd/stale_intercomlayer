#ifndef REG_EXPR
#define REG_EXPR

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <string>
#include <iostream>

namespace ztt{namespace dcs{
/**
* the class RegExpr is used to extract the single figures 
* in the target string and the optional service.
*
* @date 04.01.06
* @author Benjamin Schockert
*/
class RegExpr
{
public:
	/** 
	 * Constructor 
	 */
	RegExpr(void);
	/** 
	 * Destructor 
	 */
	~RegExpr(void);
	/** 
	 * Try to match the regular expression 
	 * @param str      String to matches against 
	 * @param value    Integer values
	 * @param service  Service name
	 * @param hardware Hardware name 
	 * @return  Number of matches 
	 */
	int getValues(std::string str,
		    std::vector<int>* value,
		    char* service,
		    char* hardware);
	/** 
	 * Check the input 
	 * @param arg input 
	 * @return  Retur value 
	 */
	bool inputValidation(char* arg);
    };
    //end of namespace
  }
}
#endif
