#ifndef ZTT_DCS_FEE_ITEM_INFO_HPP
#define ZTT_DCS_FEE_ITEM_INFO_HPP

#include "FeeInfo.hpp"
#include "dim/dis.hxx"
#include "FeeGroupedChannel.hpp"
#include <iostream>
#include <boost/shared_ptr.hpp>

namespace ztt { namespace dcs {

/**
 * Value, when link to an Item-Service is not available.
 */
#define FEE_ITEM_NO_LINK -1000.0  

/**
 * Define for timeout, when item is at least updated.
 * (in seconds, polled by client).
 */
#define FEE_ITEM_TIMEOUT 10       



/**
 * Class, to subscribe and get access to the FEE - Item - service.
 * It inheritates from FeeInfo to get the DimInfo features, and to enable a
 * polymorph access to the service data. Create objects of FeeItemInfo to get
 * a connection to a given FEE Item on the server. The value is also updated
 * after a certain timeout.
 *
 * @see FeeInfo for more details
 *
 * @author Christian Kofler, Sebastian Bablok
 *
 * @date 2003-08-27
 */


class FeeItemInfo : public FeeInfo {
	public:
		/*
		struct CollectServices{
		int valueint;
		float valuefloat;
		char servicename[256];
		CollectServices(int valint,float valfloat, char* name){
			valueint=valint;
			valuefloat=valfloat;
			strcpy(servicename,name);}
		}collectServices;
		*/
		/**
		 * Constructor for the FeeItemInfo.
		 * This constructor calls implictly the FeeInfo - constructor and with 
		 * that the DimInfo constructor, so you subsrcibe to the inserted 
		 * FEE - Item - Service (name).
		 *
		 * @param name name of the service, to subscribe to
		 * @param time time interval, when to update service
		 * @param noLink value to be shown, when no connection to this service 
		 *			is available
		 * @param pHandler pointer to the DimInfoHandler
		 */
		//!!!this constructor causes errors on DCS-Board when using DIM-Timer!!!
		/*FeeItemInfo(char* name, int time, float noLink, DimInfoHandler* pHandler)
				: mValue(FEE_ITEM_NO_LINK), mpFedService(NULL),
				FeeInfo(name, time, noLink, pHandler) {};*/
		 /**
        * Constructor for the FeeItemInfo, without service request after time
        * intervals.
        * This constructor calls implictly the FeeInfo - constructor and with
        * that the DimInfo constructor, so you subsrcibe to the inserted
        * FEE - Item - Service (name).
        *
        * @param name name of the service, to subscribe to
        * @param noLink value to be shown, when no connection to this service
        *          is available
        * @param pHandler pointer to the DimInfoHandler
        */
       FeeItemInfo(char* name, float noLink, DimInfoHandler* pHandler)
               : mValue(FEE_ITEM_NO_LINK), name(name), mpFedService(),feeGroup(),
			   FeeInfo(name, noLink, pHandler){}; 

		/**
		 * Destructor for the FeeItemInfo objects
		 */
	   virtual ~FeeItemInfo() {
		 mpFedService.reset();
		 feeGroup.reset();
	   };

		/**
		 * Function, to be executed when new data from the FEE - Item - Service
		 * is available. It retrieves the data of the FeeServer from the DIM 
		 * framework. It stores the value in the object itself. To get the value
		 * the the getValue function.
		 *
		 * @return true, if service is available, false in case of "no link"
		 */
		virtual bool retrieveServiceValue();

		/**
		 * Function to set the initial value of this ItemInfo. A possible
		 * "No link" at start up can be determined this way.
		 * This function should be called once after constructing this object.
		 */ 
		virtual void initialize();

		/**
		 * Getter for the reference to the service value.
		 *
		 * @return reference to the current value of this service
		 */
		float& getRefFloatValue();

		/**
		 * Sets the pointer to the corresponding Service on the FED-Server.
		 *
		 * @param pService pointer to the corresponding Service.
		 */
		void setCorrespondingFedService(boost::shared_ptr<DimService> pService);

		void setFeeGroupChannel(boost::shared_ptr<FeeGroupedChannel> groupedChannel);
		/**
		* name store the service name of each server.
		* Is used to assign the services of a server to a broadcastchannel. Used in FedServer
		*/
		char* name;
	private:
		/** 
		 * Here is the value of this Service stored.
		 */
		float mValue;

		/**
		 * Pointer to the corresponding DimService - object.
		 */
		boost::shared_ptr<DimService> mpFedService;
		/**
		* feeGroup represents the grouped channel
		*/
		boost::shared_ptr<FeeGroupedChannel> feeGroup;
		

}; // end class

inline float& FeeItemInfo::getRefFloatValue() {
	return mValue;
}

inline void FeeItemInfo::setCorrespondingFedService(boost::shared_ptr<DimService> pService) {
	mpFedService = pService;
}
inline void FeeItemInfo::setFeeGroupChannel(boost::shared_ptr<FeeGroupedChannel> groupedChannel){
	feeGroup = groupedChannel;
}

} } // end namespace

#endif
