#include "ControlFero.hpp"
#include "Logger.hpp"
#include <string>
#include "RegExpr.hpp"
#include "FeeServerMap.hpp"
#include "InterCom.hpp"

#include <set>

using namespace std;
using namespace ztt::dcs;



bool ControlFero::handleCommand(){
	// create the logger to write events into the logfile or to PVSS
	const Logger* logIt = Logger::getLogger();
	// get the singleton object of the ICL	
	bool nRet = false;	
	//check that command contains data
	if(getSize()<= 0){
		try {   
            char text[100];
            text[sprintf(text, 
                "Received an ControlFero command with size <= 1, command ignored.")] = 0;
            logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
        } catch (std::runtime_error &excep) {
            // Can't print and send log message,
            // but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
            cerr << "Exception: " << excep.what() << endl;
#endif
        }
        return nRet;
	}
	//check that no NULL pointer is given
	if(getData()== NULL){
		try{
			char message[256];
			message[sprintf(message,"Received null pointer from DIM in ControlFero, value rejected")] = 0;
			logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		return nRet;
	}
	char* data = NULL;
	//cast the incomming command to the expected format	
	try{
		 data = reinterpret_cast<char*>(getData());
	}catch(bad_cast& e){
		char message[256];
		message[sprintf(message,"Cast operation in ControlFero channel failed %s",e.what())] = 0;
		logIt->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);

		return nRet;
	}

#ifdef __BENCHMARK
	try{
		char message[256];
		const Logger* logger = Logger::getLogger();
		ACE_Time_Value ts = ACE_OS::gettimeofday();
		struct timeval tStamp = ts.operator timeval();;
		struct tm *ptimes = localtime(&(tStamp.tv_sec));

		message[sprintf(message,"Benchmark ICL Timestamp(Receive ContFero) Time:%.8s - %ld",
			asctime(ptimes) + 11, tStamp.tv_usec)]=0;
		logger->createLogMessage(Msg_Debug,SOURCE_LOCAL,message);
		
	}catch(std::runtime_error &excep){
		cerr << "Exception in ContFeroBenchmark: " << excep.what() << endl;
	}
#endif
	InterCom* icl = InterCom::createInterCom();
	// this pointer store the reference to the FeeServerMap
	FeeServerMap* pFeeMap = icl->getFeeServerMap();
	// feeservernames store the single servernames returned by map
	vector<char*>feeservernames;
	// store the three coordinates 
	vector<int> values;
	// reg is used to check the correct assembly of the target name 
	// and to extract the different information the target name contains
	RegExpr reg;
	// store the optional service name (servicename will be ignored)
	char* services = new char[256];
	// store the hardware type assambled in the target name
	char* hwType = new char[10];
	//get the size of the payload (20 signs are used for the name)
	int psize = getSize()-20;
	//extract values from channel and store them.
	char* target = new char[20];
	char* payload = new char[psize];
	memcpy(target,data,20);
	memcpy(payload,data+20,psize);

	//check that the target name is correct
	if(!reg.inputValidation(target)){
		try{
			const Logger* logger = Logger::getLogger();
			char message[256];
			message[sprintf(message,"Received strange targetname for ControlFero channel, command rejected")] = 0;
			logger->createLogMessage(Msg_Warning,SOURCE_LOCAL,message);
		}catch(std::runtime_error &excep) {
			// Can't print and send log message,
			// but this is not so critical (Server-functionality is not affected).
			cerr << "Exception: " << excep.what() << endl;
		}
		
		delete[] target;
		delete[] payload;
		delete[] services;
		delete[] hwType;
		return nRet;

	}
	//copy target name to string to get coordinates
	string name;
	name=target;
	//extract name of feeserver
	reg.getValues(name,&values,services,hwType);
	//get desired feeservernames
	feeservernames = pFeeMap->getFeeServer(values.at(0),values.at(1),values.at(2),hwType);

	if(!feeservernames.empty()){

		set<int> expectedAnswers;
		int answer = 0;
		//compute expected answer range
		//create feepacket with payload 
		boost::shared_ptr<FeePacket> feepacket = icl->createBroadCastFeePacket(payload,psize,false,0,&feeservernames);
		localID = feepacket->getId();
		
		mAnswerStruct.setAnswerStruct(feepacket->getId(),feepacket->getAnswerSet());
		//send the processed command to the feeserver
		send.setFeePacket(feepacket);
		send.activate(THR_DETACHED|THR_NEW_LWP,1,1);
		// start watchDog
		mWatchDog.putq(mb);
		//mark channel busy
		busy = true;
		nRet = true;
		feepacket.reset();
	}else{
		try {
					char text[100];
					text[sprintf(text, 
						"Unknown FeeServername received by ControlFero channel")] = 0;
					logIt->createLogMessage(Msg_Warning, SOURCE_LOCAL, text);
				} catch (std::runtime_error &excep) {
				// Can't print and send log message,
				// but this is not so critical (Server-functionality is not affected).
#ifdef __DEBUG
					cerr << "Exception: " << excep.what() << endl;
#endif
				}
	}
	delete[] target;
	delete[] payload;
	delete[] services;
	delete[] hwType;
	feeservernames.clear();


	return nRet;
}
bool ControlFero::setAnswerData(const int& ID){	
	bool bRet = false;
//    int status;

    // to avoid sending of the current result data when just subscribing to
    // service. No real answer, because no command has been sent first.
	if ((ID == 0)) {
        return bRet;
    }

	int ret = 0;
	ret = mAnswerStruct.findAnswerID(ID);
	if (ret == 1){
		bRet = true;
		//wake up watchdog
		mWatchDog.wakeUp();
		//notify that command was sucsessful
		char* text = new char[256];
		Timestamp ts;
		text[sprintf(text,"ControlFero Ack;%s",ts.date)]=0;
		iclAck->setAckStruct(0,text);
		
		busy = false;
		delete[] text;
	}else if(ret == 2){
		bRet = true;
	}

    // hier ist noch sicher zustellen, das entweder Antwort, !!!!
    // oder Timeout gesendet wird, wenn sich der check überschneidet !!!!
    // !!! Szenario in Gedanken noch komplett durchspielen !!!
/*
	//check that answer is expected
	vector<int>::iterator iter = expectedAnswers.begin();
	while (iter != expectedAnswers.end()){
		
		// handle right answer only if this channel is active/busy
		if (ID == *iter) {
			//to update feeserver ack channel
			bRet = true;
			//erase incomming
			expectedAnswers.erase(iter);
			//if vector is empty all ack arrived
			if(expectedAnswers.empty()){
				//wake up watchdog
				cond.signal();
				//notify that all expected acks arrived
				char* text = new char[256];
				Timestamp ts;
				text[sprintf(text,"ControlFero Ack;%s",ts.date)]=0;
				iclAck->setAckStruct(0,text);

				delete[] text;
			}
		} else {
			++iter;
		}
	}
  */
    return bRet;
	
}

void* ControlFero::watchDog(void* objHandler){
	/*ControlFero* handler = reinterpret_cast<ControlFero*>(objHandler);
	//lock critical section
	handler->mutex.acquire();
    int status = 0;
	int id = handler->localID;
	
#ifdef __DEBUG
    cout << "Watch dog started ..." << endl;
#endif
    // init timer for watch dog
	// see examples ACE example directory
	ACE_Time_Value timer (CTF_WATCH_DOG_TIMEOUT/ACE_ONE_SECOND_IN_MSECS,0L);
	ACE_Time_Value expire_at = ACE_OS::gettimeofday() + timer;
	status = handler->cond.wait(&expire_at);
	if(status == -1){
		//error message
		cout<<"WATCHDOGTIMEOUT in ControlFero occured"<<endl;
		//evtl. melden welcher Server nicht geantwortet hat(Logger||icl_messagechannel)
		//handler->expectedAnswers.clear();
		handler->mAnswerStruct.freeAnswerStruct(id);
		//told PVSS that time out occured
		char* error = new char[256];
		error[sprintf(error,"ControlFero watchdog timeout")]=0;
		handler->iclAck->setAckStruct(Msg_Warning,error);
		delete[] error;
	}else{
		cout<< "watchdog stopped normally"<<endl;
	}
	handler->busy = false;
	handler->mutex.release();
*/
	return 0;
}
ControlFero::~ControlFero(){
	mutex.remove();
	delete mb;
	ACE_Thread_Manager::instance()->cancel_task(&mWatchDog);
}
