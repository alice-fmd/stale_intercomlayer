#ifdef __UTEST

#ifndef ZTT_DCS_LOGGERTEST_HPP
#define ZTT_DCS_LOGGERTEST_HPP

#include "Logger.hpp"
#include "DataAccess.hpp"

namespace ztt { namespace dcs {

/**
 * This class makes unit tests of the Logger.
 *
 * @author Sebastian Bablok
 */
class LoggerTest {
public:
    LoggerTest();

    ~LoggerTest();

    bool testGetLogger();

    bool testSetLogLevel();

    bool testSetRemoteLogLevel();

    bool testCheckLogLevel();

    bool testCheckRemoteLogLevel();

    bool testCreateLogMessage();

    bool testRelayLogEntry();

    bool testLogger();

    bool testAll();

private:
    boost::shared_ptr<DataAccess> dA;


};// end class

} } // end namespace


#endif // end of header

#endif // end of UTEST
