#ifdef __UTEST

#ifndef REG_EXPR_TEST
#define REG_EXPR_TEST

#include "RegExpr.hpp"

class RegExprTest
{
public:
	RegExprTest(void);
	~RegExprTest(void);
	bool TestInputValidation(char*);
	bool TestInputValidationAll();
};
#endif
#endif