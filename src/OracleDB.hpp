#ifndef ORACLEDATABASE
#define ORACLEDATABASE

#ifdef HAVE_ORACLE
namespace oracle 
{
  namespace occi 
  {
    class Environment;
    class ConnectionPool;
    class Connection;
    class Statement;
    class ResultSet;
  }
}
#endif
#include <vector>
#include "Database.hpp"


namespace ztt { 
  namespace dcs {
    // Forward decls 
    class DataAccess;
    
    /**
     * This class implements the real database actions required by the
     * interComLayer.  The class is derived from the database
     * interface and implement the required functions.
     *
     * @author Benjamin Schockert
     */

    class OracleDB : public Database
    {
    public:
      /**
       * The constructor get the data accessobject to get the values
       * from the property file.
       */
      OracleDB();
      /**
       * Create the connection pool for database access.
       */
      virtual int createConnection();
      /**
       * load the exact position of the single server from the
       * database and load the corresponding servername
       *
       * @param tablename specify the table/view of the servernames
       * @param collection store the loaded servernames in a smart_ptr
       * @param posX store the x coordinates of the server
       * @param posY store the y coordinates of the server
       * @param posZ store the z coordinates of the server
       */	
      virtual int getDataFromDB(char* tablename, 
				std::vector<char* >* collection, 
				std::vector<int>*posX,
				std::vector<int>*posY,
				std::vector<int>*posZ );
      /**
       * Load the Server-/Service names from Database.
       *
       * @param tablename of the table
       * @param collection save the result of the query
       */
      virtual int getDataFromDB(char* tablename, 
				std::vector<char* >* collection );
      /**
       * Write files to Database(not implemented yet).
       * 
       * @param tablename where data should be stored
       * @param data which should written to database
       */
      virtual int setData(char* tablename, char* data);
      /**
       * Destroys the connection pool
       */
      virtual int killConnection();
    private:
      /**
       * Save the max possible number of connections
       */
      unsigned int maxCon;
      /**
       * Save the min possible number of connections.
       */
      unsigned int minCon;
      /**
       * Save the number of connections who must be free to get a new
       * connection
       */
      unsigned int incCon;
#ifdef HAVE_ORACLE
      /*
       * The environment of the Database
       */
      oracle::occi::Environment* env;
      /*
       * The connection pool variable
       */
      oracle::occi::ConnectionPool* conPl;
      /*
       * The real connection to database
       */
      oracle::occi::Connection* con;
      /*
       * Used to execute statements on db
       */
      oracle::occi::Statement* stm;
      /*
       * Return the result of the statement
       */
      oracle::occi::ResultSet* rs;
#endif
      /**
       * Value to get access to the content of the propertyfile where
       * the data- base specific values were stored.
       */
      ztt::dcs::DataAccess* dataAccess;
    };
  }}
#endif

