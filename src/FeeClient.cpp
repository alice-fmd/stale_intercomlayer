#include "FeeClient.hpp"
#include "InterCom.hpp"
#include "FeeAckInfo.hpp"
#include "FeeItemInfo.hpp"
#include "Timestamp.hpp"

#ifdef __DEBUG
#include <iostream>
#endif

using namespace std;
using namespace ztt::dcs;


FeeClient::~FeeClient() {

}


void FeeClient::infoHandler() {
	// get reference to currently handled DimInfo - incoming Service
	DimInfo* current = getInfo();
    char msg[255];
	
// Try - catch block for dynamic cast
// !! compiler must have enabled RTTI (option: /GR [on], /GR- [off, default])
	try {
		if (FeeInfo* pInfo = dynamic_cast<FeeInfo*> (current)) {
			if (!pInfo->retrieveServiceValue()) {

				// notification of no link, what to do next ??? 
                msg[sprintf(msg, "FeeClient has NO LINK to %s.", 
                        pInfo->getName())] = 0;
                mpInterCom->setLogEntry(Msg_Error, SOURCE_FEE_CLIENT, msg);

#				ifdef __DEBUG
				cout << "No link on " << current->getName() << endl;
#				endif

				return;
			}
            // message about received data would waste too much memory

#ifdef __DEBUG
			//get the name of the incoming Service
//			cout << "name of incoming service:  " << current->getName() << endl;
			//get the size of the Data of the incoming Service
//			cout << "size of incoming service:  " << current->getSize() << endl;
//			cout << "- - - - - - - - - - - - - - - - - - - - - " << endl;
#endif

		} else {
			msg[sprintf(msg, "FeeClient received unknown info-object (%s).", 
                    current->getName())] = 0;
            mpInterCom->setLogEntry(Msg_Error, SOURCE_FEE_CLIENT, msg);

#			ifdef __DEBUG
			cerr << "Wrong Info - object, dynamic_cast< > failed!" << endl;
#			endif

		}

	} catch (exception) {
        msg[sprintf(msg, 
                "In FeeClient occured an exception while receiving data from %s.",
                current->getName())] = 0;
        mpInterCom->setLogEntry(Msg_Error, SOURCE_FEE_CLIENT, msg);

#		ifdef __DEBUG
		cout << "\nException while trying dynamic cast!\n" << endl;
#		endif

	}

}

//-- subscribing to ACK - channels, message channels  and Items --
int FeeClient::subscribeTo(vector<char* >* servers, vector<char* > *pServices) {
	// get global collections in order to fill them
	vector<FeeAckInfo*>* pAckCol = mpInterCom->getFeeAckInfoCollection();
    vector<FeeMsgInfo*>* pMsgCol = mpInterCom->getFeeMsgInfoCollection();
    // iterator for list of servers
	vector<char* >::iterator itServer = servers->begin();

	// -- go through list of servers, subscribe to ACK & MSG, add them to Collection --
	while (itServer != servers->end()) {
        int length = strlen(*itServer);
		char* ack = new char[length + 13];
        char* msg = new char[length + 9];
		char* serverName = new char[length + 1];
        char* noLinkMsg = new char[length + 100];
        FeeAckInfo* ackService;
        FeeMsgInfo* msgService;
        Timestamp date;

        // construting Fee - specific structure and objects
        noLinkMsg[sprintf(noLinkMsg, 
                "No link to message channel of %s! [Note: Date is date of subscribing]",
                *itServer)] = 0;                    

        MessageStruct noLinkStruct(Msg_Warning, DETECTOR, SOURCE_FEE_CLIENT,
                noLinkMsg, date.date);

        // subscribing to ack channel
		ack[sprintf(ack, "%s_Acknowledge", *itServer)]=0;
		serverName[sprintf(serverName, "%s", *itServer)]=0;
		ackService = new FeeAckInfo(ack, (void*) FEE_ACK_NO_LINK,
                    FEE_ACK_NO_LINK_SIZE, this, serverName);
		ackService->initialize();
		pAckCol->push_back(ackService);

        // subscribing to msg channel
        msg[sprintf(msg, "%s_Message", *itServer)]=0;
        msgService = new FeeMsgInfo(msg, (void*) &noLinkStruct,
                    this, serverName);
		msgService->initialize();
		pMsgCol->push_back(msgService);

        // don't delete serverName, this is stored in the AckInfo and MsgInfo
        // objects, so keep it. !!! Look, if memory managment is in these cases
        // correct ??? maybe search for different solution !!!
		// HACK same serverName[] is used in AckInfo & MsgInfo (as const)
		delete[] ack;
        delete[] msg;
        delete[] noLinkMsg;
		//delete[] serverName;
		++itServer;
	}

    // subscribing to item channels
    vector<FeeItemInfo*>* pItemCol = mpInterCom->getFeeItemInfoCollection();
    // iterator for list of services
	vector<char* >::iterator itServ = pServices->begin();
	while (itServ != pServices->end()) {
		//when service unreachable --> -1000.0 (FEE_ITEM_NO_LINK)
		float nolink = FEE_ITEM_NO_LINK;
		FeeItemInfo* service = new FeeItemInfo(*itServ,nolink, this);
		service->initialize();
		pItemCol->push_back(service);
		++itServ;
	}

	
    return (pItemCol->size() + pAckCol->size() + pMsgCol->size());
}

