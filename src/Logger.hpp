#ifndef ZTT_DCS_LOGGER_HPP
#define ZTT_DCS_LOGGER_HPP

#include <fstream>
#include <stdexcept>
#include "ace/Synch.h"

#include "FedMessenger.hpp"


namespace ztt { namespace dcs {

/**
 * Define for default local log level (Msg_Alarm should be always inclueded).
 */
#ifndef __BENCHMARK
	#define DEFAULT_LOCAL_LOGLEVEL (Msg_Info+Msg_Error+Msg_Warning+Msg_Alarm)
#endif

#ifdef __BENCHMARK
	#define DEFAULT_LOCAL_LOGLEVEL (Msg_Info+Msg_Error+Msg_Warning+Msg_Alarm+Msg_Debug)
#endif
/**
 * Define for default remote log level (Msg_Alarm should be always inclueded).
 */
#ifndef __BENCHMARK
	#define DEFAULT_REMOTE_LOGLEVEL (Msg_Info+Msg_Error+Msg_Warning+Msg_Alarm)
#endif
#ifdef __BENCHMARK
#define DEFAULT_REMOTE_LOGLEVEL (Msg_Info+Msg_Error+Msg_Warning+Msg_Alarm+Msg_Debug)
#endif
/**
 * Defines, when the logfile shall be closed and reopened to ensure readability
 */
#define FILE_CLOSE_COUNT 3

/**
 * Define for origin in DataAccess module
 */
#define INTERCOM_DATA_ACCESS "InterComLayer-DataAccess"

/*#ifndef WIN32
#define STANDARD_LOG_DIRECTORY "/var/log/"
#endif*/
/**
 * Logger class, controlles all kind s of logging mechanisms of the interCom -
 * Layer. It provides the FedMessenger with logging - messages. IT will take 
 * care of the different levels of logging, the different places, where 
 * messages will be stored. It also writes entries to a logfile (logfile name:
 * "InterCom.log".
 * 
 * @date 23-12-2003
 *
 * @author Sebastian Bablok, Benjamin Schockert
 */
class Logger {
public:

    /**
     * Method to create a single Logger - object (Singleton).
     * This method takes care, that only one Logger - object is created
     * at one time. If this object already exists, it returns the pointer 
     * to this object (see also "Design Patterns, GoF, - Singleton).
     * This function must be static, because it is called without having an
     * Logger - object.
     * ??? maybe use smart - pointer here to take care of the logger !!!
     *
     * @param pMessenger pointer to the FedMessenger, which informs the upper
     *          layers about messages.
     *
     * @return pointer to the one and only Logger - object.
     */
    static Logger* createLogger(FedMessenger* pMessenger);

    /**
     * Destructor for Logger and writes a logfile entry about shuting down
     * the InterCom - Layer (Exception: no check for the LogLevel will be done
     * here).
     */
    virtual ~Logger();

    /** 
     * Function to get the Logger from "outside".
     * If Logger - objects has not been created yet, an exception is thrown.
     *
     * @return pointer to the one and only Logger - object, if created.
     *
     * @throws Nullpointer exception, if no Logger obejcts has been created yet.
     */
    static const Logger* getLogger();

    /**
     * Prepares a logging message for sending it to the FedClient via 
     * FedMessenger and writes it also to the logfile, if possible.
     *
     * @param type the event type of this message
     * @param origin the source of this message
     * @param description the message body
     */
    void createLogMessage(unsigned int type, char* origin, char* description) const;

    /**
     * Relays a log entry, which is received from a FeeServer.
     * The messages is checked against the remote log level (for the FeeServer
     * messages), if this fails, they are at least written to the log file, 
     * else they are delivered further.
     *
     * @param msgStruct pointer to the message struct storing the received log
     *          entry from the FeeServer.
     */
    void relayLogEntry(MessageStruct* msgStruct) const;

    /**
     * Setter for the LogLevel (localy created events).
     * A check for a valid LogLevel value is also done (if not valid, a message
     * is send). [<b>NOTE</b>: The log level <b>Msg_Alarm</b> is always set!)
     * A valid LogLevel can be any combination of:<br><ul>
     * <li> INFO [1] </li>
     * <li> WARNING [2] </li>
     * <li> ERROR [4] </li>
     * <li> FAILURE AUDIT [8] </li>
     * <li> SUCCESS AUDIT [16] </li>
     * <li> DEBUG [32] </li>
     * <li> ALARM [64] </li></ul><br>
     *
     * @see FedMessenger for more details about LogLevels.
     * 
     * @param logLevel the desired logLevel.
     */
    void setLogLevel(unsigned int logLevel);

    /**
     * Setter for the LogLevel of remote events.
     * A check for a valid remote LogLevel value is also done (if not valid, a 
     * message is send). [<b>NOTE</b>: The log level <b>Msg_Alarm</b> is always 
     * set!) A valid LogLevel can be any combination of:<br><ul>
     * <li> INFO [1] </li>
     * <li> WARNING [2] </li>
     * <li> ERROR [4] </li>
     * <li> FAILURE AUDIT [8] </li>
     * <li> SUCCESS AUDIT [16] </li>
     * <li> DEBUG [32] </li>
     * <li> ALARM [64] </li></ul><br>
     *
     * @see FedMessenger for more details about LogLevels.
     * 
     * @param remoteLogLevel the desired logLevel for further delivery of 
     *          remote events.
     */
    void setRemoteLogLevel(unsigned int remoteLogLevel);

    /** 
     * Creates a char string containing the current date for a messagy entry
     * in the format used for the message channel.
     *
     * @return the char string of the current date formatted for messages.
     */
//    static char* createDateEntry();
    // better solution with timestamp struct


   /**
     * Getter for the current LogLevel (local), used for unit tests.
     *
     * @return current LogLevel (local)
     */
    unsigned int getLogLevel();

    /**
     * Getter for the current remote LogLevel, used for unit tests.
     *
     * @return current RemoteLogLevel
     */
    unsigned int getRemoteLogLevel();




private:

    /**
     * Constructor for the Logger, and informs the FedMessenger, that InterCom -
     * Layer has been started. If a logFile can be created, every message is 
     * also written to this file. Implemented as Singleton.
     * 
     * @param pMessenger pointer to the FedMessenger, which informs the upper
     *          layers about messages.
     */
    Logger(FedMessenger* pMessenger);

    /**
     * This is the one and only Logger - object.
     * (see Singleton, GoF, Desing Patterns).
     */
    static Logger* pInstance;

    /**
     * Pointer to the FedMessenger to deliver messages for sending.
     */
    FedMessenger* mpMessenger;

    /**
     * Pointer to FileHandle for LogFile.
     */
    std::ofstream* mLogFile;

    /**
     * LogFile name, used to identify logFile of the current run.
     */
    char mFileName[40];

    /**
     * Current LogLevel (default level is "INFO|WARNING|ERROR|ALARM").
     *
     * @see FedMessenger for more details about LogLevels.
     * @see setLogLevel for more information about valid LogLevels.
     */
    unsigned int mLogLevel;

    /**
     * Current remote LogLevel (this log level filters messages from the 
     * FeeServers, rejected messages are at least written to file, accepted
     * ones are delivered further / default level "INFO|WARNING|ERROR|ALARM").
     *
     * @see FedMessenger for more details about LogLevels.
     * @see setLogLevel for more information about valid LogLevels.
     */
    unsigned int mRemoteLogLevel;

	/**
	 * counter to regularly close and open the log file. This is necessary to be
	 * able to read the logfile during the interComLayer is still running.
	 * This is a compromise of:
	 * - opening, writing and closing each time
	 * - opening once, writing and closing on clean(!) exit
	 */
	unsigned int mFileCount;
	
	/**
     * Checks the event - type to the current LogLevel for local messages. 
     * If event - type is inside current LogLevel, the event will be announced,
     * if not, no message will be send or logged (NO log file entry).
     * 
     * @param event type of the event.
     *
     * @return true, if event - type is inside current LogLevel, else false.
     */
    bool checkLogLevel(unsigned int event) const;

    /**
     * Checks the event - type to the current LogLevel for remote messages. 
     * If event - type is inside current remote LogLevel, the event will be 
     * announced, if not, no message will be send, BUT written to file. 
     * 
     * @param event type of the remote event.
     *
     * @return true, if remote event - type is inside current remote LogLevel,
     *               else false.
     */
    bool checkRemoteLogLevel(unsigned int event) const;

    /**
     * Sends a log entry, received from a FeeServer or created localy, further
     * to the PVSS.
     *
     * @param msgStruct pointer to the message struct storing the received log
     *          entry from a FeeServer or the localy created one.
     */
    void sendLogEntry(MessageStruct* msgStruct) const;

	/**
	 * finally writes an entry in the log file.
	 * this method encapsulates all checks related with the log file,
	 * such as 'does it exist', 'do I have to close and reopen it?'.
	 * If something goes wrong with writin the log file, false is returned.
	 *
	 * @param logEntry the entry for the log file
	 * 
	 * @return indicates if writing the log entry was sucecssful
	 */
	bool writeLogEntry(MessageStruct* msgStruct);

	/**
	* synchronize access to createLogEntry()
	*/
	ACE_Mutex mutex;
	/**
	* miLogDay stores the start day of the InterComLayer. If a new day start
	* the InterComLayer creates a new log file.
	* This step was required to achieve small logfiles for each day.
	*/
	int miLogDay;
};


// inline for checking LogLevel for local events
inline bool Logger::checkLogLevel(unsigned int event) const {
    // typically Eric construct !
    return (event & mLogLevel) ? true : false;
}

// inline for checking LogLevel for remote events
inline bool Logger::checkRemoteLogLevel(unsigned int event) const {
    
    return (event & mRemoteLogLevel) ? true : false;
}

// inliner to get created Logger
inline const Logger* Logger::getLogger() {
    if (pInstance == 0) {
        throw std::runtime_error("Logger has not been instanciated yet!");
    }
    return pInstance;
}

} } //end namespaces

#endif
