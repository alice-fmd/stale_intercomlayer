dnl -*- mode: Autoconf -*- 
dnl
dnl  ROOT generic intercomlayer framework 
dnl  Copyright (C) 2004 Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl
dnl
dnl __________________________________________________________________
dnl
dnl AC_INTERCOMLAYER([MINIMUM-VERSION 
dnl             [,ACTION-IF_FOUND 
dnl              [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AC_INTERCOMLAYER],
[
    # Command line argument to specify prefix. 
    AC_ARG_WITH([intercomlayer],
        [AC_HELP_STRING([--with-intercomlayer],	
                        [Prefix where InterComLayer is installed])],
	[],[with_intercomlayer="yes"])

    # Command line argument to specify documentation URL. 
    AC_ARG_WITH([intercomlayer-url],
        [AC_HELP_STRING([--with-intercomlayer-url],
	  [Base URL where the InterComLayer documentation is installed])],
        intercomlayer_url=$withval, intercomlayer_url="")
    if test "x${INTERCOMLAYER_CONFIG+set}" != xset ; then 
        if test "x$with_intercomlayer" != "xno" ; then 
	    INTERCOMLAYER_CONFIG=$with_intercomlayer/bin/intercomlayer-config
	fi
    fi   
	
    # Check for the configuration script. 
    if test "x$with_intercomlayer" != "xno" ; then 
        AC_PATH_PROG(INTERCOMLAYER_CONFIG, intercomlayer-config, no)
        intercomlayer_min_version=ifelse([$1], ,0.3,$1)
        # Message to user
        AC_MSG_CHECKING(for InterComLayer version>=$intercomlayer_min_version)

        # Check if we got the script
        with_intercomlayer=no    
        if test "x$INTERCOMLAYER_CONFIG" != "xno" ; then 
           # If we found the script, set some variables 
           INTERCOMLAYER_CPPFLAGS=`$INTERCOMLAYER_CONFIG --cppflags`
           INTERCOMLAYER_INCLUDEDIR=`$INTERCOMLAYER_CONFIG --includedir`
           INTERCOMLAYER_LIBS=`$INTERCOMLAYER_CONFIG --libs`
           INTERCOMLAYER_LTLIBS=`$INTERCOMLAYER_CONFIG --ltlibs`
           INTERCOMLAYER_LIBDIR=`$INTERCOMLAYER_CONFIG --libdir`
           INTERCOMLAYER_LDFLAGS=`$INTERCOMLAYER_CONFIG --ldflags`
           INTERCOMLAYER_LTLDFLAGS=`$INTERCOMLAYER_CONFIG --ltldflags`
           INTERCOMLAYER_PREFIX=`$INTERCOMLAYER_CONFIG --prefix`
           
           # Check the version number is OK.
           intercomlayer_version=`$INTERCOMLAYER_CONFIG -V` 
           intercomlayer_vers=`echo $intercomlayer_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           intercomlayer_regu=`echo $intercomlayer_min_version | \
             awk 'BEGIN { FS = "."; } \
    	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
           if test $intercomlayer_vers -ge $intercomlayer_regu ; then 
                with_intercomlayer=yes
           fi
        fi
        AC_MSG_RESULT($with_intercomlayer - is $intercomlayer_version) 
    
        # Some autoheader templates. 
        AH_TEMPLATE(HAVE_INTERCOMLAYER, [Whether we have intercomlayer])
    
    
        if test "x$with_intercomlayer" = "xyes" ; then
            # Now do a check whether we can use the found code. 
            save_LDFLAGS=$LDFLAGS
    	    save_CPPFLAGS=$CPPFLAGS
            LDFLAGS="$LDFLAGS $INTERCOMLAYER_LDFLAGS"
            CPPFLAGS="$CPPFLAGS $INTERCOMLAYER_CPPFLAGS"
     
            # Change the language 
            AC_LANG_PUSH(C++)
    
     	    # Check for a header 
            have_intercomlayer_intercom_h=0
            AC_CHECK_HEADER([intercomlayer/InterCom.hpp], 
	                    [have_intercomlayer_intercom_h=1])
    
            # Check the library. 
            have_libintercomlayer=no
            AC_MSG_CHECKING(for -lInterCom)
            AC_LINK_IFELSE([
               AC_LANG_PROGRAM([#include <intercomlayer/InterCom.hpp>
#include <intercomlayer/CommandCoderBase.hpp>
#include <string>
#include <vector>
struct DummyCoco : public CommandCoderBase {
  int createDataBlock(char*,int) { return 0;}
  long int* getDataBlock() { return 0; }
  std::vector<std::string> getError() {std::vector<std::string> r; return r; }
};
CommandCoderBase* CommandCoderBase::instance = new DummyCoco;],
                               [ztt::dcs::InterCom::createInterCom()])], 
                               [have_libintercomlayer=yes])
            AC_MSG_RESULT($have_libintercomlayer)
    
            if test $have_intercomlayer_intercom_h -gt 0    && \
                test "x$have_libintercomlayer"   = "xyes" ; then
    
                # Define some macros
                AC_DEFINE(HAVE_INTERCOMLAYER)
            else 
                with_intercomlayer=no
            fi
            # Change the language 
            AC_LANG_POP(C++)
    	CPPFLAGS=$save_CPPFLAGS
    	LDFLAGS=$save_LDFLAGS
        fi
    
        AC_MSG_CHECKING(where the InterComLayer documentation is installed)
        if test "x$intercomlayer_url" = "x" && \
    	test ! "x$INTERCOMLAYER_PREFIX" = "x" ; then 
           INTERCOMLAYER_URL=${INTERCOMLAYER_PREFIX}/share/doc/intercomlayer
        else 
    	INTERCOMLAYER_URL=$intercomlayer_url
        fi	
        AC_MSG_RESULT($INTERCOMLAYER_URL)
    fi
   
    if test "x$with_intercomlayer" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(INTERCOMLAYER_URL)
    AC_SUBST(INTERCOMLAYER_PREFIX)
    AC_SUBST(INTERCOMLAYER_CPPFLAGS)
    AC_SUBST(INTERCOMLAYER_INCLUDEDIR)
    AC_SUBST(INTERCOMLAYER_LDFLAGS)
    AC_SUBST(INTERCOMLAYER_LIBDIR)
    AC_SUBST(INTERCOMLAYER_LIBS)
    AC_SUBST(INTERCOMLAYER_LTLIBS)
    AC_SUBST(INTERCOMLAYER_LTLDFLAGS)
])
dnl
dnl EOF
dnl 
